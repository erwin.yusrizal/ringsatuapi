import arrow
from sqlalchemy import extract, text

from marshmallow import (
    Schema, 
    fields
)
from api.v1.models.users import (
    UserModel
)
from api.v1.models.residentials import (
    FinancialReportModel,
    ChatMessageModel
)
from api.v1.utils import (
    reverse_id, 
    get_administrative_region,
    denormalize_phone_number
)

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)

class DenormalizePhone(fields.Field):
    def _serialize(self, value, attr, obj):
        if value:
            return denormalize_phone_number(value)
        return value

class LiveLocationSchema(Schema):
    id = ReverseId(dump_only=True)
    latitude = fields.Decimal(as_string=True)
    longitude = fields.Decimal(as_string=True)
    altitude = fields.Int()
    accuracy = fields.Int()
    speed = fields.Int()
    time = fields.Int()
    created = fields.DateTime()
    updated = fields.DateTime()
    panic = fields.Nested('PanicSchema', attribute='live_location_panic')
    user = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))


class ChatSchema(Schema):
    id = ReverseId(dump_only=True)
    sender = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'bank_accounts', 'childs', ))
    recipient = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'bank_accounts', 'childs', ))
    created = fields.DateTime()
    updated = fields.DateTime()
    last_message = fields.Method('get_last_message')

    def get_last_message(self, obj):
        message = obj.messages.order_by(text('created desc')).first()

        if message:
            return {
                'message': message.message,
                'created': message.created
            }
        else:
            return None

class ChatMessageSchema(Schema):
    id = ReverseId(dump_only=True)
    message = fields.Str()
    latitude = fields.Decimal(as_string=True)
    longitude = fields.Decimal(as_string=True)
    mapurl = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    medias = fields.Nested('MediaChatSchema', many=True)
    chat = fields.Nested('ChatSchema', attribute='chat')
    user = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'bank_accounts', 'childs', ))

class InfoSchema(Schema):
    id = ReverseId(dump_only=True)
    title = fields.Str()
    slug = fields.Str()
    body = fields.Str()
    latitude = fields.Decimal()
    longitude = fields.Decimal()
    created = fields.DateTime()
    updated = fields.DateTime()
    medias = fields.Nested('MediaInfoSchema', many=True)
    user = fields.Nested('UserSchema', attribute="info_user", exclude=('residentials', 'residential', 'bank_accounts', 'childs', ))
    total_comment = fields.Method('get_total_comment')

    def get_total_comment(self, obj):
        return obj.comments.count()


class CommentSchema(Schema):
    id = ReverseId(dump_only=True)
    comment = fields.Str()
    latitude = fields.Decimal()
    longitude = fields.Decimal()
    mapurl = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    medias = fields.Nested('MediaCommentSchema', many=True)
    user = fields.Nested('UserSchema', attribute='comment_user', exclude=('residentials', 'residential', 'bank_accounts', 'childs', ))
    info = fields.Nested('InfoSchema', attribute='comment_info', only=('id','title','body','updated', ))
    announcement = fields.Nested('AnnouncementSchema', attribute='comment_announcement', only=('id','title','body','updated', ))
    residentreport = fields.Nested('ResidentReportSchema', attribute='comment_resident_report', only=('id','title','body','updated', ))
    panic = fields.Nested('PanicSchema', attribute='comment_panic', only=('id','name','description','updated', ))
    freebies = fields.Nested('FreebiesSchema', attribute='comment_freebies', only=('id','title','description','updated', ))


class SignedBySchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    position = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    announcement = fields.Nested('AnnouncementSchema', attribute='signed_announcement', exclude=('signs', ))
    financial_report = fields.Nested('FinancialReportSchema', attribute='signed_financial_report', exclude=('signs', ))
    inventory_movement = fields.Nested('InventoryMovementSchema', attribute='signed_inventory_movement', exclude=('signs', ))


class ResidentialSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    address = fields.Str()
    path = fields.Str()
    polygon = fields.Str()
    note = fields.Str()
    restricted = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    medias = fields.Nested('MediaResidentialSchema', many=True)
    region = fields.Nested('RegionSchema', attribute='residential_region')
    administrative_region = fields.Method('get_region')    
    has_cctv = fields.Method('get_total_cctv')
    panics = fields.Nested('PanicSchema', many=True, exclude=('residentials', ))
    owner = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'bank_accounts', 'childs', ))
    users = fields.Nested('UserSchema', attribute='users', many=True, exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))

    def get_region(self, obj):
        return get_administrative_region(obj.residential_region)

    def get_total_cctv(self, obj):
        return len(obj.cctv.all()) > 0  


class CCTVSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    url = fields.Str()
    latitude = fields.Decimal()
    longitude = fields.Decimal()
    archived = fields.Bool(attribute='isdeleted')
    status = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    residential = fields.Nested('ResidentialSchema', attribute='cctv_residential', exclude=('owner', 'panics', 'medias', 'region', 'users', ))
    author = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))


class PanicSchema(Schema):
    id = ReverseId(dump_only=True)
    panic_group = fields.Str()
    name = fields.Str()
    slug = fields.Str()
    icon = fields.Str()
    restricted = fields.Bool()
    live_location = fields.Bool()
    description = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    residentials = fields.Nested('ResidentialSchema', attribute='panics_residentials', many=True, exclude=('owner', 'panics', 'medias', 'region', 'users', ))
    actions = fields.Nested('PanicActionSchema', many=True, exclude=('panic', ))
    participants = fields.Nested('PanicParticipantSchema', many=True, exclude=('panic', ))
    reports = fields.Nested('PanicReportSchema', many=True, exclude=('panic', ))
    author = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    owner = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    media = fields.Nested('MediaPanicSchema')
    total_report = fields.Method('get_total_report')
    total_comment = fields.Method('get_total_comment')
    total_participant = fields.Method('get_total_participant')

    def get_total_report(self, obj):
        return obj.reports.count()

    def get_total_comment(self, obj):
        return obj.comments.count()

    def get_total_participant(self, obj):
        return obj.participants.count()


class PanicParticipantSchema(Schema):
    id = ReverseId(dump_only=True)
    status = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    panic = fields.Nested('PanicSchema', attribute='participant_panic', exclude=('participants', ))
    user = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))


class PanicActionSchema(Schema):
    id = ReverseId(dump_only=True)
    action = fields.Str()
    phone = DenormalizePhone()
    alt_phone = DenormalizePhone()
    created = fields.DateTime()
    updated = fields.DateTime()
    panic = fields.Nested('PanicSchema', attribute='action_panic', exclude=('actions', ))
    reports = fields.Nested('PanicReportSchema', many=True, exclude=('action', ))
    role = fields.Nested('RoleSchema', attribute='action_role')


class PanicReportSchema(Schema):
    id = ReverseId(dump_only=True)
    number = fields.Str()
    latitude = fields.Decimal()
    longitude = fields.Decimal()
    mapurl = fields.Str()
    performance = fields.Int()
    note = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    status = fields.Str()
    live_expired = fields.Int()
    reporter = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    panic = fields.Nested('PanicSchema', attribute='report_panic', exclude=('reports', 'actions', 'residentials'))
    action = fields.Nested('PanicActionSchema', attribute='report_action', exclude=('reports', 'panic', ))
    residential = fields.Nested('ResidentialSchema', attribute='panic_report_residential', exclude=('reports', 'panics', 'owner', 'panics', 'medias', 'region', 'users', ))
    owner = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))


class AnnouncementSchema(Schema):
    id = ReverseId(dump_only=True)
    number = fields.Str()
    title = fields.Str()
    slug = fields.Str()
    body = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    status = fields.Str()
    author = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    category = fields.Nested('CategorySchema', attribute='announcement_category')
    medias = fields.Nested('MediaAnnouncementSchema', many=True)
    residentials = fields.Nested('ResidentialSchema', many=True, attribute='announcements_residentials', exclude=('owner', 'panics', 'medias', 'region', 'users', ))
    signs = fields.Nested('SignedBySchema', many=True, exclude=('announcement', ))
    owner = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    total_comment = fields.Method('get_total_comment')

    def get_total_comment(self, obj):
        return obj.comments.count()



class ResidentReportSchema(Schema):
    id = ReverseId(dump_only=True)
    number = fields.Str()
    title = fields.Str()
    slug = fields.Str()
    body = fields.Str()
    latitude = fields.Decimal()
    longitude = fields.Decimal()
    status = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    author = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    category = fields.Nested('CategorySchema', attribute='resident_report_category')
    medias = fields.Nested('MediaAnnouncementSchema', many=True)
    residential = fields.Nested('ResidentialSchema', attribute='resident_report_residential', exclude=('owner', 'panics', 'medias', 'region', 'users', ))
    owner = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    total_comment = fields.Method('get_total_comment')

    def get_total_comment(self, obj):
        return obj.comments.count()



class FinancialReportSchema(Schema):
    id = ReverseId(dump_only=True)
    trx_type = fields.Str()
    number = fields.Str()
    title = fields.Str()
    slug = fields.Str()
    debit = fields.Int()
    credit = fields.Int()
    recepient = fields.Str()
    period = fields.Date()
    note = fields.Str()
    status = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    residential = fields.Nested('ResidentialSchema', attribute='financial_report_residential', exclude=('owner', 'panics', 'medias', 'region', 'users', ))
    category = fields.Nested('CategorySchema', attribute='financial_report_category')
    medias = fields.Nested('MediaFinancialReportSchema', many=True)
    author = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    owner = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    balance = fields.Integer()
    signs = fields.Nested('SignedBySchema', many=True, exclude=('financial_report', ))


class InventorySchema(Schema):
    id = ReverseId(dump_only=True)
    number = fields.Str()
    title = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    received_date = fields.Date()
    quantity = fields.Int()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    residential = fields.Nested('ResidentialSchema', attribute='inventory_residential', exclude=('owner', 'panics', 'medias', 'region', 'users', ))
    category = fields.Nested('CategorySchema', attribute='inventory_category')
    medias = fields.Nested('MediaInventorySchema', many=True)
    movements = fields.Nested('InventoryMovementSchema', many=True)
    author = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    owner = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    unit = fields.Nested('UnitSchema', attribute='inventory_unit')


class InventoryMovementSchema(Schema):
    id = ReverseId(dump_only=True)
    number = fields.Str()
    name = fields.Str()
    address = fields.Str()
    phone = fields.Str()
    start_date = fields.Date()
    end_date = fields.Date()
    quantity = fields.Int()
    cost = fields.Integer()
    description = fields.Str()
    status = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    inventory = fields.Nested('InventorySchema', attribute='movement_inventory', exclude=('movements', ))
    region = fields.Nested('RegionSchema', attribute='inventory_movement_region')
    administrative_region = fields.Method('get_region')
    residential = fields.Nested('ResidentialSchema', attribute='inventory_movement_residential', exclude=('owner', 'panics', 'medias', 'region', 'users', ))
    owner = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    author = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', )) 
    signs = fields.Nested('SignedBySchema', many=True, exclude=('inventory_movement', )) 

    def get_region(self, obj):
        return get_administrative_region(obj.inventory_movement_region)