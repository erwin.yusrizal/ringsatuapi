import arrow

from api.v1.models.residentials import (
    ResidentialModel
)

from marshmallow import (
    Schema, 
    fields
)
from api.v1.utils import (
    reverse_id, 
    get_administrative_region,
    denormalize_phone_number,
    initial
)

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)

class DenormalizePhone(fields.Field):
    def _serialize(self, value, attr, obj):
        if value:
            return denormalize_phone_number(value)
        return value


class PermissionSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    shared = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()


class RoleSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    permissions = fields.Dict()
    created = fields.DateTime()
    updated = fields.DateTime()
    total_user = fields.Method('count_total_user')
    owner = fields.Nested('UserSchema', exclude=('role', ))

    def count_total_user(self, obj):
        return len(obj.role_users)


class ProfileSchema(Schema):
    id = ReverseId(dump_only=True)
    fullname = fields.Str()
    birthday = fields.Date()
    gender = fields.Str()
    phone = DenormalizePhone()
    address = fields.Str()
    profile_region_id = ReverseId()
    idcard = fields.Str()
    idcard_number = fields.Str()
    residential_status = fields.Str()
    landlord_name = fields.Str()
    landlord_phone = DenormalizePhone()
    landlord_address = fields.Str()
    landlord_region_id = ReverseId()
    store_name = fields.Str()
    store_about = fields.Str()
    store_open = fields.Str()
    store_close = fields.Str()
    invitable = fields.Bool()
    balance = fields.Int()
    point = fields.Int()
    latitude = fields.Decimal()
    longitude = fields.Decimal()
    created = fields.DateTime()
    updated = fields.DateTime()
    media = fields.Nested('MediaProfileSchema', many=False)    
    profile_region = fields.Method('get_profile_region')
    landlord_region = fields.Method('get_landlord_region')
    initial = fields.Method('get_initial')

    def get_profile_region(self, obj):
        return get_administrative_region(obj.profile_region)

    def get_landlord_region(self, obj):
        return get_administrative_region(obj.landlord_region)

    def get_initial(self, obj):
        return initial(obj.fullname)


class UserSchema(Schema):
    id = ReverseId(dump_only=True)
    nid = fields.Str()
    phone = DenormalizePhone()
    status = fields.Str()
    relationship_status =  fields.Str()
    parent_id = ReverseId()
    isonline = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    parents = fields.Nested('self', exclude=('residential', 'residentials', 'bank_accounts', 'childs', ))
    childs = fields.Nested('self',many=True, exclude=('residential', 'residentials', 'bank_accounts', 'parents', ))
    role = fields.Nested('RoleSchema', exclude=('owner', ))
    profile = fields.Nested('ProfileSchema')
    bank_accounts = fields.Nested('BankAccountSchema', attribute='holder_banks', many=True, exclude=('holder', 'owner', ))
    residential = fields.Nested('ResidentialSchema', exclude=('users', 'owner', 'panics', 'medias', ))
    residentials = fields.Nested('ResidentialSchema', many=True, attribute='residentials', exclude=('owner', 'users', 'panics', 'medias', ))
    
    def get_residential(self, obj):
        print('user: ', obj.id, obj.residential)
        return dict()

    def get_residentials(self, obj):
        print('Owner: ', obj.id, obj.residentials)
        return []


class BankAccountSchema(Schema):
    id = ReverseId(dump_only=True)
    account_name = fields.Str()
    account_number = fields.Str()
    branch = fields.Str()
    description = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    bank_id = ReverseId()
    holder = fields.Nested('UserSchema', exclude=('bank_accounts', ))
    bank = fields.Nested('BankSchema', attribute='bank_account_bank')

class UserNotificationSchema(Schema):
    id = ReverseId(dump_only=True)
    nid = fields.Str()
    title = fields.Str()
    body = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    user = fields.Nested('UserSchema', exclude=('bank_accounts', ))