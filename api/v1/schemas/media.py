import arrow

from marshmallow import (
    Schema, 
    fields
)
from flask import url_for
from api.v1.utils import reverse_id

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)


class MediaURLField(fields.Field):
    def _serialize(self, value, attr, obj):

        url = dict(
            original=url_for('static', filename='uploads/tmp/' + obj.filename, _external=True)
        )
        
        if obj.profile_id:
            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/profiles/' + obj.filename, width=48, height=48, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/profiles/' + obj.filename, width=80, height=80, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/profiles/' + obj.filename, width=128, height=128, quality=95, _external=True),
                    original=url_for('static', filename='uploads/profiles/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/profiles/'+ obj.filename, _external=True)

        elif obj.announcement_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/announcements/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/announcements/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/announcements/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/announcements/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/announcements/'+ obj.filename, _external=True)

        elif obj.resident_report_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/residentreports/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/residentreports/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/residentreports/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/residentreports/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/residentreports/'+ obj.filename, _external=True)

        elif obj.financial_report_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/financials/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/financials/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/financials/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/financials/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/financials/'+ obj.filename, _external=True)

        elif obj.category_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/categories/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/categories/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/categories/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/categories/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/categories/'+ obj.filename, _external=True)

        elif obj.product_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/products/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/products/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/products/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/products/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/products/'+ obj.filename, _external=True)

        elif obj.inventory_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/inventories/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/inventories/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/inventories/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/inventories/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/inventories/'+ obj.filename, _external=True)

        elif obj.info_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/infos/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/infos/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/infos/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/infos/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/infos/'+ obj.filename, _external=True)

        elif obj.comment_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/comments/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/comments/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/comments/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/comments/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/comments/'+ obj.filename, _external=True)

        elif obj.message_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/messages/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/messages/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/messages/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/messages/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/messages/'+ obj.filename, _external=True)


        elif obj.panic_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/panics/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/panics/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/panics/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/panics/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/panics/'+ obj.filename, _external=True)


        elif obj.freebies_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/freebies/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/freebies/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/freebies/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/freebies/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/freebies/'+ obj.filename, _external=True)

        elif obj.residential_id:

            if obj.mimetype in ['image/jpeg', 'image/jpg', 'image/png']:
                url = dict(
                    small=url_for('images.crop', filename='uploads/residentials/' + obj.filename, width=300, height=300, quality=95, _external=True),
                    medium=url_for('images.crop', filename='uploads/residentials/' + obj.filename, width=600, height=600, quality=95, _external=True),
                    large=url_for('images.crop', filename='uploads/residentials/' + obj.filename, width=1024, height=1024, quality=95, _external=True),
                    original=url_for('static', filename='uploads/residentials/' + obj.filename, _external=True)
                )
            else:
                url = url_for('static', filename='uploads/residentials/'+ obj.filename, _external=True)

        return url


class MediaTmpSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='id')


class MediaResidentialSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='residential_id')


class MediaProfileSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='profile_id')


class MediaAnnouncementSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='announcement_id')


class MediaResidentReportSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='resident_report_id')


class MediaFinancialReportSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='financial_report_id')


class MediaInventorySchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='inventory_id')


class MediaProductSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='product_id')


class MediaCategorySchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='category_id')


class MediaInfoSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='info_id')


class MediaCommentSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='comment_id')


class MediaChatSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='message_id')


class MediaPanicSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='panic_id')


class MediaFreebiesSchema(Schema):
    
    id = ReverseId(dump_only=True)
    filename = fields.Str()
    original_filename = fields.Str()
    size = fields.Str()
    mimetype = fields.Str()
    ismain = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    url = MediaURLField(attribute='freebies_id')