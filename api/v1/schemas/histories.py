import arrow

from marshmallow import (
    Schema, 
    fields
)
from api.v1.utils import reverse_id

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)


class HistorySchema(Schema):

    id = ReverseId(dump_only=True)
    module_id = fields.Int()
    module = fields.Str()
    action = fields.Str()
    content = fields.Dict()
    remote_address = fields.Str()
    user_agent = fields.Str()
    created = fields.DateTime()