import arrow

from marshmallow import (
    Schema, 
    fields
)
from api.v1.utils import (
    reverse_id, 
    get_administrative_region,
    denormalize_phone_number
)

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)

class DenormalizePhone(fields.Field):
    def _serialize(self, value, attr, obj):
        if value:
            return denormalize_phone_number(value)
        return value


class BankSchema(Schema):
    id = ReverseId(dump_only=True)
    code = fields.Str()
    name = fields.Str()
    slug = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    total_account = fields.Method('get_total_bank_account')

    def get_total_bank_account(self, obj):
        return obj.bank_accounts.count()


class RegionSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    path = fields.Str()
    latitude = fields.Decimal(as_string=True)
    longitude = fields.Decimal(as_string=True)
    polygon = fields.Str()
    postal = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    parents = fields.Nested('self')
    created = fields.DateTime()
    updated = fields.DateTime()
    administrative = fields.Method('get_administrative')

    def get_administrative(self, obj):
        return get_administrative_region(obj)


class PaymentMethodSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    charge = fields.Int()
    charge_percent = fields.Int()
    fee = fields.Int()
    fee_percent = fields.Int()
    description = fields.Str()
    isdefault = fields.Bool()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    owner = fields.Nested('UserSchema', exclude=('childs', ))
    author = fields.Nested('UserSchema', exclude=('childs', ))


class UnitSchema(Schema):
    id = ReverseId(dump_only=True)
    name = fields.Str()
    slug = fields.Str()
    symbol = fields.Str()
    description = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    owner = fields.Nested('UserSchema', exclude=('childs', ))
    author = fields.Nested('UserSchema', exclude=('childs', ))


class ContactSchema(Schema):
    id = ReverseId(dump_only=True)
    title = fields.Str()
    slug = fields.Str()
    fullname = fields.Str()
    address = fields.Str()
    phone = DenormalizePhone()
    alt_phone = DenormalizePhone()
    email = fields.Str()
    description = fields.Str()
    latitude = fields.Decimal(as_string=True)
    longitude = fields.Decimal(as_string=True)
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    region = fields.Nested('RegionSchema', attribute='contact_region')
    owner = fields.Nested('UserSchema', exclude=('childs', ))
    author = fields.Nested('UserSchema', exclude=('childs', ))

    def get_region(self, obj):
        return get_administrative_region(obj.contact_region)


class CategorySchema(Schema):
    id = ReverseId(dump_only=True)
    module = fields.Str()
    name = fields.Str()
    slug = fields.Str()
    path = fields.Str()
    icon = fields.Str()
    polygon = fields.Str()
    description = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    parents = fields.Nested('self', exclude=('childs', 'author', 'owner'))
    childs = fields.Nested('self', many=True, exclude=('parents', 'author', 'owner' ))
    media = fields.Nested('MediaCategorySchema')
    author = fields.Nested('UserSchema', exclude=('childs', ))
    owner = fields.Nested('UserSchema', exclude=('childs', ))
    total_product = fields.Method('get_total_product')

    def get_total_product(self, obj):
        return obj.products.count()