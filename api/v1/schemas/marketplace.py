import arrow

from marshmallow import (
    Schema, 
    fields
)
from api.v1.utils import (
    reverse_id, 
    get_administrative_region
)

class ReverseId(fields.Field):
    def _serialize(self, value, attr, obj):
        if not isinstance(value, (int, long)):
            return value
        return reverse_id(value)

class FreebiesSchema(Schema):
    id = ReverseId(dump_only=True)
    title = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    status = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    author = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    medias = fields.Nested('MediaFreebiesSchema', many=True)
    total_comment = fields.Method('get_total_comment')

    def get_total_comment(self, obj):
        return obj.comments.count()

class ProductSchema(Schema):
    id = ReverseId(dump_only=True)
    conditions = fields.Str()
    title = fields.Str()
    slug = fields.Str()
    description = fields.Str()
    minorder = fields.Int()
    price = fields.Int()
    delivery_cost = fields.Int()
    discount = fields.Int()
    isnegotiable = fields.Bool()
    isdeliverable = fields.Bool()
    status = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    created = fields.DateTime()
    updated = fields.DateTime()
    category = fields.Nested('CategorySchema', attribute='product_category')
    unit = fields.Nested('UnitSchema', attribute='product_unit')
    author = fields.Nested('UserSchema', exclude=('residentials', 'residential', 'childs', 'bank_accounts', ))
    medias = fields.Nested('MediaProductSchema', many=True)
    total_comment = fields.Method('get_total_comment')

    def get_total_comment(self, obj):
        return obj.comments.count()


class OrderSchema(Schema):
    id = ReverseId(dump_only=True)
    po = fields.Str()
    expired = fields.Int()
    reason = fields.Str()
    archived = fields.Bool(attribute='isdeleted')
    status = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    details = fields.Nested('OrderDetailSchema', many=True)
    buyer = fields.Nested('UserSchema')
    seller = fields.Nested('UserSchema')

class OrderDetailSchema(Schema):
    id = ReverseId(dump_only=True)
    quantity = fields.Int()
    price = fields.Int()
    discount = fields.Int()
    cost = fields.Int()
    total = fields.Int()
    note = fields.Str()
    created = fields.DateTime()
    updated = fields.DateTime()
    product = fields.Nested('ProductSchema', attribute='order_product')
    order = fields.Nested('OrderSchema', exclude=('details'))