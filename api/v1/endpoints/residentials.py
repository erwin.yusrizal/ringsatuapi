# -*- coding: utf-8 -*-
import arrow
import bleach
import time
import re
import os
import json
import requests
from urlparse import urlparse

import googlemaps

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)

from flask import (
    g,
    current_app,
    jsonify,
    Response, 
    send_file,
    stream_with_context
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    use_kwargs,
    parser
)
from slugify import slugify
from sqlalchemy import extract
from sqlalchemy.sql.expression import func, and_, or_

from api import cache, db, jwt, mail
from api.v1 import api

from api.v1.models.media import(
    MediaModel
)
from api.v1.models.users import(
    UserModel,
    UserNotificationModel
)
from api.v1.models.residentials import (
    SignedByModel,
    ResidentialModel,
    PanicModel,
    PanicActionModel,
    PanicReportModel,
    PanicParticipantModel,
    CCTVModel,    
    AnnouncementModel,
    ResidentReportModel,
    FinancialReportModel,
    InventoryModel,
    InventoryMovementModel,
    InfoModel,
    CommentModel,
    ChatModel,
    ChatMessageModel,
    LiveLocationModel
)
from api.v1.schemas.residentials import(
    ResidentialSchema,
    PanicSchema,
    PanicReportSchema,
    PanicParticipantSchema,
    CCTVSchema,    
    AnnouncementSchema,
    ResidentReportSchema,
    FinancialReportSchema,
    InventorySchema,
    InventoryMovementSchema,
    InfoSchema,
    CommentSchema,
    ChatSchema,
    ChatMessageSchema,
    LiveLocationSchema
)
from api.v1.utils import(
    reverse_id,
    is_dir_exist,
    is_file_exist,
    generate_unique_id,
    normalize_phone_number,
    denormalize_phone_number,
    valid_url,
    restricted_area,
    generate_stream
)
from api.v1.schemas.media import(
    MediaProfileSchema,
    MediaChatSchema,
    MediaCommentSchema
)
from api.v1.utils.pagination import (
    Pagination
)
from api.v1.utils.pushersocket import (
    PusherSocket
)
from api.v1.decorators import (
    permission_required,
    current_user,
    ownership
)
from api.v1.tasks import (
    sendnotif_queue
)
import logging

# Comment out this line to hide the log lines
logging.basicConfig(level=logging.DEBUG)


class LiveLocation(Resource):
    '''
    @method POST
    @endpoint /v1/livelocation
    @permission jwt_required
    @return live location object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {        
        'latitude': fields.Decimal(required=True),
        'longitude': fields.Decimal(required=True),
        'altitude': fields.Int(missing=None),
        'accuracy': fields.Int(missing=None),
        'speed': fields.Int(missing=None),
        'time': fields.Int(missing=None),
        'panic': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('panic', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create Live Location '''

        live = LiveLocationModel()
        live.latitude = payload['latitude']
        live.longitude = payload['longitude']
        live.altitude = payload['altitude']
        live.accuracy = payload['accuracy']
        live.speed = payload['speed']
        live.time = payload['time']
        live.panic_id = reverse_id(payload['panic']) if payload['panic'] else None
        live.user_id = g.current_user.id

        db.session.add(live)
        db.session.commit()

        schema = LiveLocationSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(live).data

        if live.panic_id:

            panic = PanicModel.query.get(live.panic_id)
            ids = []
            participants = panic.participants.all()

            for participant in participants:
                if participant.user_id != g.current_user.id:
                    ids.append(reverse_id(participant.user_id))

            pusher = PusherSocket()
            pusher.trigger('panic', 'livelocation', {
                'ids': ids,
                'user_id': reverse_id(live.user_id),
                'panic': reverse_id(live.panic_id) if live.panic_id else None,
                'latitude': str(live.latitude),
                'longitude': str(live.longitude),
                'altitude': live.altitude,
                'speed': live.speed
            })


        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'livelocation': result
            }
        }), 200


api.add_resource(LiveLocation, '/livelocation')


class Residentials(Resource):
    
    '''
    @method GET
    @endpoint /v1/residentials
    @permission jwt_required
    @return residentials list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'region': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residential', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Residentials '''

        q = args['q']
        region_id = reverse_id(args['region']) if args['region'] else None
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = ResidentialModel.count_all(q, region_id, g.owner_id, isdeleted)
        query = ResidentialModel.get_all(q, region_id, g.owner_id, isdeleted, page, perpage)

        schema = ResidentialSchema(only=args['only'], exclude=args['exclude'], many=True)
        residentials = schema.dump(query).data
        pagination = Pagination('api.residentials', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'residentials': residentials,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/residentials
    @permission jwt_required
    @return new residential object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama perumahan maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama perumahan maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'address': fields.Str(required=True, validate=[
            validate_required,
            validate.Length(max=255, error='Panjang alamat maksimal 255 karakter.')
        ]),
        'path': fields.Str(required=True, validate=[
            validate_required,
            validate.Length(max=255, error='Panjang path maksimal 255 karakter.')
        ]),
        'polygon': fields.Str(missing=None),
        'note': fields.Str(missing=None),
        'region': fields.Int(required=True),
        'restricted': fields.Bool(missing=False),
        'panics': fields.List(fields.Int(), missing=None),
        'owner': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residential', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Residential '''

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        address = bleach.clean(payload['address'], strip=True)
        path = payload['path']
        polygon = payload['polygon']
        note = bleach.clean(payload['note'], strip=True) if payload['note'] else None
        region_id = reverse_id(payload['region'])
        restricted = payload['restricted']
        panic_ids = [reverse_id(x) for x in payload['panics']] if payload['panics'] else None
        owner = reverse_id(payload['owner']) if payload['owner'] else None

        if g.current_user.role.slug == 'root' and not owner:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'owner': ['Silahkan pilih akun pemilik.']
                    }
                }
            }), 422

        elif g.current_user.role.slug == 'owner' and not owner:
            g.owner_id = g.current_user.id
        else:
            g.owner_id = owner

        if restricted and not polygon:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'polygon': ['Batas wilayah perumahan wajib ditentukan.']
                    }
                }
            }), 422

        residential_exist = ResidentialModel.query.filter(ResidentialModel.slug==slug, ResidentialModel.region_id==region_id)

        if g.owner_id:
            residential_exist = residential_exist.filter(ResidentialModel.owner_id==g.owner_id)

        if residential_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Nama perumahan sudah terdaftar.']
                    }
                }
            }), 422

        residential = ResidentialModel()
        residential.name = name
        residential.address = address
        residential.slug = slug
        residential.path = path
        residential.note = note
        residential.restricted = restricted
        residential.polygon = polygon
        residential.region_id = region_id
        residential.owner_id = g.owner_id
        residential.isdeleted = False

        if panic_ids:
            for panic_id in panic_ids:
                panic = PanicModel.query.get(panic_id)
                if panic:
                    residential.panics.append(panic)

        db.session.add(residential)
        db.session.commit()

        schema = ResidentialSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(residential).data

        return jsonify({
            'success': True,
            'message': 'Perumahan berhasil ditambahkan.',
            'data': {
                'residential': result
            }
        }), 201


api.add_resource(Residentials, '/residentials')


class Residential(Resource):
    
    '''
    @method GET
    @endpoint /v1/residentials/<int:residential_id>
    @permission jwt_required
    @return residential list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residential', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, residential_id):

        ''' View Residential '''

        residential_id = reverse_id(residential_id)
        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = ResidentialSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(residential).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'residential': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/residentials/<int:residential_id>
    @permission jwt_required
    @return updated residential object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama perumahan maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama perumahan maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'address': fields.Str(required=True, validate=[
            validate_required,
            validate.Length(max=255, error='Panjang alamat maksimal 255 karakter.')
        ]),
        'path': fields.Str(required=True, validate=[
            validate_required,
            validate.Length(max=255, error='Panjang path maksimal 255 karakter.')
        ]),
        'polygon': fields.Str(missing=None),
        'note': fields.Str(missing=None),
        'region': fields.Int(required=True),
        'restricted': fields.Bool(missing=False),
        'archived': fields.Bool(missing=False),
        'panics': fields.List(fields.Int(), missing=None),
        'owner': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residential', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, residential_id):

        ''' Edit Residential '''

        residential_id = reverse_id(residential_id)
        residential = ResidentialModel.get_by(residential_id, g.owner_id)

        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        address = bleach.clean(payload['address'], strip=True)
        path = payload['path']
        polygon = payload['polygon']
        note = bleach.clean(payload['note'], strip=True) if payload['note'] else None
        region_id = reverse_id(payload['region'])
        restricted = payload['restricted']
        isdeleted = payload['archived']
        panic_ids = [reverse_id(x) for x in payload['panics']] if payload['panics'] else None
        owner = reverse_id(payload['owner']) if payload['owner'] else None

        if g.current_user.role.slug == 'root' and not owner:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'owner': ['Silahkan pilih akun pemilik.']
                    }
                }
            }), 422

        elif g.current_user.role.slug == 'owner' and not owner:
            g.owner_id = g.current_user.id
        else:
            g.owner_id = owner

        if restricted and not polygon:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'polygon': ['Batas wilayah perumahan wajib ditentukan.']
                    }
                }
            }), 422

        residential_exist = ResidentialModel.query.filter(ResidentialModel.slug==slug, ResidentialModel.region_id==region_id, ResidentialModel.id!=residential.id)

        if g.owner_id:
            residential_exist = residential_exist.filter(ResidentialModel.owner_id==g.owner_id)

        if residential_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Nama perumahan sudah terdaftar.']
                    }
                }
            }), 422

        residential.name = name
        residential.address = address
        residential.slug = slug
        residential.path = path
        residential.note = note
        residential.restricted = restricted
        residential.polygon = polygon
        residential.region_id = region_id
        residential.isdeleted = isdeleted

        if panic_ids:
            for panic_id in panic_ids:
                panic = PanicModel.query.get(panic_id)
                if panic:
                    residential.panics.append(panic)

        db.session.commit()

        schema = ResidentialSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(residential).data

        return jsonify({
            'success': True,
            'message': 'Perumahan berhasil diubah.',
            'data': {
                'residential': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/residentials/<int:residential_id>
    @permission jwt_required
    @return deleted residential object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residential', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, residential_id):

        ''' Delete Residential '''

        residential_id = reverse_id(residential_id)
        residential = ResidentialModel.get_by(residential_id, g.owner_id)

        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = ResidentialSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(residential).data

        if residential.isdeleted:
            db.session.delete(residential)
        else:
            residential.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Perumahan berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'residential': result
            }
        }), 200


api.add_resource(Residential, '/residentials/<int:residential_id>')


class Announcements(Resource):
    
    '''
    @method GET
    @endpoint /v1/announcements
    @permission jwt_required
    @return announcements list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dipublikasikan'], error='Pilihan status tidak valid.')
        ], missing=None),
        'author': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'category': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('announcement', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Announcements '''

        q = args['q']
        status = args['status']
        author_id = reverse_id(args['author']) if args['author'] else None
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        category_id = reverse_id(args['category']) if args['category'] else None
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        if g.current_user.role.slug not in ['root', 'owner'] and not residential_id:
            residential_id = g.current_user.residential_id
        
        total = AnnouncementModel.count_all(q, status, author_id, residential_id, category_id, g.owner_id, isdeleted)
        query = AnnouncementModel.get_all(q, status, author_id, residential_id, category_id, g.owner_id, isdeleted, page, perpage)

        schema = AnnouncementSchema(only=args['only'], exclude=args['exclude'], many=True)
        announcements = schema.dump(query).data
        pagination = Pagination('api.announcements', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'announcements': announcements,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/announcements
    @permission jwt_required
    @return new announcement object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,?]{1,255})$', error='Panjang judul pemberitahuan maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul pemberitahuan maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'body': fields.Str(required=True, validate=[validate_required]),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dipublikasikan'], error='Pilihan status tidak valid.')
        ]),
        'category': fields.Int(required=True),
        'residentials': fields.List(fields.Int(), missing=None),
        'signedby': fields.List(fields.Dict(), required=True),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('announcement', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Annnouncement '''

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        body = bleach.clean(payload['body'], strip=True, tags=['strong', 'p', 'i', 'ol', 'ul', 'li', 'h1', 'h2', 'h3'], attributes=['style'], styles=['text-align'])
        status = payload['status']
        category_id = reverse_id(payload['category'])
        residential_ids = [reverse_id(x) for x in payload['residentials']] if payload['residentials'] else None 
        signedby = payload['signedby'] 
        media_ids = payload['medias']

        announcement_exist = AnnouncementModel.query.filter(AnnouncementModel.slug==slug)

        if g.owner_id:
            announcement_exist = announcement_exist.filter(AnnouncementModel.owner_id==g.owner_id)

        if announcement_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'announcement': ['Pemberitahuan sudah terdaftar.']
                    }
                }
            }), 422        

        now = arrow.get().format('YYYY/MM/DD')

        announcement = AnnouncementModel()
        announcement.number = 'PEM/' + now + '/' + generate_unique_id(5)
        announcement.title = title
        announcement.slug = slug
        announcement.body = body
        announcement.status = status
        announcement.category_id = category_id
        announcement.author_id = g.current_user.id
        announcement.owner_id = g.owner_id or g.current_user.id
        announcement.isdeleted = False

        db.session.add(announcement)
        db.session.flush()

        if residential_ids:
            for residential_id in residential_ids:
                residential = ResidentialModel.query.get(residential_id)
                if residential:
                    residential.announcements.append(announcement)

        elif g.current_user.role.slug not in['root', 'owner']:
            residential = ResidentialModel.query.get(g.current_user.residential_id)
            if residential:
                residential.announcements.append(announcement)


        if signedby:
            for sb in signedby:

                signer_name = bleach.clean(sb['name'], strip=True)

                signed = SignedByModel()
                signed.name = signer_name
                signed.slug = slugify(signer_name)
                signed.position = bleach.clean(sb['position'], strip=True)
                
                announcement.signs.append(signed)

        db.session.commit()

        if media_ids:

            for media_id in media_ids:
                media_id = reverse_id(media_id)
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
                    media.announcement_id = announcement.id
                    media.residential_id = residential_id

                    media.owner_id = g.owner_id

                    media.isorphan = False
                    media.expired = None

                    file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                    new_path = os.path.join(current_app.config.get('ANNOUNCEMENT_FOLDER'), media.filename)

                    is_dir_exist(new_path)

                    if is_file_exist(file_path):
                        os.rename(file_path, new_path)

                    db.session.commit() 

        schema = AnnouncementSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(announcement).data

        # send notif via task queue manager based on residential
        # if residential is None, send notif to all users
        # if has residential, send notif to all selected residential user 
        if residential_ids:
            for residential_id in residential_ids:
                residential = ResidentialModel.query.get(residential_id)
                residents = residential.users

                for resident in residents:
                    notif = UserNotificationModel()
                    notif.nid = resident.nid
                    notif.title = 'RingSatu'
                    notif.body = 'Pengumuman '+ residential.name+ ': ' + announcement.title
                    notif.user_id = resident.id
                    notif.residential_id = residential_id

                    db.session.add(notif)
                    db.session.commit()

                    if resident.nid:
                        sendnotif_queue.apply_async(kwargs={
                            'player_ids': [resident.nid],
                            'title': 'RingSatu',
                            'body': 'Pengumuman '+ residential.name+ ': ' + announcement.title
                        }, countdown=60)

        elif g.current_user.role.slug not in['root', 'owner']:
            residential = ResidentialModel.query.get(g.current_user.residential_id)
            residents = residential.users

            for resident in residents:
                notif = UserNotificationModel()
                notif.nid = resident.nid
                notif.title = 'RingSatu'
                notif.body = 'Pengumuman '+ residential.name+ ': ' + announcement.title
                notif.user_id = resident.id
                notif.residential_id = residential_id

                db.session.add(notif)
                db.session.commit()

                if resident.nid:
                    sendnotif_queue.apply_async(kwargs={
                        'player_ids': [resident.nid],
                        'title': 'RingSatu',
                        'body': 'Pengumuman '+ residential.name+ ': ' + announcement.title
                    }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Pemberitahuan berhasil ditambahkan.',
            'data': {
                'announcement': result
            }
        }), 201


api.add_resource(Announcements, '/announcements')


class Annnouncement(Resource):
    
    '''
    @method GET
    @endpoint /v1/announcements/<int:announcement_id>
    @permission jwt_required
    @return announcement list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('announcement', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, announcement_id):

        ''' View announcement '''

        announcement_id = reverse_id(announcement_id)
        announcement = AnnouncementModel.get_by(announcement_id, g.owner_id)

        if not announcement:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'announcement': ['Pemberitahuan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = AnnouncementSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(announcement).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'announcement': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/announcements/<int:announcement_id>
    @permission jwt_required
    @return updated announcement object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,?]{1,255})$', error='Panjang judul pemberitahuan maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul pemberitahuan maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'body': fields.Str(required=True, validate=[validate_required]),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dipublikasikan'], error='Pilihan status tidak valid.')
        ]),
        'category': fields.Int(required=True),
        'residentials': fields.List(fields.Int(), missing=None),
        'signedby': fields.List(fields.Dict(), required=True),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('announcement', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, announcement_id):

        ''' Edit Announcement '''

        announcement_id = reverse_id(announcement_id)
        announcement = AnnouncementModel.get_by(announcement_id, g.owner_id)

        if not announcement:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'announcement': ['Pemberitahuan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        body = bleach.clean(payload['body'], strip=True, tags=['strong', 'p', 'i', 'ol', 'ul', 'li', 'h1', 'h2', 'h3'], attributes=['style'], styles=['text-align'])
        status = payload['status']
        category_id = reverse_id(payload['category'])
        residential_ids = [reverse_id(x) for x in payload['residentials']] if payload['residentials'] else None 
        signedby = payload['signedby'] 
        isdeleted = payload['archived']

        if g.current_user.role.slug == 'owner' and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        announcement_exist = AnnouncementModel.query.filter(AnnouncementModel.slug==slug, AnnouncementModel.residential_id==residential_id, AnnouncementModel.id!=announcement_id)

        if g.owner_id:
            announcement_exist = announcement_exist.filter(AnnouncementModel.owner_id==g.owner_id)

        if announcement_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'announcement': ['Pemberitahuan sudah terdaftar.']
                    }
                }
            }), 422        

        announcement.title = title
        announcement.slug = slug
        announcement.body = body
        Annnouncement.status = status
        announcement.category_id = category_id
        announcement.isdeleted = isdeleted

        if residential_ids:
            announcement.announcements_residentials = []
            for residential_id in residential_ids:
                residential = ResidentialModel.query.get(residential_id)
                if residential:
                    residential.announcements.append(announcement)
                    
        elif g.current_user.role.slug not in['root', 'owner']:
            residential = ResidentialModel.query.get(g.current_user.residential_id)
            if residential:
                residential.announcements.append(announcement)

        if signedby:
            announcement.signs = []            
            for sb in signedby:
                signer_name = bleach.clean(sb['name'], strip=True)

                signed = SignedByModel()
                signed.name = signer_name
                signed.slug = slugify(signer_name)
                signed.position = bleach.clean(sb['position'], strip=True)
                
                announcement.signs.append(signed)

        db.session.commit()

        schema = AnnouncementSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(announcement).data

        if residential_ids:
            for residential_id in residential_ids:
                residential = ResidentialModel.query.get(residential_id)
                residents = residential.users

                for resident in residents:
                    notif = UserNotificationModel()
                    notif.nid = resident.nid
                    notif.title = 'RingSatu'
                    notif.body = '[Perbaikan] Pengumuman '+ residential.name+ ': ' + announcement.title
                    notif.user_id = resident.id
                    notif.residential_id = residential_id

                    db.session.add(notif)
                    db.session.commit()

                    if resident.nid:
                        sendnotif_queue.apply_async(kwargs={
                            'player_ids': [resident.nid],
                            'title': 'RingSatu',
                            'body': '[Perbaikan] Pengumuman '+ residential.name+ ': ' + announcement.title
                        }, countdown=60)

        elif g.current_user.role.slug not in['root', 'owner']:
            residential = ResidentialModel.query.get(g.current_user.residential_id)
            residents = residential.users

            for resident in residents:
                notif = UserNotificationModel()
                notif.nid = resident.nid
                notif.title = 'RingSatu'
                notif.body = '[Perbaikan] Pengumuman '+ residential.name+ ': ' + announcement.title
                notif.user_id = resident.id
                notif.residential_id = residential_id

                db.session.add(notif)
                db.session.commit()

                if resident.nid:
                    sendnotif_queue.apply_async(kwargs={
                        'player_ids': [resident.nid],
                        'title': 'RingSatu',
                        'body': '[Perbaikan] Pengumuman '+ residential.name+ ': ' + announcement.title
                    }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Pemberitahuan berhasil diubah.',
            'data': {
                'announcement': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/announcements/<int:announcement_id>
    @permission jwt_required
    @return deleted announcement object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('announcement', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, announcement_id):

        ''' Delete announcement '''

        announcement_id = reverse_id(announcement_id)
        announcement = AnnouncementModel.get_by(announcement_id, g.owner_id)

        if not announcement:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'announcement': ['Pemberitahuan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = AnnouncementSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(announcement).data

        if announcement.isdeleted:
            if announcement.medias:
                for media in announcement.medias:
                    os.remove(os.path.join(current_app.config.get('ANNOUNCEMENT_FOLDER'), media.filename))
            db.session.delete(announcement)
        else:
            announcement.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Pemberitahuan berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'announcement': result
            }
        }), 200


api.add_resource(Annnouncement, '/announcements/<int:announcement_id>')

class ResidentReports(Resource):
    
    '''
    @method GET
    @endpoint /v1/residentreports
    @permission jwt_required
    @return residentreport list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dipublikasikan'], error='Pilihan status tidak valid.')
        ], missing=None),
        'author': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'category': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residentreport', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Resident Report '''

        q = args['q']
        status = args['status']
        author_id = reverse_id(args['author']) if args['author'] else None
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        category_id = reverse_id(args['category']) if args['category'] else None
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        if g.current_user.role.slug not in ['root', 'owner'] and not residential_id:
            residential_id = g.current_user.residential_id

        total = ResidentReportModel.count_all(q, status, author_id, residential_id, category_id, g.owner_id, isdeleted)
        query = ResidentReportModel.get_all(q, status, author_id, residential_id, category_id, g.owner_id, isdeleted, page, perpage)

        schema = ResidentReportSchema(only=args['only'], exclude=args['exclude'], many=True)
        residentreports = schema.dump(query).data
        pagination = Pagination('api.residentreports', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'residentreports': residentreports,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/residentreports
    @permission jwt_required
    @return new residentreport object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,?]{1,255})$', error='Panjang judul laporan maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul laporan maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'body': fields.Str(required=True, validate=[validate_required]),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dipublikasikan'], error='Pilihan status tidak valid.')
        ]),
        'author': fields.Int(required=True),
        'category': fields.Int(required=True),
        'residential': fields.Int(missing=None),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residentreport', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Resident Report '''

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        body = bleach.clean(payload['body'], strip=True)
        latitude = payload['latitude']
        longitude = payload['longitude']
        status = payload['status']
        author_id = reverse_id(payload['author'])
        category_id = reverse_id(payload['category'])
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 
        media_ids = payload['medias']

        if not g.owner_id:
            author = UserModel.query.get(author_id)
            g.owner_id = author.parent_id if author.parent_id else author.id 

        if g.current_user.role.slug == 'owner' and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        residentreport_exist = ResidentReportModel.query.filter(ResidentReportModel.slug==slug, ResidentReportModel.residential_id==residential_id)

        if g.owner_id:
            residentreport_exist = residentreport_exist.filter(ResidentReportModel.owner_id==g.owner_id)

        if residentreport_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residentreport': ['Laporan warga sudah terdaftar.']
                    }
                }
            }), 422        

        now = arrow.get().format('YYYY/MM/DD')

        residentreport = ResidentReportModel()
        residentreport.number = 'LPW/' + now + '/' + generate_unique_id(5)
        residentreport.title = title
        residentreport.slug = slug
        residentreport.body = body
        residentreport.latitude = latitude
        residentreport.longitude = longitude
        residentreport.status = status
        residentreport.category_id = category_id
        residentreport.author_id = author_id
        residentreport.owner_id = g.owner_id
        residentreport.isdeleted = False
        residentreport.residential_id = residential_id

        db.session.add(residentreport)
        db.session.commit()

        if media_ids:

            for media_id in media_ids:
                media_id = reverse_id(media_id)
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
                    media.resident_report_id = residentreport.id
                    media.residential_id = residential_id

                    media.owner_id = g.owner_id

                    media.isorphan = False
                    media.expired = None

                    file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                    new_path = os.path.join(current_app.config.get('RESIDENT_REPORT_FOLDER'), media.filename)

                    is_dir_exist(new_path)

                    if is_file_exist(file_path):
                        os.rename(file_path, new_path)

                    db.session.commit() 

        schema = ResidentReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(residentreport).data

        # send notif via task queue manager based on residential
        residents = residential.users

        for resident in residents:
            notif = UserNotificationModel()
            notif.nid = resident.nid
            notif.title = 'RingSatu'
            notif.body = 'Laporan Warga '+ residential.name+ ': ' + residentreport.title
            notif.user_id = resident.id
            notif.residential_id = residential.id

            db.session.add(notif)
            db.session.commit()

            if resident.nid:
                sendnotif_queue.apply_async(kwargs={
                    'player_ids': [resident.nid],
                    'title': 'RingSatu',
                    'body': 'Laporan Warga '+ residential.name+ ': ' + residentreport.title
                }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Laporan warga berhasil ditambahkan.',
            'data': {
                'residentreport': result
            }
        }), 201


api.add_resource(ResidentReports, '/residentreports')


class ResidentReport(Resource):
    
    '''
    @method GET
    @endpoint /v1/residentreports/<int:residentreport_id>
    @permission jwt_required
    @return residentreport list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residentreport', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, residentreport_id):

        ''' View Resident Report '''

        residentreport_id = reverse_id(residentreport_id)
        residentreport = ResidentReportModel.get_by(residentreport_id, g.owner_id)

        if not residentreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residentreport': ['Pemberitahuan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = ResidentReportSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(residentreport).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'residentreport': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/residentreports/<int:residentreport_id>
    @permission jwt_required
    @return updated residentreport object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,?]{1,255})$', error='Panjang judul laporan maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul laporan maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'body': fields.Str(required=True, validate=[validate_required]),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dipublikasikan'], error='Pilihan status tidak valid.')
        ]),
        'author': fields.Int(required=True),
        'category': fields.Int(required=True),
        'residential': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residentreport', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, residentreport_id):

        ''' Edit Resident Report '''

        residentreport_id = reverse_id(residentreport_id)
        residentreport = ResidentReportModel.get_by(residentreport_id, g.owner_id)

        if not residentreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residentreport': ['Laporan warga tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        body = bleach.clean(payload['body'], strip=True)
        latitude = payload['latitude']
        longitude = payload['longitude']
        status = payload['status']
        author_id = reverse_id(payload['author'])
        category_id = reverse_id(payload['category'])
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 
        isdeleted = payload['archived']

        if g.current_user.role.slug == 'owner' and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        residentreport_exist = ResidentReportModel.query.filter(ResidentReportModel.slug==slug, ResidentReportModel.residential_id==residential_id, ResidentReportModel.id!=residentreport_id)

        if g.owner_id:
            residentreport_exist = residentreport_exist.filter(ResidentReportModel.owner_id==g.owner_id)

        if residentreport_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residentreport': ['Laporan warga sudah terdaftar.']
                    }
                }
            }), 422        


        residentreport.title = title
        residentreport.slug = slug
        residentreport.body = body
        residentreport.latitude = latitude
        residentreport.longitude = longitude
        residentreport.status = status
        residentreport.author_id = author_id
        residentreport.category_id = category_id
        residentreport.isdeleted = isdeleted
        residentreport.residential_id = residential_id

        db.session.commit()

        schema = ResidentReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(residentreport).data

        return jsonify({
            'success': True,
            'message': 'Laporan warga berhasil diubah.',
            'data': {
                'residentreport': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/residentreports/<int:residentreport_id>
    @permission jwt_required
    @return deleted residentreport object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('residentreport', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, residentreport_id):

        ''' Delete Resident Report '''

        residentreport_id = reverse_id(residentreport_id)
        residentreport = ResidentReportModel.get_by(residentreport_id, g.owner_id)

        if not residentreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residentreport': ['Laporan warga tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = ResidentReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(residentreport).data

        if residentreport.isdeleted:
            if residentreport.medias:
                for media in residentreport.medias:
                    os.remove(os.path.join(current_app.config.get('RESIDENT_REPORT_FOLDER'), media.filename))
            db.session.delete(residentreport)
        else:
            residentreport.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Laporan warga berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'residentreport': result
            }
        }), 200


api.add_resource(ResidentReport, '/residentreports/<int:residentreport_id>')


class Inventories(Resource):
    
    '''
    @method GET
    @endpoint /v1/inventory
    @permission jwt_required
    @return inventory list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(missing=None, validate=[
            validate.OneOf(choices=['waiting', 'onloan', 'rentedout', 'beingrepaired', 'outoforder', 'missing', 'broken', 'returned'], labels=['Tersedia', 'Sedang Dipinjam', 'Sedang Disewa', 'Sedang Diperbaiki', 'Rusak', 'Hilang', 'Rusak', 'Telah Dikembalikan'], error='Pilihan status tidak valid.')
        ]),
        'residential': fields.Int(missing=None),
        'category': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventory', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Inventory '''

        q = args['q']
        status = args['status']
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        category_id = reverse_id(args['category']) if args['category'] else None
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = InventoryModel.count_all(q, status, residential_id, category_id, g.owner_id, isdeleted)
        query = InventoryModel.get_all(q, status, residential_id, category_id, g.owner_id, isdeleted, page, perpage)

        schema = InventorySchema(only=args['only'], exclude=args['exclude'], many=True)
        inventories = schema.dump(query).data
        pagination = Pagination('api.inventories', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'inventories': inventories,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/inventories
    @permission jwt_required
    @return new inventory object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang judul inventaris maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul inventaris maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'quantity': fields.Int(required=True),
        'description': fields.Str(missing=None),
        'received_date': fields.Date(required=True),
        'category': fields.Int(required=True),
        'residential': fields.Int(missing=None),
        'author': fields.Int(missing=None),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventory', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Inventory '''

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        received_date = payload['received_date']
        quantity = payload['quantity']
        author_id = reverse_id(payload['author']) if payload['author'] else None
        category_id = reverse_id(payload['category'])
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 
        media_ids = payload['medias']

        if g.current_user.role.slug == 'owner' and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        if not author_id:
            author_id = g.current_user.id

        if not g.owner_id:
            author = UserModel.query.get(author_id)
            g.owner_id = author.parent_id if author.parent_id else author.id

        inventory_exist = InventoryModel.query.filter(InventoryModel.slug==slug, InventoryModel.residential_id==residential_id)

        if g.owner_id:
            inventory_exist = inventory_exist.filter(InventoryModel.owner_id==g.owner_id)

        if inventory_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'inventory': ['Inventaris sudah terdaftar.']
                    }
                }
            }), 422        

        now = arrow.get().format('YYYY/MM/DD')

        inventory = InventoryModel()
        inventory.number = 'INVT/' + now + '/' + generate_unique_id(5)
        inventory.title = title
        inventory.slug = slug
        inventory.description = description
        inventory.received_date = received_date
        inventory.quantity = quantity
        inventory.category_id = category_id
        inventory.author_id = author_id
        inventory.owner_id = g.owner_id
        inventory.isdeleted = False
        inventory.residential_id = residential_id

        db.session.add(inventory)
        db.session.commit()

        if media_ids:

            for media_id in media_ids:
                media_id = reverse_id(media_id)
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
                    media.inventory_id = inventory.id
                    media.residential_id = residential_id

                    media.owner_id = g.owner_id

                    media.isorphan = False
                    media.expired = None

                    file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                    new_path = os.path.join(current_app.config.get('INVENTORY_FOLDER'), media.filename)

                    is_dir_exist(new_path)

                    if is_file_exist(file_path):
                        os.rename(file_path, new_path)

                    db.session.commit() 

        schema = InventorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(inventory).data

        # send notif via task queue manager based on residential
        residents = residential.users
        if residents:
            for resident in residents:
                notif = UserNotificationModel()
                notif.nid = resident.nid
                notif.title = 'RingSatu'
                notif.body = 'Barang Inventaris Baru: ' + inventory.title
                notif.user_id = resident.id
                notif.residential_id = residential_id

                db.session.add(notif)
                db.session.commit()

                if resident.nid:
                    sendnotif_queue.apply_async(kwargs={
                        'player_ids': [resident.nid],
                        'title': 'RingSatu',
                        'body': 'Barang Inventaris Baru: ' + inventory.title
                    }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Inventaris berhasil ditambahkan.',
            'data': {
                'inventory': result
            }
        }), 201


api.add_resource(Inventories, '/inventory')


class Inventory(Resource):
    
    '''
    @method GET
    @endpoint /v1/inventory/<int:inventory_id>
    @permission jwt_required
    @return inventory list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventory', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, inventory_id):

        ''' View Inventory '''

        inventory_id = reverse_id(inventory_id)
        inventory = InventoryModel.get_by(inventory_id, g.owner_id)

        if not inventory:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'inventory': ['Inventaris tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = InventorySchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(inventory).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'inventory': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/inventory/<int:inventory_id>
    @permission jwt_required
    @return updated inventory object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang judul inventaris maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul inventaris maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'received_date': fields.Date(required=True),
        'quantity': fields.Int(required=True),
        'description': fields.Str(missing=None),
        'category': fields.Int(required=True),
        'residential': fields.Int(missing=None),
        'author': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventory', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, inventory_id):

        ''' Edit Resident Report '''

        inventory_id = reverse_id(inventory_id)
        inventory = InventoryModel.get_by(inventory_id, g.owner_id)

        if not inventory:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'inventory': ['Inventaris tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        received_date = payload['received_date']
        quantity = payload['quantity']
        category_id = reverse_id(payload['category'])
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 
        author_id = reverse_id(payload['author']) if payload['author'] else None 
        isdeleted = payload['archived']

        if g.current_user.role.slug == 'owner' and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        inventory_exist = InventoryModel.query.filter(InventoryModel.slug==slug, InventoryModel.residential_id==residential_id, InventoryModel.id!=inventory_id)

        if g.owner_id:
            inventory_exist = inventory_exist.filter(InventoryModel.owner_id==g.owner_id)

        if inventory_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'inventory': ['Inventaris sudah terdaftar.']
                    }
                }
            }), 422 

        inventory.title = title
        inventory.slug = slug
        inventory.description = description
        inventory.received_date = received_date
        inventory.quantity = quantity
        inventory.isdeleted = isdeleted
        inventory.residential_id = residential_id
        inventory.category_id = category_id

        if author_id != inventory.author_id:
            inventory.author_id = author_id

        db.session.commit()

        schema = InventorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(inventory).data

        return jsonify({
            'success': True,
            'message': 'Laporan warga berhasil diubah.',
            'data': {
                'inventory': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/inventory/<int:inventory_id>
    @permission jwt_required
    @return deleted inventory object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventory', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, inventory_id):

        ''' Delete Inventory '''

        inventory_id = reverse_id(inventory_id)
        inventory = InventoryModel.get_by(inventory_id, g.owner_id)

        if not inventory:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'inventory': ['Inventaris tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = InventorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(inventory).data

        if inventory.isdeleted:
            if inventory.medias:
                for media in inventory.medias:
                    os.remove(os.path.join(current_app.config.get('INVENTORY_FOLDER'), media.filename))
            db.session.delete(inventory)
        else:
            inventory.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Inventaris berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'inventory': result
            }
        }), 200


api.add_resource(Inventory, '/inventory/<int:inventory_id>')


class InventoryMovements(Resource):
    
    '''
    @method GET
    @endpoint /v1/inventory/movements
    @permission jwt_required
    @return inventory movement list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(missing=None, validate=validate.OneOf(choices=['waiting', 'onloan', 'rentedout', 'beingrepaired', 'broken', 'returned', 'missing', 'fixed'], labels=['Menunggu Persetujuan', 'Dipinjam', 'Disewa', 'Diperbaiki', 'Rusak', 'Hilang', 'Telah Dikembalikan', 'Telah Diperbaiki'], error='Pilihan status tidak valid.')),
        'duration': fields.List(fields.Date(), missing=None),
        'residential': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventorymovement', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Inventory Movement'''

        q = args['q']
        status = args['status']
        duration = args['duration']
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        start_date, end_date = [None, None]

        if duration:
            start_date, end_date = duration

        total = InventoryMovementModel.count_all(q, status, start_date, end_date, residential_id, g.owner_id, isdeleted)
        query = InventoryMovementModel.get_all(q, status, start_date, end_date, residential_id, g.owner_id, isdeleted, page, perpage)

        schema = InventoryMovementSchema(only=args['only'], exclude=args['exclude'], many=True)
        inventories = schema.dump(query).data
        pagination = Pagination('api.inventorymovements', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'movements': inventories,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/inventory/movements
    @permission jwt_required
    @return new inventory movement object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.') 

    def validate_phone(value):
        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', value)

        if phone_match is None:
            raise ValidationError('No. handphone tidak valid. Harus diawali dengan 08, +628 atau 628')  

    post_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'address': fields.Str(required=True, validate=[
            validate_required,
            validate.Length(max=255, error='Alamat maksimal 255 karakter.')
        ]),
        'phone': fields.Str(required=True, validate=[validate_required, validate_phone]),
        'duration': fields.List(fields.Date(), missing=None),
        'quantity': fields.Int(required=True),
        'cost': fields.Int(required=True),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi inventaris maksimal 255 karakter.')
        ]),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['waiting', 'onloan', 'rentedout', 'beingrepaired', 'broken', 'returned', 'missing', 'fixed'], labels=['Menunggu Persetujuan', 'Dipinjam', 'Disewa', 'Diperbaiki', 'Rusak', 'Hilang', 'Telah Dikembalikan', 'Telah Diperbaiki'], error='Pilihan status tidak valid.')
        ]),
        'inventory': fields.Int(required=True),
        'region': fields.Int(required=True),
        'residential': fields.Int(missing=None),
        'author': fields.Int(missing=None),
        'signedby': fields.List(fields.Dict(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventorymovement', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Inventory Movement '''

        name = bleach.clean(payload['name'], strip=True) if payload['name'] else None
        address = bleach.clean(payload['address']) if payload['address'] else None
        phone = payload['phone']
        duration = payload['duration']
        quantity = payload['quantity']
        cost = payload['cost']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        status = payload['status']
        inventory_id = reverse_id(payload['inventory'])
        region_id = reverse_id(payload['region']) if payload['region'] else None
        author_id = reverse_id(payload['author']) if payload['author'] else None
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 
        signedby = payload['signedby']

        start_date, end_date = [None, None]

        if duration:
            start_date, end_date = duration

        if not author_id:
            author_id = g.current_user.id

        if not g.owner_id:
            author = UserModel.query.get(author_id)
            g.owner_id = author.parent_id if author.parent_id else g.current_user.id

        if g.current_user.role.slug in ['root', 'owner'] and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        inventory = InventoryModel.query.get(inventory_id)
        if not inventory:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'inventory': ['Inventaris tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        total_movement = InventoryMovementModel.query.with_entities(func.sum(InventoryMovementModel.quantity).label('total')).filter(InventoryMovementModel.inventory_id==inventory_id, InventoryMovementModel.status.notin_(['broken','returned','fixed'])).first()

        available_inventory = None

        if total_movement[0] is not None:
            available_inventory = (inventory.quantity - total_movement[0])
        else:
            available_inventory = inventory.quantity

        if quantity > available_inventory:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'inventory': ['Inventaris dengan jumlah yang dimasukkan tidak tersedia.']
                    }
                }
            }), 422

        now = arrow.get().format('YYYY/MM/DD')

        movement = InventoryMovementModel()
        movement.number = 'MOVEMENT/' + now + '/' + generate_unique_id(5)
        movement.name = name
        movement.address = address
        movement.phone = phone
        movement.start_date = start_date
        movement.end_date = end_date
        movement.quantity = quantity
        movement.cost = cost
        movement.description = description
        movement.status = status
        movement.author_id = author_id
        movement.owner_id = g.owner_id
        movement.region_id = region_id
        movement.isdeleted = False
        movement.residential_id = residential_id
        movement.inventory_id = inventory.id

        db.session.add(movement)
        db.session.flush()

        if signedby:
            for sb in signedby:

                signer_name = bleach.clean(sb['name'], strip=True)

                signed = SignedByModel()
                signed.name = signer_name
                signed.slug = slugify(signer_name)
                signed.position = bleach.clean(sb['position'], strip=True)
                
                movement.signs.append(signed)

        db.session.commit()

        schema = InventoryMovementSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(movement).data

        # send notif via task queue manager based on residential
        organizers = UserModel.query.filter(UserModel.residential_id==residential.id, UserModel.role_id.in_([4,5,7,8])).all()
        if organizers:
            for organizer in organizers:

                notif = UserNotificationModel()
                notif.nid = organizer.nid
                notif.title = 'RingSatu'
                notif.body = 'Pengajuan Pinjam/Sewa Barang Inventaris' + inventory.title + ', ' + name + ', dari ' + start_date + ' - ' + end_date 
                notif.user_id = organizer.id
                notif.residential_id = residential_id

                db.session.add(notif)
                db.session.commit()

                if organizer.nid:
                    sendnotif_queue.apply_async(kwargs={
                        'player_ids': [organizer.nid],
                        'title': 'RingSatu',
                        'body': 'Pengajuan Pinjam/Sewa Barang Inventaris' + inventory.title + ', ' + name + ', dari ' + start_date + ' - ' + end_date
                    }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Inventaris berhasil ditambahkan.',
            'data': {
                'movement': result
            }
        }), 201


api.add_resource(InventoryMovements, '/inventory/movements')


class InventoryMovement(Resource):
    
    '''
    @method GET
    @endpoint /v1/inventory/movements/<int:movement_id>
    @permission jwt_required
    @return inventory list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventorymovement', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, movement_id):

        ''' View Inventory Movement '''

        movement_id = reverse_id(movement_id)
        inventory = InventoryMovementModel.get_by(movement_id, g.owner_id)

        if not inventory:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'inventory': ['Inventaris tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = InventoryMovementSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(inventory).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'movement': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/inventory/movements/<int:movement_id>
    @permission jwt_required
    @return updated inventory object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    def validate_phone(value):
        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', value)

        if phone_match is None:
            raise ValidationError('No. handphone tidak valid. Harus diawali dengan 08, +628 atau 628')  

    put_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'address': fields.Str(required=True, validate=[
            validate_required,
            validate.Length(max=255, error='Alamat maksimal 255 karakter.')
        ]),
        'phone': fields.Str(required=True, validate=[validate_required, validate_phone]),
        'duration': fields.List(fields.Date(), missing=None),
        'quantity': fields.Int(required=True),
        'cost': fields.Int(required=True),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi inventaris maksimal 255 karakter.')
        ]),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['waiting', 'onloan', 'rentedout', 'beingrepaired', 'broken', 'returned', 'missing', 'fixed'], labels=['Menunggu Persetujuan', 'Dipinjam', 'Disewa', 'Diperbaiki', 'Rusak', 'Hilang', 'Telah Dikembalikan', 'Telah Diperbaiki'], error='Pilihan status tidak valid.')
        ]),
        'inventory': fields.Int(required=True),
        'region': fields.Int(required=True),
        'residential': fields.Int(missing=None),
        'author': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'signedby': fields.List(fields.Dict(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventorymovement', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, movement_id):

        ''' Edit Inventory Movement '''

        movement_id = reverse_id(movement_id)
        movement = InventoryMovementModel.get_by(movement_id, g.owner_id)

        if not movement:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'inventory': ['Inventaris tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        name = bleach.clean(payload['name'], strip=True) if payload['name'] else None
        address = bleach.clean(payload['address']) if payload['address'] else None
        phone = payload['phone']
        duration = payload['duration']
        quantity = payload['quantity']
        cost = payload['cost']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        status = payload['status']
        inventory_id = reverse_id(payload['inventory'])
        region_id = reverse_id(payload['region']) if payload['region'] else None
        author_id = reverse_id(payload['author']) if payload['author'] else None
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 
        signedby = payload['signedby']

        start_date, end_date = [None, None]

        if duration:
            start_date, end_date = duration

        if not author_id:
            author_id = g.current_user.id

        if not g.owner_id:
            author = UserModel.query.get(author_id)
            g.owner_id = author.parent_id if author.parent_id else g.current_user.id

        if g.current_user.role.slug in ['root', 'owner'] and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        inventory = InventoryModel.query.get(inventory_id)
        if not inventory:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'inventory': ['Inventaris tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        total_movement = InventoryMovementModel.query.with_entities(func.sum(InventoryMovementModel.quantity).label('total')).filter(InventoryMovementModel.inventory_id==inventory_id, InventoryMovementModel.id!=movement.id, InventoryMovementModel.status.notin_(['broken','returned','fixed'])).first()

        available_inventory = None

        if total_movement[0] is not None:
            available_inventory = (inventory.quantity - total_movement[0].total)
        else:
            available_inventory = inventory.quantity

        if quantity > available_inventory:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'inventory': ['Inventaris dengan jumlah yang dimasukkan tidak terserdia.']
                    }
                }
            }), 422

        now = arrow.get().format('YYYY/MM/DD')

        movement.number = 'MOVEMENT/' + now + '/' + generate_unique_id(5)
        movement.name = name
        movement.address = address
        movement.phone = phone
        movement.start_date = start_date
        movement.end_date = end_date
        movement.quantity = quantity
        movement.cost = cost
        movement.description = description
        movement.status = status
        movement.author_id = author_id
        movement.owner_id = g.owner_id
        movement.region_id = region_id
        movement.isdeleted = False
        movement.residential_id = residential_id
        movement.inventory_id = inventory.id

        if signedby:
            movement.signs = []
            for sb in signedby:

                signer_name = bleach.clean(sb['name'], strip=True)

                signed = SignedByModel()
                signed.name = signer_name
                signed.slug = slugify(signer_name)
                signed.position = bleach.clean(sb['position'], strip=True)
                
                movement.signs.append(signed)

        db.session.commit()

        schema = InventoryMovementSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(inventory).data

        author = UserModel.query.get(author_id)
        status = 'Selamat pengajuan pinjam/sewa barang inventaris anda telah disetujui' if movement.signs else 'Maaf pengajuan pinjam/sewa barang invetaris anda tidak disetujui.'

        notif = UserNotificationModel()
        notif.nid = author.nid
        notif.title = 'RingSatu'
        notif.body = 'Status Pinjam/Sewa Barang Inventaris: ' + status
        notif.user_id = author.id
        notif.residential_id = residential_id

        db.session.add(notif)
        db.session.commit()

        if author.nid:
            sendnotif_queue.apply_async(kwargs={
                'player_ids': [author.nid],
                'title': 'RingSatu',
                'body': 'Status Pinjam/Sewa Barang Inventaris: ' + status
            }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Inventaris berhasil diubah.',
            'data': {
                'movement': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/inventory/movements/<int:movement_id>
    @permission jwt_required
    @return deleted inventory object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('inventorymovement', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, movement_id):

        ''' Delete Inventory Movement '''

        movement_id = reverse_id(movement_id)
        inventory = InventoryMovementModel.get_by(movement_id, g.owner_id)

        if not inventory:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'inventory': ['Inventaris tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = InventoryMovementSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(inventory).data

        if inventory.isdeleted:
            db.session.delete(inventory)
        else:
            inventory.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Inventaris berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'movement': result
            }
        }), 200


api.add_resource(InventoryMovement, '/inventory/movements/<int:movement_id>')


class FinancialReports(Resource):
    
    '''
    @method GET
    @endpoint /v1/financials
    @permission jwt_required
    @return financial list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'trx_type': fields.Str(missing=None, validate=[
            validate.OneOf(choices=['debit', 'credit'], labels=['Debit', 'Kredit'], error='Pilihan tipe transaksi tidak valid.')
        ]),
        'status': fields.Str(missing=None, validate=[
            validate.OneOf(choices=['waiting', 'unpaid', 'paid'], labels=['Menunggu', 'Belum Lunas', 'Lunas'], error='Pilihan status tidak valid.')
        ]),
        'period': fields.Date(missing=None),
        'residential': fields.Int(missing=None),
        'author_id': fields.Int(missing=None),
        'category': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('financialreport', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Financial Report '''

        q = args['q']
        trx_type = args['trx_type']
        period = args['period']
        status = args['status']
        author_id = reverse_id(args['author_id']) if args['author_id'] else None
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        category_id = reverse_id(args['category']) if args['category'] else None
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        period = arrow.get(period).format('YYYY-MM-DD') if period else None

        total_debit = FinancialReportModel.get_total_debit(q, trx_type, period, status, author_id, residential_id, category_id, g.owner_id, isdeleted)

        total_credit = FinancialReportModel.get_total_credit(q, trx_type, period, status, author_id, residential_id, category_id, g.owner_id, isdeleted)
        total = FinancialReportModel.count_all(q, trx_type, period, status, author_id, residential_id, category_id, g.owner_id, isdeleted, True)
        query = FinancialReportModel.get_all(q, trx_type, period, status, author_id, residential_id, category_id, g.owner_id, isdeleted, True, page, perpage)
        financialreports = []

        for index, q in enumerate(query):
            schema = FinancialReportSchema(only=args['only'], exclude=args['exclude'])
            financialreports.append(schema.dump(q[0]).data)
            financialreports[index]['balance'] = q[1]
            
        pagination = Pagination('api.financialreports', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'financialreports': financialreports,
                'pagination': pagination.paginate,
                'total_debit': total_debit,
                'total_credit': total_credit
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/financialreports
    @permission jwt_required
    @return new financial object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'trx_type': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['debit', 'credit'], labels=['Debit', 'Kredit'], error='Pilihan tipe transaksi tidak valid.')
        ]),
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang judul laporan keuangan maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul laporan keuangan maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['waiting', 'unpaid', 'paid'], labels=['Menunggu Konfirmasi', 'Belum Lunas', 'Lunas'], error='Pilihan status tidak valid.')
        ]),
        'recepient': fields.Str(missing=None),
        'nominal': fields.Int(required=True),
        'period': fields.Date(missing=None),
        'note': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi laporan keuangan maksimal 255 karakter.')
        ]),
        'author': fields.Int(missing=None),
        'category': fields.Int(required=True),
        'residential': fields.Int(missing=None),
        'signedby': fields.List(fields.Dict(), missing=None),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('financialreport', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Financial Report '''

        trx_type = payload['trx_type']
        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        nominal = payload['nominal']
        recepient = bleach.clean(payload['recepient'], strip=True) if payload['recepient'] else None
        period = payload['period']
        note = bleach.clean(payload['note'], strip=True) if payload['note'] else None
        status = payload['status']
        author_id = reverse_id(payload['author']) if payload['author'] else None
        category_id = reverse_id(payload['category'])
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 
        media_ids = payload['medias']
        signedby = payload['signedby']

        if g.current_user.role.slug == 'owner' and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        financial_report_exist = FinancialReportModel.query.filter(FinancialReportModel.slug==slug, FinancialReportModel.residential_id==residential_id, FinancialReportModel.author_id==author_id)

        if g.owner_id:
            financial_report_exist = financial_report_exist.filter(FinancialReportModel.owner_id==g.owner_id)

        if financial_report_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'financial': ['Laporan keuangan sudah terdaftar.']
                    }
                }
            }), 422    

        owner_id = g.owner_id

        if not owner_id:
            owner_id = residential.owner_id    

        if not author_id:
            user = UserModel.query.filter(UserModel.residential_id==residential.id, UserModel.role_id==7).first()
            if not user:
                author_id = g.current_user.id
            else:
                author_id = user.id

        now = arrow.get().format('YYYY/MM/DD')

        prefix = trx_type.upper() + '/'

        financialreport = FinancialReportModel()
        financialreport.number = prefix + now + '/' + generate_unique_id(5)
        financialreport.trx_type = trx_type
        financialreport.title = title
        financialreport.slug = slug
        financialreport.recepient = recepient
        financialreport.period = period
        financialreport.note = note
        financialreport.status = status
        financialreport.author_id = author_id
        financialreport.owner_id = owner_id
        financialreport.isdeleted = False
        financialreport.residential_id = residential_id  
        financialreport.category_id = category_id

        if trx_type == 'credit':
            financialreport.credit = nominal
        else:
            financialreport.debit = nominal     

        db.session.add(financialreport)
        db.session.flush()

        if signedby:
            for sb in signedby:

                signer_name = bleach.clean(sb['name'], strip=True)

                signed = SignedByModel()
                signed.name = signer_name
                signed.slug = slugify(signer_name)
                signed.position = bleach.clean(sb['position'], strip=True)
                
                financialreport.signs.append(signed)

        db.session.commit()

        if media_ids:

            for media_id in media_ids:
                media_id = reverse_id(media_id)
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
                    media.financial_report_id = financialreport.id
                    media.residential_id = residential_id

                    media.owner_id = g.owner_id

                    media.isorphan = False
                    media.expired = None

                    file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                    new_path = os.path.join(current_app.config.get('FINANCIAL_REPORT_FOLDER'), media.filename)

                    is_dir_exist(new_path)

                    if is_file_exist(file_path):
                        os.rename(file_path, new_path)

                    db.session.commit()
                    
        period = arrow.get().format('YYYY-MM-DD')
        query = FinancialReportModel.get_all(None, None, period, None, None, residential_id, None, g.owner_id, False, False, 1, 2)

        financialreports = []

        if query:            
            for index, q in enumerate(query):
                schema = FinancialReportSchema(only=payload['only'], exclude=payload['exclude'])
                financialreports.append(schema.dump(q[0]).data)
                financialreports[index]['balance'] = q[1]

        # send notif via task queue manager based on residential
        residents = residential.users
        if residents:
            for resident in residents:
                ntitle = 'Penambahan Saldo ' + residential.name if trx_type == 'credit' else 'Pengurangan Saldo ' + residential.name

                notif = UserNotificationModel()
                notif.nid = resident.nid
                notif.title = 'RingSatu'
                notif.body = ntitle
                notif.user_id = resident.id
                notif.residential_id = residential_id

                db.session.add(notif)
                db.session.commit()

                if resident.nid:
                    sendnotif_queue.apply_async(kwargs={
                        'player_ids': [resident.nid],
                        'title': 'RingSatu',
                        'body': ntitle
                    }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Laporan keuangan berhasil ditambahkan.',
            'data': {
                'financialreport': financialreports[0]
            }
        }), 201


api.add_resource(FinancialReports, '/financialreports')


class FinancialReport(Resource):
    
    '''
    @method GET
    @endpoint /v1/financialreports/<int:financialreport_id>
    @permission jwt_required
    @return financial report list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('financialreport', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, financialreport_id):

        ''' View Financial Report '''

        financialreport_id = reverse_id(financialreport_id)
        financialreport = FinancialReportModel.get_by(financialreport_id, g.owner_id)

        if not financialreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'financialreport': ['Laporang keuangan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = FinancialReportSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(financialreport).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'financialreport': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/financialreports/<int:financialreport_id>
    @permission jwt_required
    @return updated financialreport object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'trx_type': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['debit', 'credit'], labels=['Debit', 'Kredit'], error='Pilihan tipe transaksi tidak valid.')
        ]),
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang judul laporan keuangan maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul laporan keuangan maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['waiting', 'unpaid', 'paid'], labels=['Menunggu Konfirmasi', 'Belum Lunas', 'Lunas'], error='Pilihan status tidak valid.')
        ]),
        'recepient': fields.Str(missing=None),
        'nominal': fields.Int(required=True),
        'period': fields.Date(missing=None),
        'note': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi laporan keuangan maksimal 255 karakter.')
        ]),
        'author': fields.Int(missing=None),
        'category': fields.Int(required=True),
        'residential': fields.Int(missing=None),
        'medias': fields.List(fields.Int(), missing=None),
        'signedby': fields.List(fields.Dict(), missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('financialreport', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, financialreport_id):

        ''' Edit Resident Report '''

        financialreport_id = reverse_id(financialreport_id)
        financialreport = FinancialReportModel.get_by(financialreport_id, g.owner_id)

        if not financialreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'financialreport': ['Laporan keuangan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        trx_type = payload['trx_type']
        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        nominal = payload['nominal']
        recepient = bleach.clean(payload['recepient'], strip=True) if payload['recepient'] else None
        period = payload['period']
        note = bleach.clean(payload['note'], strip=True) if payload['note'] else None
        status = payload['status']
        author_id = reverse_id(payload['author']) if payload['author'] else None 
        category_id = reverse_id(payload['category'])
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 
        isdeleted = payload['archived']
        signedby = payload['signedby']

        if g.current_user.role.slug == 'owner' and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        financial_report_exist = FinancialReportModel.query.filter(FinancialReportModel.slug==slug, FinancialReportModel.residential_id==residential_id, FinancialReportModel.author_id==author_id, FinancialReportModel.id!=financialreport_id)

        if g.owner_id:
            financial_report_exist = financial_report_exist.filter(FinancialReportModel.owner_id==g.owner_id)

        if financial_report_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'financial': ['Laporan keuangan sudah terdaftar.']
                    }
                }
            }), 422        


        financialreport.trx_type = trx_type
        financialreport.title = title
        financialreport.slug = slug
        financialreport.recepient = recepient
        financialreport.period = period
        financialreport.note = note
        financialreport.status = status
        financialreport.isdeleted = isdeleted
        financialreport.residential_id = residential_id
        financialreport.category_id = category_id

        if not author_id:
            user = UserModel.query.filter(UserModel.residential_id==residential.id, UserModel.role_id==7).first()
            if not user:
                author_id = g.current_user.id
            else:
                author_id = user.id

        financialreport.author_id = author_id

        if trx_type == 'credit':
            financialreport.credit = nominal
        else:
            financialreport.debit = nominal

        if signedby:
            financialreport.signs = []
            for sb in signedby:

                signer_name = bleach.clean(sb['name'], strip=True)

                signed = SignedByModel()
                signed.name = signer_name
                signed.slug = slugify(signer_name)
                signed.position = bleach.clean(sb['position'], strip=True)
                
                financialreport.signs.append(signed)

        db.session.commit()

        period = arrow.get().format('YYYY-MM-DD')
        query = FinancialReportModel.get_all(None, None, period, None, None, residential_id, None, g.owner_id, False, False,  1, 2)

        financialreports = []

        if query:
            for index, q in enumerate(query):
                schema = FinancialReportSchema(only=payload['only'], exclude=payload['exclude'])
                financialreports.append(schema.dump(q[0]).data)
                financialreports[index]['balance'] = q[1]

        author = UserModel.query.get(author_id)

        if trx_type == 'credit':
            nstatus = 'Pembayaran anda sedang kami proses'

            if status == 'unpaid':
                nstatus = 'Maaf pembayaran anda belum kami terima'
            elif status == 'paid':
                nstatus = 'Terima kasih, pembayaran anda telah kami terima'

            notif = UserNotificationModel()
            notif.nid = author.nid
            notif.title = 'RingSatu'
            notif.body = 'Status Transaksi: ' + nstatus
            notif.user_id = author.id
            notif.residential_id = residential_id

            db.session.add(notif)
            db.session.commit()

            if author.nid:
                sendnotif_queue.apply_async(kwargs={
                    'player_ids': [author.nid],
                    'title': 'RingSatu',
                    'body': 'Status Transaksi: ' + nstatus
                }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Laporan keuangan berhasil diubah.',
            'data': {
                'financialreport': financialreports[0]
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/financialreports/<int:financialreport_id>
    @permission jwt_required
    @return deleted financialreport object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('financialreport', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, financialreport_id):

        ''' Delete Financial Report '''

        financialreport_id = reverse_id(financialreport_id)
        financialreport = FinancialReportModel.get_by(financialreport_id, g.owner_id)

        if not financialreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'financialreport': ['Laporan keuangan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = FinancialReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(financialreport).data

        if financialreport.isdeleted:
            if financialreport.medias:
                for media in financialreport.medias:
                    os.remove(os.path.join(current_app.config.get('FINANCIAL_REPORT_FOLDER'), media.filename))
            db.session.delete(financialreport)
        else:
            financialreport.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Laporan keuangan berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'financialreport': result
            }
        }), 200


api.add_resource(FinancialReport, '/financialreports/<int:financialreport_id>')


class CCTVS(Resource):
    
    '''
    @method GET
    @endpoint /v1/cctv
    @permission jwt_required
    @return financial list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(missing=None, validate=[
            validate.OneOf(choices=['online', 'offline'], labels=['Online', 'Offline'], error='Pilihan status tidak valid.')
        ]),
        'residential': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('cctv', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All CCTV '''

        q = args['q']
        status = args['status']
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = CCTVModel.count_all(q, status, residential_id, g.owner_id, isdeleted)
        query = CCTVModel.get_all(q, status, residential_id, g.owner_id, isdeleted, page, perpage)

        schema = CCTVSchema(only=args['only'], exclude=args['exclude'], many=True)
        cctv = schema.dump(query).data
        pagination = Pagination('api.cctvs', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'cctvs': cctv,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/cctv
    @permission jwt_required
    @return new cctv object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    def validate_url(value):
        if not valid_url(value):
            raise ValidationError(message='URL tidak valid')

    post_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama cctv maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama cctv maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['online', 'offline'], labels=['Online', 'Offline'], error='Pilihan status tidak valid.')
        ]),
        'url': fields.Str(required=True, validate=[validate_required, validate_url]),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi cctv maksimal 255 karakter.')
        ]),
        'residential': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('cctv', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New CCTV '''

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        url = payload['url']
        latitude = payload['latitude']
        longitude = payload['longitude']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        status = payload['status']
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 

        if g.current_user.role.slug == 'owner' and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        author_id = residential.owner_id if g.current_user.role.slug == 'root' else g.current_user.id

        cctv_exist = CCTVModel.query.filter(CCTVModel.slug==slug, CCTVModel.residential_id==residential_id, CCTVModel.url==url)

        if g.owner_id:
            cctv_exist = cctv_exist.filter(CCTVModel.owner_id==g.owner_id)

        if cctv_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'cctv': ['CCTV sudah terdaftar.']
                    }
                }
            }), 422        

        cctv = CCTVModel()
        cctv.name = name
        cctv.slug = slug
        cctv.description = description
        cctv.url = url
        cctv.latitude = latitude
        cctv.longitude = longitude
        cctv.status = status
        cctv.author_id = author_id
        cctv.owner_id = g.owner_id
        cctv.isdeleted = False
        cctv.residential_id = residential_id

        db.session.add(cctv)
        db.session.commit()

        schema = CCTVSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(cctv).data

        # send notif via task queue manager based on residential

        return jsonify({
            'success': True,
            'message': 'CCTV berhasil ditambahkan.',
            'data': {
                'cctv': result
            }
        }), 201


api.add_resource(CCTVS, '/cctv')


class CCTV(Resource):
    
    '''
    @method GET
    @endpoint /v1/cctv/<int:cctv_id>
    @permission jwt_required
    @return cctv list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('cctv', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, cctv_id):

        ''' View CCTV '''

        cctv_id = reverse_id(cctv_id)
        cctv = CCTVModel.get_by(cctv_id, g.owner_id)

        if not cctv:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'cctv': ['CCTV tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = CCTVSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(cctv).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'cctv': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/cctv/<int:cctv_id>
    @permission jwt_required
    @return updated cctv object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.') 

    def validate_url(value):
        if not valid_url(value):
            raise ValidationError(message='URL tidak valid')

    put_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama cctv maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama cctv maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['online', 'offline'], labels=['Online', 'Offline'], error='Pilihan status tidak valid.')
        ]),
        'url': fields.Str(required=True, validate=[validate_required, validate_url]),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi cctv maksimal 255 karakter.')
        ]),
        'residential': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('cctv', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, cctv_id):

        ''' Edit CCTV'''

        cctv_id = reverse_id(cctv_id)
        cctv = CCTVModel.get_by(cctv_id, g.owner_id)

        if not cctv:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'cctv': ['CCTV tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        url = payload['url']
        latitude = payload['latitude']
        longitude = payload['longitude']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        status = payload['status']
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None 
        isdeleted = payload['archived']

        if g.current_user.role.slug == 'owner' and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Perumahan wajib dipilih.']
                    }
                }
            }), 422

        if g.current_user.role.slug not in ['root', 'owner']:
            residential_id = g.current_user.residential_id

        residential = ResidentialModel.get_by(residential_id, g.owner_id)
        if not residential:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        cctv_exist = CCTVModel.query.filter(CCTVModel.slug==slug, CCTVModel.residential_id==residential_id, CCTVModel.url==url, CCTVModel.id!=cctv_id)

        if g.owner_id:
            cctv_exist = cctv_exist.filter(CCTVModel.owner_id==g.owner_id)

        if cctv_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'cctv': ['CCTV sudah terdaftar.']
                    }
                }
            }), 422        

        cctv.name = name
        cctv.slug = slug
        cctv.description = description
        cctv.url = url
        cctv.latitude = latitude
        cctv.longitude = longitude
        cctv.status = status
        cctv.isdeleted = isdeleted
        cctv.residential_id = residential_id

        db.session.commit()

        schema = CCTVSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(cctv).data

        return jsonify({
            'success': True,
            'message': 'CCTV berhasil diubah.',
            'data': {
                'cctv': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/cctv/<int:cctv_id>
    @permission jwt_required
    @return deleted cctv object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('cctv', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, cctv_id):

        ''' Delete CCTV '''

        cctv_id = reverse_id(cctv_id)
        cctv = CCTVModel.get_by(cctv_id, g.owner_id)

        if not cctv:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'cctv': ['CCTV tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = CCTVSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(cctv).data

        if cctv.isdeleted:
            db.session.delete(cctv)
        else:
            cctv.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'CCTV berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'cctv': result
            }
        }), 200


api.add_resource(CCTV, '/cctv/<int:cctv_id>')


class CCTVStream(Resource):

    '''
    @method GET
    @endpoint /v1/cctv/stream/<int:cctv_id>
    @permission jwt_required
    @return cctv stream image
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @use_args(get_args, locations=['query'])
    def get(self, args, cctv_id):

        ''' View CCTV '''

        cctv_id = reverse_id(cctv_id)
        cctv = CCTVModel.get_by(cctv_id)

        if cctv:
            url = cctv.url
            parsedURL = urlparse(url)

            error_image = 'static/no-signal.jpg'

            if parsedURL.username and parsedURL.password:
                stream = requests.get(url, auth=(parsedURL.username, parsedURL.password), stream=True)
            else:
                stream = requests.get(url, stream=True)

            if stream.status_code == 200:
                return Response(stream_with_context(stream.iter_content()), content_type = stream.headers['content-type'])

            else:
                return send_file(error_image, mimetype="image/jpeg")

api.add_resource(CCTVStream, '/cctv/stream/<int:cctv_id>')


class Panics(Resource):
    
    '''
    @method GET
    @endpoint /v1/panics
    @permission jwt_required
    @return panic list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'residential': fields.Int(missing=None),
        'author': fields.Int(missing=None),
        'restricted': fields.Bool(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panic', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Panics '''

        q = args['q']
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        author_id = reverse_id(args['author']) if args['author'] else None
        restricted = args['restricted']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = PanicModel.count_all(q, restricted, residential_id, g.owner_id, author_id, isdeleted)
        query = PanicModel.get_all(q, restricted, residential_id, g.owner_id, author_id, isdeleted, page, perpage)

        schema = PanicSchema(only=args['only'], exclude=args['exclude'], many=True)
        panics = schema.dump(query).data
        pagination = Pagination('api.panics', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'panics': panics,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/panic
    @permission jwt_required
    @return new panic object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'group': fields.Str(required=True, validate=[
            validate.OneOf(choices=['community', 'family', 'friendship', 'work', 'residential'], labels=['Komunitas', 'Keluarga', 'Pekerjaan', 'Perumahan'], error='Pilihan group tidak valid.')
        ]),
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama tombol panik maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama tombol panik maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'icon': fields.Str(missing=None),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi tombol panik maksimal 255 karakter.')
        ]),
        'restricted': fields.Bool(missing=False),
        'live_location': fields.Bool(missing=False),
        'residentials': fields.List(fields.Int(), missing=None),
        'participants': fields.List(fields.Int(), missing=None),
        'actions': fields.List(fields.Dict(), missing=None),
        'media': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panic', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Panic '''

        group = payload['group']
        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        icon = payload['icon']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        residential_ids = [reverse_id(x) for x in payload['residentials']] if payload['residentials'] else None
        participant_ids = [reverse_id(x) for x in payload['participants']] if payload['participants'] else None 
        restricted = payload['restricted']
        live_location = payload['live_location'] 
        media_id = reverse_id(payload['media']) if payload['media'] else None
        actions = payload['actions']

        panic_exist = PanicModel.query.filter(PanicModel.slug==slug)

        if g.owner_id:
            panic_exist = panic_exist.filter(PanicModel.owner_id==g.owner_id)

        if panic_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'panic': ['Tombol panik sudah terdaftar.']
                    }
                }
            }), 422

        panic = PanicModel()
        panic.panic_group = group
        panic.name = name
        panic.slug = slug
        panic.description = description
        panic.icon = icon
        panic.restricted = restricted
        panic.live_location = live_location
        panic.author_id = g.current_user.id
        panic.owner_id = g.owner_id
        panic.isdeleted = False

        db.session.add(panic)
        db.session.flush()

        if actions:
            for act in actions:
                action = PanicActionModel()
                action.action = act['action']
                action.phone = act['phone']
                action.alt_phone = act['altphone']
                action.role_id = reverse_id(act['role']) if act['role'] else None

                panic.actions.append(action)

        if residential_ids:
            for residential_id in residential_ids:
                residential = ResidentialModel.query.get(residential_id)
                if residential:
                    residential.panics.append(panic)

        if participant_ids:
            for participant_id in participant_ids:
                participant_user = UserModel.query.get(participant_id)
                if participant_user:
                    p = PanicParticipantModel()
                    p.user_id = participant_user.id
                    
                    if participant_user.id == g.current_user.id:
                        p.status = 'approved'
                    else:
                        p.status = 'waiting'
                    panic.participants.append(p)

        if media_id:
            media = MediaModel.query.get(media_id)

            if media and media.isorphan:
                media.panic_id = panic.id

                media.isorphan = False
                media.expired = None

                file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                new_path = os.path.join(current_app.config.get('PANIC_FOLDER'), media.filename)

                is_dir_exist(new_path)

                if is_file_exist(file_path):
                    os.rename(file_path, new_path)

        db.session.commit()

        schema = PanicSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(panic).data

        # send notif via task queue manager based on residential

        participants = PanicParticipantModel.query.filter(PanicParticipantModel.id != g.current_user.id, PanicParticipantModel.panic_id == panic.id).all()

        if participants:
            for p in participants:

                if p.user_id != g.current_user.id:

                    notif = UserNotificationModel()
                    notif.nid = p.user.nid
                    notif.title = 'RingSatu'
                    notif.body = g.current_user.profile.fullname.upper() + ' mengundang anda untuk bergabung di group panik ' + panic.name.upper()
                    notif.user_id = p.user_id

                    db.session.add(notif)
                    db.session.commit()

                    if p.user.nid:
                        sendnotif_queue.apply_async(kwargs={
                            'player_ids': [p.user.nid],
                            'title': 'RingSatu',
                            'body': g.current_user.profile.fullname.upper() + ' mengundang anda untuk bergabung di group panik ' + panic.name.upper()
                        }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Tombol panik berhasil ditambahkan.',
            'data': {
                'panic': result
            }
        }), 201


api.add_resource(Panics, '/panics')


class Panic(Resource):
    
    '''
    @method GET
    @endpoint /v1/panic/<int:panic_id>
    @permission jwt_required
    @return panic list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panic', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, panic_id):

        ''' View panic '''

        panic_id = reverse_id(panic_id)
        panic = PanicModel.get_by(panic_id, g.owner_id)

        if not panic:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panic': ['Tombol panik tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = PanicSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(panic).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'panic': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/panic/<int:panic_id>
    @permission jwt_required
    @return updated panic object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.') 

    def validate_url(value):
        if not validate_url(value):
            raise ValidationError(message='URL tidak valid')

    put_args = {
        'group': fields.Str(required=True, validate=[
            validate.OneOf(choices=['community', 'family', 'friendship', 'work', 'residential'], labels=['Komunitas', 'Keluarga', 'Pekerjaan', 'Perumahan'], error='Pilihan group tidak valid.')
        ]),
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama tombol panik maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama tombol panik maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'icon': fields.Str(missing=None),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi tombol panik maksimal 255 karakter.')
        ]),
        'residentials': fields.List(fields.Int(), missing=None),
        'participants': fields.List(fields.Int(), missing=None),
        'actions': fields.List(fields.Dict(), missing=None),
        'restricted': fields.Bool(missing=False),
        'live_location': fields.Bool(missing=False),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panic', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, panic_id):

        ''' Edit Panic'''

        panic_id = reverse_id(panic_id)
        panic = PanicModel.get_by(panic_id, g.owner_id)

        if not panic:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panic': ['Tombol panik tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        group = payload['group']
        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        icon = payload['icon']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        residential_ids = [reverse_id(x) for x in payload['residentials']] if payload['residentials'] else None  
        participant_ids = [reverse_id(x) for x in payload['participants']] if payload['participants'] else None
        restricted = payload['restricted']
        live_location = payload['live_location']
        actions = payload['actions']              

        panic_exist = PanicModel.query.filter(PanicModel.slug==slug, PanicModel.id!=panic.id)

        if g.owner_id:
            panic_exist = panic_exist.filter(PanicModel.owner_id==g.owner_id)

        if panic_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'panic': ['Tombol panik sudah terdaftar.']
                    }
                }
            }), 422

        panic.panic_group = group
        panic.name = name
        panic.slug = slug
        panic.description = description
        panic.icon = icon
        panic.restricted = restricted
        panic.live_location = live_location
        panic.isdeleted = False

        if actions:
            panic_actions = []
            for act in actions:
                action = PanicActionModel()
                action.action = act['action']
                action.phone = act['phone']
                action.alt_phone = act['altphone']
                action.role_id = reverse_id(act['role']) if act['role'] else None

                panic_actions.append(action)

            panic.actions = panic_actions

        if residential_ids:
            panic.panics_residentials = []
            for residential_id in residential_ids:
                residential = ResidentialModel.query.get(residential_id)
                if residential:
                    residential.panics.append(panic)

        if participant_ids:
            for participant_id in participant_ids:
                participant_user = PanicParticipantModel.query.filter_by(user_id=participant_id).first()
                if not participant_user:
                    p = PanicParticipantModel()
                    p.user_id = participant_id
                    p.status = 'waiting'
                    panic.participants.append(p)

        db.session.commit()

        schema = PanicSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(panic).data

        return jsonify({
            'success': True,
            'message': 'Tombol panik berhasil diubah.',
            'data': {
                'panic': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/panic/<int:panic_id>
    @permission jwt_required
    @return deleted panic object
    '''  

    delete_args = {        
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panic', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, panic_id):

        ''' Panic Invitation Status '''

        panic_id = reverse_id(panic_id)
        panic = PanicModel.get_by(panic_id, g.owner_id)

        if not panic:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panic': ['Tombol panik tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        if g.current_user.id != panic.author_id and not g.current_user.check_permission('panic', 'deleteother'):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 403,
                    'messages': {
                        'permission': ['Anda tidak memiliki izin untuk melakukan permintaan ini.']
                    }
                }
            }), 403

        schema = PanicSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(panic).data

        if panic.isdeleted:
            if panic.media:
                os.remove(os.path.join(current_app.config.get('PANIC_FOLDER'), media.filename))
            db.session.delete(panic)
        else:
            panic.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Tombol panik berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'panic': result
            }
        }), 200


api.add_resource(Panic, '/panics/<int:panic_id>')

class PanicInvitation(Resource):
    
    '''
    @method PUT
    @endpoint /v1/panic/<int:panic_id>/invitation
    @permission jwt_required
    @return panic invitation status
    '''  

    put_args = {
        'status': fields.Str(required=True, validate=[
            validate.OneOf(choices=['approved', 'rejected'], labels=['Disetujui', 'Ditolak'], error='Pilihan group tidak valid.')
        ]),
        'user': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panic', 'viewown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, panic_id):

        ''' Panic Invitation '''

        panic_id = reverse_id(panic_id)
        panic = PanicModel.query.get(panic_id)

        status = payload['status']
        user_id = reverse_id(payload['user']) if payload['user'] else g.current_user.id

        if not panic:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panic': ['Tombol panik tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        invited = PanicParticipantModel.query.filter(PanicParticipantModel.panic_id==panic.id, PanicParticipantModel.user_id==user_id).first()

        if not invited:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panic': ['Maaf anda tidak terdaftar dalam undangan anggota tombol panik ini.']
                    }
                }
            }), 404

        if status == 'approved':
            invited.status = 'approved'
            db.session.commit()
        else:
            db.session.delete(invited)
            db.session.commit()

        schema = PanicSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(panic).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'panic': result
            }
        }), 200


api.add_resource(PanicInvitation, '/panics/<int:panic_id>/invitation')


class PanicReports(Resource):
    
    '''
    @method GET
    @endpoint /v1/panic/reports
    @permission jwt_required
    @return report list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['waiting', 'onprogress', 'done'], labels=['Menunggu', 'Dalam Ditindaklanjuti', 'Selesai'], error='Pilihan status tidak valid.')
        ], missing=None),
        'panic': fields.Int(missing=None),
        'action': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panicreport', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Panic Report '''

        q = args['q']
        panic_id = reverse_id(args['panic']) if args['panic'] else None
        action_id = reverse_id(args['action']) if args['action'] else None
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        status = args['status']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = PanicReportModel.count_all(q, status, residential_id, panic_id, action_id, g.owner_id, isdeleted)
        query = PanicReportModel.get_all(q, status, residential_id, panic_id, action_id, g.owner_id, isdeleted, page, perpage)

        schema = PanicReportSchema(only=args['only'], exclude=args['exclude'], many=True)
        panicreports = schema.dump(query).data
        pagination = Pagination('api.panicreports', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'panicreports': panicreports,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/panic/reports
    @permission jwt_required
    @return new panic report object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'status': fields.Str(validate=[
            validate.OneOf(choices=['waiting', 'onprogress', 'done'], labels=['Menunggu', 'Dalam Ditindaklanjuti', 'Selesai'], error='Pilihan status tidak valid.')
        ], missing='waiting'),
        'latitude': fields.Decimal(required=True),
        'longitude': fields.Decimal(required=True),
        'performance': fields.Int(missing=0),
        'panic': fields.Int(required=True),
        'action': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'reporter': fields.Int(required=True),
        'note': fields.Str(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panicreport', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Panic Report '''

        status = payload['status']
        latitude = payload['latitude']
        longitude = payload['longitude']
        performance = payload['performance']
        panic_id = reverse_id(payload['panic']) 
        action_id = reverse_id(payload['action']) if payload['action'] else None
        reporter_id = reverse_id(payload['reporter'])
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None
        note = bleach.clean(payload['note'], strip=True) if payload['note'] else None

        panic = PanicModel.query.get(panic_id)
        if not panic:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panic': ['Tombol panik tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        if residential_id:
            residential = ResidentialModel.query.get(residential_id)
            if not g.owner_id:
                g.owner_id = residential.owner_id

            if panic.restricted and latitude and latitude and residential.restricted and residential.polygon:
                poly = json.loads(residential.polygon)
                if restricted_area(latitude, longitude, poly) == False:
                    return jsonify({
                        'success': False,
                        'errors': {
                            'code': 422,
                            'messages': {
                                'restricted': ['Maaf lokasi anda diluar jangkauan perumahans.']
                            }
                        }
                    }), 422

        now = arrow.get().format('YYYY/MM/DD')

        panicreport = PanicReportModel()
        panicreport.number = 'PANIC/' + now + '/' + generate_unique_id(5)
        panicreport.latitude = latitude
        panicreport.longitude = longitude
        panicreport.performance = performance
        panicreport.note = note
        panicreport.status = status
        panicreport.action_id = action_id
        panicreport.panic_id = panic_id
        panicreport.reporter_id = reporter_id
        panicreport.owner_id = g.owner_id
        panicreport.isdeleted = False
        panicreport.residential_id = residential_id

        if panic.live_location:
            nexthour = datetime.now() + timedelta(hours=1)
            live_expired = time.mktime(nexthour.timetuple())
            panicreport.live_expired = live_expired

        db.session.add(panicreport)
        db.session.commit()

        schema = PanicReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(panicreport).data

        # send notif via task queue manager based on residential
        reporter = UserModel.query.get(reporter_id)
        actions = panic.actions.all()
        participants = panic.participants.filter_by(status='approved').all()

        pusher = PusherSocket()

        if participants:
            ids = []
            for p in participants:

                if p.user_id != reporter.id:
                    ids.append(reverse_id(p.user_id))

                notif = UserNotificationModel()
                notif.nid = p.user.nid
                notif.title = 'PANIK! ' + panic.name + ': ' + reporter.profile.fullname
                notif.body = '<a class="geo" data-href="geo:'+str(latitude)+','+str(longitude)+'?z=18">Lihat Lokasi</a>'
                notif.user_id = p.user_id

                db.session.add(notif)
                db.session.commit()

            comment = CommentModel()
            comment.comment = 'PANIK!<br><a class="geo" data-href="geo:'+str(latitude)+','+str(longitude)+'?z=18">Lihat Lokasi</a>'
            comment.latitude = latitude
            comment.longitude = longitude
            comment.user_id = reporter.id
            comment.panic_id = panic.id

            db.session.add(comment)
            db.session.commit()

            pusher.trigger('panic', 'alarm', {
                'ids': ids,
                'panic': {
                    'id': reverse_id(panic.id),
                    'name': panic.name,
                    'live': panic.live_location,
                    'expired': panicreport.live_expired
                }, 
                'reporter': {
                    'id': reverse_id(reporter.id),
                    'fullname': reporter.profile.fullname,
                    'address': reporter.profile.address,
                    'phone': reporter.profile.phone,
                    'latitude': str(latitude),
                    'longitude': str(longitude)
                }
            })

        elif actions:
            for action in actions:
                if action.action == 'alarm':                    
                    users = UserModel.query.filter(UserModel.role_id==action.role_id, UserModel.residential_id==residential.id).all()

                    ids = []

                    for u in users:
                        ids.append(reverse_id(u.id))

                        notif = UserNotificationModel()
                        notif.nid = u.nid
                        notif.title = 'PANIK! ' + panic.name + ': ' + reporter.profile.fullname
                        notif.body = reporter.profile.fullname + ', ' + reporter.profile.address + ', ' + reporter.profile.phone
                        notif.user_id = u.id
                        notif.residential_id = residential_id

                        db.session.add(notif)
                        db.session.commit()

                    pusher.trigger('panic', 'alarm', {
                        'ids': ids,
                        'panic': {
                            'id': reverse_id(panic.id),
                            'name': panic.name,
                            'live': panic.live_location,
                            'expired': panic.live_expired
                        },                        
                        'reporter': {
                            'id': reverse_id(reporter.id),
                            'fullname': reporter.profile.fullname,
                            'address': reporter.profile.address,
                            'phone': reporter.profile.phone,
                            'latitude': str(latitude),
                            'longitude': str(longitude)
                        }
                    })

                elif action.action == 'notification':
                    users = UserModel.query.filter(UserModel.role_id==action.role_id, UserModel.residential_id==residential.id).all()

                    for u in users:
                        notif = UserNotificationModel()
                        notif.nid = u.nid
                        notif.title = 'PANIK! ' + panic.name + ': ' + reporter.profile.fullname
                        notif.body = reporter.profile.fullname + ', ' + reporter.profile.address + ', ' + reporter.profile.phone
                        notif.user_id = u.id
                        notif.residential_id = residential_id

                        db.session.add(notif)
                        db.session.commit()

                        if u.nid:
                            sendnotif_queue.apply_async(kwargs={
                                'player_ids': [u.nid],
                                'title': 'PANIK! ' + panic.name + ': ' + reporter.profile.fullname,
                                'body': reporter.profile.fullname + ', ' + reporter.profile.address + ', ' + reporter.profile.phone
                            }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Laporan panik berhasil ditambahkan.',
            'data': {
                'panicreport': result
            }
        }), 201


api.add_resource(PanicReports, '/panic/reports')


class PanicReport(Resource):
    
    '''
    @method GET
    @endpoint /v1/panic/reports/<int:panicreport_id>
    @permission jwt_required
    @return panic panicreport list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panicreport', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, panicreport_id):

        ''' View Panic Report '''

        panicreport_id = reverse_id(panicreport_id)
        panicreport = PanicReportModel.get_by(panicreport_id, g.owner_id)

        if not panicreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panicreport': ['Laporan tombol panik tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = PanicReportSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(panicreport).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'panicreport': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/panic/reports/<int:panicreport_id>
    @permission jwt_required
    @return updated panic action object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.') 

    put_args = {
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['waiting', 'onprogress', 'done'], labels=['Menunggu', 'Dalam Ditindaklanjuti', 'Selesai'], error='Pilihan status tidak valid.')
        ]),
        'latitude': fields.Decimal(required=True),
        'longitude': fields.Decimal(required=True),
        'performance': fields.Int(missing=0),
        'note': fields.Str(missing=None),
        'panic': fields.Int(required=True),
        'action': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'reporter': fields.Int(required=True),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panicreport', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, panicreport_id):

        ''' Edit Panic Report'''

        panicreport_id = reverse_id(panicreport_id)
        panicreport = PanicReportModel.get_by(panicreport_id, g.owner_id)

        if not panicreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panicreport': ['Laporan tombol panik tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        status = payload['status']
        latitude = payload['latitude']
        longitude = payload['longitude']
        performance = payload['performance']
        panic_id = reverse_id(payload['panic']) 
        action_id = reverse_id(payload['action'])  if payload['action'] else None
        reporter_id = reverse_id(payload['reporter'])
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None
        note = bleach.clean(payload['note'], strip=True) if payload['note'] else None

        panic = PanicModel.query.get(panic_id)
        if not panic:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panic': ['Tombol panik tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404 

        if residential_id:
            residential = ResidentialModel.query.get(residential_id)
            if not g.owner_id:
                g.owner_id = residential.owner_id

            if panic.restricted and latitude and longitude and residential.restricted and residential.polygon:
                poly = json.loads(residential.polygon)

                if not restricted_area(latitude, longitude, poly):
                    return jsonify({
                        'success': False,
                        'errors': {
                            'code': 422,
                            'messages': {
                                'restricted': ['Maaf lokasi anda diluar jangkauan.']
                            }
                        }
                    }), 422

        panicreport.status = status
        panicreport.latitude = latitude
        panicreport.longitude = longitude
        panicreport.performance = performance
        panicreport.note = note
        panicreport.action_id = action_id
        panicreport.panic_id = panic_id
        panicreport.reporter_id = reporter_id
        panicreport.owner_id = g.owner_id
        panicreport.isdeleted = False
        panicreport.residential_id = residential_id

        db.session.commit()

        schema = PanicReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(panicreport).data

        return jsonify({
            'success': True,
            'message': 'Laporan panik berhasil diubah.',
            'data': {
                'panicreport': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/panic/reports/<int:panicreport_id>
    @permission jwt_required
    @return deleted panic object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('panicreport', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, panicreport_id):

        ''' Delete Panic '''

        panicreport_id = reverse_id(panicreport_id)
        panicreport = PanicReportModel.get_by(panicreport_id, g.owner_id)

        if not panicreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'panicreport': ['Laporan tombol panik tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = PanicReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(panicreport).data

        if panicreport.isdeleted:
            db.session.delete(panicreport)
        else:
            panicreport.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Laporan tombol panik berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'panicreport': result
            }
        }), 200


api.add_resource(PanicReport, '/panic/reports/<int:panicreport_id>')


class Infos(Resource):
    
    '''
    @method GET
    @endpoint /v1/info
    @permission jwt_required
    @return info list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'user': fields.Int(missing=None),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('info', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Info '''

        q = args['q']
        user_id = reverse_id(args['user']) if args['user'] else None

        page = args['page']
        perpage = args['perpage']
        latitude = args['latitude']
        longitude = args['longitude']

        total = InfoModel.count_all(q, user_id, latitude, longitude)
        query = InfoModel.get_all(q, user_id, latitude, longitude, page, perpage)

        if latitude and longitude:

            infos = []

            for index, q in enumerate(query):
                schema = InfoSchema(only=args['only'], exclude=args['exclude'])
                infos.append(schema.dump(q[0]).data)

        else:
            schema = InfoSchema(only=args['only'], exclude=args['exclude'], many=True)
            infos = schema.dump(query).data

        pagination = Pagination('api.infos', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'infos': infos,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/info
    @permission jwt_required
    @return new info object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama hak akses maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'body': fields.Str(required=True, validate=[validate_required]),
        'latitude': fields.Decimal(required=True),
        'longitude': fields.Decimal(required=True),
        'user': fields.Int(required=True),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('info', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create Info '''

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title)
        body = bleach.clean(payload['body'], strip=True)
        latitude = payload['latitude']
        longitude = payload['longitude']
        user_id = reverse_id(payload['user'])
        media_ids = payload['medias'] 

        info_exist = InfoModel.query.filter(InfoModel.slug==slug).first()

        if info_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'info': ['Info sudah terdaftar, silahkan ganti judul.']
                    }
                }
            }), 404

        info = InfoModel()
        info.title = title
        info.slug = slug
        info.body = body
        info.latitude = latitude
        info.longitude = longitude
        info.user_id = user_id

        db.session.add(info)
        db.session.commit()

        if media_ids:

            for media_id in media_ids:
                media_id = reverse_id(media_id)
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
                    media.info_id = info.id

                    media.isorphan = False
                    media.expired = None

                    file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                    new_path = os.path.join(current_app.config.get('INFO_FOLDER'), media.filename)

                    is_dir_exist(new_path)

                    if is_file_exist(file_path):
                        os.rename(file_path, new_path)

                    db.session.commit() 

        schema = InfoSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(info).data

        # send notif to all users

        return jsonify({
            'success': True,
            'message': 'Info berhasil ditambahkan.',
            'data': {
                'info': result
            }
        }), 201


api.add_resource(Infos, '/info')


class Info(Resource):
    
    '''
    @method GET
    @endpoint /v1/info/<int:info_id>
    @permission jwt_required
    @return panic info list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('info', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, info_id):

        ''' View Info '''

        info_id = reverse_id(info_id)
        info = InfoModel.get_by(info_id)

        if not info:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'info': ['Info tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = InfoSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(info).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'info': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/info/<int:info_id>
    @permission jwt_required
    @return updated info object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.') 

    put_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama hak akses maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'body': fields.Str(required=True, validate=[validate_required]),
        'latitude': fields.Decimal(required=True),
        'longitude': fields.Decimal(required=True),
        'user': fields.Int(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('info', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, info_id):

        ''' Edit Info'''

        info_id = reverse_id(info_id)
        info = InfoModel.get_by(info_id)

        if not info:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'info': ['Info tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title)
        body = bleach.clean(payload['body'], strip=True)
        latitude = payload['latitude']
        longitude = payload['longitude']
        user_id = reverse_id(payload['user'])

        info_exist = InfoModel.query.filter(InfoModel.slug==slug, InfoMode.id!=info.id).first()

        if info_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'info': ['Info sudah terdaftar, silahkan ganti judul.']
                    }
                }
            }), 404

        info.title = title
        info.slug = slug
        info.body = body
        info.latitude = latitude
        info.longitude = longitude
        info.user_id = user_id

        db.session.commit()

        schema = InfoSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(info).data

        return jsonify({
            'success': True,
            'message': 'Info berhasil diubah.',
            'data': {
                'info': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/info/<int:info_id>
    @permission jwt_required
    @return deleted info object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('info', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, info_id):

        ''' Delete Info '''

        info_id = reverse_id(info_id)
        info = InfoModel.query.get(info_id)

        if not info:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'info': ['Info tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = InfoSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(info).data

        if info.medias:
            for media in info.medias:
                os.remove(os.path.join(current_app.config.get('INFO_FOLDER'), media.filename))

        db.session.delete(info)
        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Info berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'info': result
            }
        }), 200


api.add_resource(Info, '/info/<int:info_id>')


class Comments(Resource):
    
    '''
    @method GET
    @endpoint /v1/comments
    @permission jwt_required
    @return comment list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'user': fields.Int(missing=None),
        'info': fields.Int(missing=None),
        'announcement': fields.Int(missing=None),
        'resident_report': fields.Int(missing=None),
        'product': fields.Int(missing=None),
        'panic': fields.Int(missing=None),
        'freebies': fields.Int(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('comment', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Comment '''

        q = args['q']
        user_id = reverse_id(args['user']) if args['user'] else None
        info_id = reverse_id(args['info']) if args['info'] else None
        announcement_id = reverse_id(args['announcement']) if args['announcement'] else None
        resident_report_id = reverse_id(args['resident_report']) if args['resident_report'] else None
        product_id = reverse_id(args['product']) if args['product'] else None
        panic_id = reverse_id(args['panic']) if args['panic'] else None
        freebies_id = reverse_id(args['freebies']) if args['freebies'] else None

        page = args['page']
        perpage = args['perpage']

        total = CommentModel.count_all(q, user_id, info_id, announcement_id, resident_report_id, product_id, panic_id, freebies_id)
        query = CommentModel.get_all(q, user_id, info_id, announcement_id, resident_report_id, product_id, panic_id, freebies_id, page, perpage)

        schema = CommentSchema(only=args['only'], exclude=args['exclude'], many=True)
        comments = schema.dump(query).data
        pagination = Pagination('api.comments', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'comments': comments,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/comments
    @permission jwt_required
    @return new comment object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'comment': fields.Str(required=True, validate=[validate_required]),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'mapurl': fields.Str(missing=None),
        'user': fields.Int(required=True),
        'info': fields.Int(missing=None),
        'announcement': fields.Int(missing=None),
        'resident_report': fields.Int(missing=None),
        'product': fields.Int(missing=None),
        'panic': fields.Int(missing=None),
        'freebies': fields.Int(missing=None),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('comment', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create comment '''

        comment = bleach.clean(payload['comment'], strip=True)
        latitude = payload['latitude']
        longitude = payload['longitude']
        mapurl = payload['mapurl']
        user_id = reverse_id(payload['user'])
        info_id = reverse_id(payload['info']) if payload['info'] else None
        announcement_id = reverse_id(payload['announcement']) if payload['announcement'] else None
        resident_report_id = reverse_id(payload['resident_report']) if payload['resident_report'] else None
        product_id = reverse_id(payload['product']) if payload['product'] else None
        panic_id = reverse_id(payload['panic']) if payload['panic'] else None
        freebies_id = reverse_id(payload['freebies']) if payload['freebies'] else None
        media_ids = payload['medias'] 

        if latitude and longitude:
            comment = comment + '<br><a class="geo" data-href="geo:'+str(latitude)+','+str(longitude)+'?z=18">Lihat Lokasi</a>'

        comm = CommentModel()
        comm.comment = comment
        comm.latitude = latitude
        comm.longitude = longitude
        comm.mapurl = mapurl
        comm.user_id = user_id

        if info_id:
            comm.info_id = info_id
        elif announcement_id:
            comm.announcement_id = announcement_id
        elif resident_report_id:
            comm.resident_report_id = resident_report_id
        elif product_id:
            comm.product_id = product_id
        elif panic_id:
            comm.panic_id = panic_id
        elif freebies_id:
            comm.freebies_id = freebies_id

        db.session.add(comm)
        db.session.commit()

        if media_ids:

            for media_id in media_ids:
                media_id = reverse_id(media_id)
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
                    media.comment_id = comm.id

                    media.isorphan = False
                    media.expired = None

                    file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                    new_path = os.path.join(current_app.config.get('COMMENT_FOLDER'), media.filename)

                    is_dir_exist(new_path)

                    if is_file_exist(file_path):
                        os.rename(file_path, new_path)

                    db.session.commit() 

        schema = CommentSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(comm).data

        # send notif to all users

        if info_id:
            comments = CommentModel.query.filter_by(info_id=info_id).all()
            ids = [] 
            if comments:
                for c in comments:
                    if c.user_id != user_id and c.user_id not in ids:

                        notif = UserNotificationModel()
                        notif.nid = c.comment_user.nid
                        notif.title = 'RingSatu'
                        notif.body = c.comment_user.profile.fullname + 'memberikan komentar untuk info: ' + c.comment_info.title
                        notif.user_id = c.comment_user.id

                        db.session.add(notif)
                        db.session.commit()

                        if c.comment_user.nid:
                            sendnotif_queue.apply_async(kwargs={
                                'player_ids': [c.comment_user.nid],
                                'title': 'RingSatu',
                                'body': c.comment_user.profile.fullname + ' memberikan komentar untuk info: ' + c.comment_info.title
                            }, countdown=60)

                        ids.append(c.user_id)

        elif freebies_id:
            comments = CommentModel.query.filter_by(freebies_id=freebies_id).all()
            ids = [] 
            if comments:
                for c in comments:
                    if c.user_id != user_id and c.user_id not in ids:

                        notif = UserNotificationModel()
                        notif.nid = c.comment_user.nid
                        notif.title = 'RingSatu'
                        notif.body = c.comment_user.profile.fullname + 'memberikan komentar untuk barang loak: ' + c.comment_info.title
                        notif.user_id = c.comment_user.id

                        db.session.add(notif)
                        db.session.commit()

                        if c.comment_user.nid:
                            sendnotif_queue.apply_async(kwargs={
                                'player_ids': [c.comment_user.nid],
                                'title': 'RingSatu',
                                'body': c.comment_user.profile.fullname + ' memberikan komentar untuk barang loak: ' + c.comment_info.title
                            }, countdown=60)

                        ids.append(c.user_id)

        elif announcement_id:
            comments = CommentModel.query.filter_by(announcement_id=announcement_id)
            ids = [] 
            if comments:
                for c in comments:
                    if c.user_id != user_id and c.user_id not in ids:

                        notif = UserNotificationModel()
                        notif.nid = c.comment_user.nid
                        notif.title = 'RingSatu'
                        notif.body = c.comment_user.profile.fullname + ' memberikan komentar untuk berita: ' + c.comment_announcement.title
                        notif.user_id = c.comment_user.id

                        db.session.add(notif)
                        db.session.commit()

                        if c.comment_user.nid:
                            sendnotif_queue.apply_async(kwargs={
                                'player_ids': [c.comment_user.nid],
                                'title': 'RingSatu',
                                'body': c.comment_user.profile.fullname + ' memberikan komentar untuk berita: ' + c.comment_announcement.title
                            }, countdown=60)

                        ids.append(c.user_id)

        elif resident_report_id:
            comments = CommentModel.query.filter_by(resident_report_id=resident_report_id)
            ids = []
            if comments:
                for c in comments:
                    if c.user_id != user_id and c.user_id not in ids:

                        notif = UserNotificationModel()
                        notif.nid = c.comment_user.nid
                        notif.title = 'RingSatu '
                        notif.body = c.comment_user.profile.fullname + ' memberikan komentar untuk laporan warga: ' + c.comment_resident_report.title
                        notif.user_id = c.comment_user.id

                        db.session.add(notif)
                        db.session.commit()

                        if c.comment_user.nid:
                            sendnotif_queue.apply_async(kwargs={
                                'player_ids': [c.comment_user.nid],
                                'title': 'RingSatu',
                                'body': c.comment_user.profile.fullname + ' memberikan komentar untuk laporan warga: ' + c.comment_resident_report.title
                            }, countdown=60)

                        ids.append(c.user_id)

        elif product_id:
            comments = CommentModel.query.filter_by(product_id=product_id)
            ids = []
            if comments:
                for c in comments:
                    if c.user_id != user_id and c.user_id not in ids:

                        notif = UserNotificationModel()
                        notif.nid = c.comment_user.nid
                        notif.title = 'Ringsatu'
                        notif.body = c.comment_user.profile.fullname + ' memberikan komentar untuk produk: ' + c.comment_product.title
                        notif.user_id = c.comment_user.id

                        db.session.add(notif)
                        db.session.commit()

                        if c.comment_user.nid:
                            sendnotif_queue.apply_async(kwargs={
                                'player_ids': [c.comment_user.nid],
                                'title': 'Ringsatu',
                                'body': c.comment_user.profile.fullname + ' memberikan komentar untuk produk: ' + c.comment_product.title
                            }, countdown=60)

                        ids.append(c.user_id)

        elif panic_id:
            ids = []
            panic = PanicModel.query.get(panic_id)
            participants = panic.participants.filter_by(status='approved').all()
            if participants:
                pusher = PusherSocket()
                for p in participants:
                    if p.user_id != user_id and p.user_id not in ids:

                        medias_schema = MediaCommentSchema(many=True)
                        medias = medias_schema.dump(comm.medias).data

                        profile_media = None

                        if comm.comment_user.profile.media:
                            profile_media_schema = MediaProfileSchema()
                            profile_media = profile_media_schema.dump(comm.comment_user.profile.media).data

                        pusher.trigger('panic', 'discussion', {
                            'id': reverse_id(comm.id),
                            'comment': comm.comment,
                            'created': arrow.get(comm.created).format('DD-MMMM-YYYY HH:mm'),
                            'updated': arrow.get(comm.updated).format('DD-MMMM-YYYY HH:mm'),
                            'latitude': str(comm.latitude),
                            'longitude': str(comm.longitude),
                            'mapurl': comm.mapurl,
                            'medias': medias,
                            'user': {
                                'id': reverse_id(comm.user_id),
                                'profile': {
                                    'fullname': comm.comment_user.profile.fullname,
                                    'media': profile_media
                                }
                            },
                            'recipient': {
                                'id': reverse_id(p.user_id)
                            }
                        })

                        notif = UserNotificationModel()
                        notif.nid = p.user.nid
                        notif.title = 'RingSatu'
                        notif.body = comm.comment_user.profile.fullname + ' mengirimkan pesan baru dalam diskusi grup panik: ' + panic.name
                        notif.user_id = p.user_id

                        db.session.add(notif)
                        db.session.commit()

                        if p.user.nid:
                            sendnotif_queue.apply_async(kwargs={
                                'player_ids': [p.user.nid],
                                'title': 'RingSatu',
                                'body': comm.comment_user.profile.fullname + ' mengirimkan pesan baru dalam diskusi grup panik: ' + panic.name
                            }, countdown=60)

                        ids.append(p.user_id)

        return jsonify({
            'success': True,
            'message': 'Komentar berhasil ditambahkan.',
            'data': {
                'comment': result
            }
        }), 201


api.add_resource(Comments, '/comments')


class Comment(Resource):
    
    '''
    @method GET
    @endpoint /v1/comments/<int:comment_id>
    @permission jwt_required
    @return panic comment list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('comment', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, comment_id):

        ''' View Comment '''

        comment_id = reverse_id(comment_id)
        comment = CommentModel.get_by(comment_id)

        if not comment:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'comment': ['Info tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = CommentSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(comment).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'comment': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/comments/<int:comment_id>
    @permission jwt_required
    @return updated comment object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.') 

    put_args = {
        'comment': fields.Str(required=True, validate=[validate_required]),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'mapurl': fields.Str(missing=None),
        'user': fields.Int(required=True),
        'info': fields.Int(missing=None),
        'announcement': fields.Int(missing=None),
        'resident_report': fields.Int(missing=None),
        'product': fields.Int(missing=None),
        'panic': fields.Int(missing=None),
        'freebies': fields.Int(missing=None),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('comment', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, comment_id):

        ''' Edit Comment'''

        comment_id = reverse_id(comment_id)
        comment = CommentModel.get_by(comment_id)

        if not comment:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'comment': ['Info tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        comment = bleach.clean(payload['comment'], strip=True)
        latitude = payload['latitude']
        longitude = payload['longitude']
        mapurl = payload['mapurl']
        user_id = reverse_id(payload['user'])
        info_id = reverse_id(payload['info']) if payload['info'] else None
        announcement_id = reverse_id(payload['announcement']) if payload['announcement'] else None
        resident_report_id = reverse_id(payload['resident_report']) if payload['resident_report'] else None
        product_id = reverse_id(payload['product']) if payload['product'] else None
        panic_id = reverse_id(payload['panic']) if payload['panic'] else None
        freebies_id = reverse_id(payload['freebies']) if payload['freebies'] else None
        media_ids = payload['medias'] 

        comment = CommentModel()
        comment.comment = comment
        comment.latitude = latitude
        comment.longitude = longitude
        comment.mapurl = mapurl
        comment.user_id = user_id

        if info_id:
            comment.info_id = info_id
        elif announcement_id:
            comment.announcement_id = announcement_id
        elif resident_report_id:
            comment.resident_report_id = resident_report_id
        elif product_id:
            comment.product_id = product_id
        elif panic_id:
            comment.panic_id = panic_id
        elif freebies_id:
            comment.freebies_id = freebies_id

        db.session.commit()

        schema = CommentSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(comment).data

        return jsonify({
            'success': True,
            'message': 'Komentar berhasil diubah.',
            'data': {
                'comment': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/comments/<int:comment_id>
    @permission jwt_required
    @return deleted comment object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('comment', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, comment_id):

        ''' Delete Comment '''

        comment_id = reverse_id(comment_id)
        comment = CommentModel.query.get(comment_id)

        if not comment:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'comment': ['Info tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = InfoSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(comment).data

        if comment.medias:
            for media in comment.medias:
                os.remove(os.path.join(current_app.config.get('COMMENT_FOLDER'), media.filename))

        db.session.delete(comment)
        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Komentar berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'comment': result
            }
        }), 200


api.add_resource(Comment, '/comment/<int:comment_id>')

class Chats(Resource):
    
    '''
    @method GET
    @endpoint /v1/chats
    @permission jwt_required
    @return chat list
    '''  

    get_args = {
        'chattype': fields.Str(validate=validate.OneOf(choices=['sender', 'recipient'], error='Pilihan tipe chat tidak valid.'), missing=None),
        'totalonly': fields.Bool(missing=None), 
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('chat', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Chat '''

        page = args['page']
        perpage = args['perpage']
        chattype = args['chattype']
        totalonly = args['totalonly']

        sender_id = None
        recipient_id = None

        if chattype == 'sender':
            sender_id = g.current_user.id
        elif chattype == 'recipient':
            recipient_id = g.current_user.id

        if totalonly == None:

            total = ChatModel.count_all(sender_id, recipient_id)
            query = ChatModel.get_all(sender_id, recipient_id, page, perpage)

            schema = ChatSchema(only=args['only'], exclude=args['exclude'], many=True)
            chats = schema.dump(query).data
            pagination = Pagination('api.chats', total, **args)

            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'chats': chats,
                    'pagination': pagination.paginate
                }
            }), 200

        else:

            total_sender = ChatModel.count_all(g.current_user.id, None)
            total_recipient = ChatModel.count_all(None, g.current_user.id)

            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'total_sender': total_sender,
                    'total_recipient': total_recipient
                }
            }), 200


    
    '''
    @method POST
    @endpoint /v1/chats
    @permission jwt_required
    @return new chat message object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'recipient_id': fields.Int(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('chat', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create Chat '''

        sender_id = g.current_user.id
        recipient_id = reverse_id(payload['recipient_id'])
        recipient = UserModel.query.get(recipient_id)

        if not recipient:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan']
                    }
                }
            }), 404

        chat = ChatModel.query.filter(ChatModel.sender_id==g.current_user.id, ChatModel.recipient_id==recipient_id).first()

        if not chat:
            chat = ChatModel()
            chat.sender_id = sender_id
            chat.recipient_id = recipient_id

            db.session.add(chat)
            db.session.commit()

        schema = ChatSchema(only=payload['only'], exclude=payload['exclude'])
        chat = schema.dump(chat).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'chat': chat
            }
        }), 201


api.add_resource(Chats, '/chats')


class ChatMessages(Resource):

    '''
    @method GET
    @endpoint /v1/chats/<int:chat_id>
    @permission jwt_required
    @return chat list
    '''  

    get_args = {
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[10, 25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('chat', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args, chat_id):

        ''' All Chat Message '''

        page = args['page']
        perpage = args['perpage']

        chat_id = reverse_id(chat_id)
        chat_query = ChatModel.query.get(chat_id)

        total = ChatMessageModel.count_all(chat_id)
        query = ChatMessageModel.get_all(chat_id, page, perpage)

        chat_schema = ChatSchema(only='id,sender.id,sender.profile.fullname,recipient.id,recipient.profile.fullname'.split(','))
        chat = chat_schema.dump(chat_query).data

        schema = ChatMessageSchema(only=args['only'], exclude=args['exclude'], many=True)
        messages = schema.dump(query).data

        args['chat_id'] = reverse_id(chat_id)

        pagination = Pagination('api.chatmessages', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'chat': chat,
                'messages': messages,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/chats/<int:chat_id>
    @permission jwt_required
    @return new chat message object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'message': fields.Str(required=True, validate=[validate_required]),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'mapurl': fields.Str(missing=None),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('chat', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload, chat_id):

        ''' Chat Message '''

        chat_id = reverse_id(chat_id)
        chat = ChatModel.query.get(chat_id)

        if not chat:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'chat': ['Chat tidak ditemukan atau sudah dihapus']
                    }
                }
            }), 404

        message = bleach.clean(payload['message'], strip=True)
        latitude = payload['latitude']
        longitude = payload['longitude']
        mapurl = payload['mapurl']
        media_ids = payload['medias'] 

        if latitude and longitude:
            message = message + '<a class="geo" data-href="geo:'+str(latitude)+','+str(longitude)+'?z=18">Lihat Lokasi</a>'

        chat_message = ChatMessageModel()
        chat_message.message = message
        chat_message.latitude = latitude
        chat_message.longitude = longitude
        chat_message.mapurl = mapurl
        chat_message.user_id = g.current_user.id
        chat_message.chat_id = chat_id

        db.session.add(chat_message)
        db.session.commit()

        if media_ids:

            for media_id in media_ids:
                media_id = reverse_id(media_id)
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
                    media.message_id = chat_message.id

                    media.isorphan = False
                    media.expired = None

                    file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                    new_path = os.path.join(current_app.config.get('MESSAGE_FOLDER'), media.filename)

                    is_dir_exist(new_path)

                    if is_file_exist(file_path):
                        os.rename(file_path, new_path)

                    db.session.commit() 

        schema = ChatMessageSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(chat_message).data

        # send notif to recipient

        pusher = PusherSocket()

        medias = []
        sender_profile_media = None
        recipient_profile_media = None

        if chat_message.medias:
            for m in chat_message.medias:
                message_media_schema = MediaChatSchema()
                message_media = message_media_schema.dump(m).data
                medias.append({
                    'id': message_media['id'],
                    'url': {
                        'small': message_media['url']['small'],
                        'medium': message_media['url']['medium'],
                        'large': message_media['url']['large'],
                        'original': message_media['url']['original']
                    }
                })

        if chat.sender.profile.media:
            sender_profile_media_schema = MediaProfileSchema()
            sender_media = sender_profile_media_schema.dump(chat.sender.profile.media).data
            sender_profile_media = {
                'id': sender_media['id'],
                'url': {
                    'small': sender_media['url']['small'],
                    'medium': sender_media['url']['medium'],
                    'large': sender_media['url']['large'],
                    'original': sender_media['url']['original']
                }
            }

        if chat.recipient.profile.media:
            recipient_profile_media_schema = MediaProfileSchema()
            recipient_media = recipient_profile_media_schema.dump(chat.recipient.profile.media).data
            recipient_profile_media = {
                'id': recipient_media['id'],
                'url': {
                    'small': recipient_media['url']['small'],
                    'medium': recipient_media['url']['medium'],
                    'large': recipient_media['url']['large'],
                    'original': recipient_media['url']['original']
                }
            }

        pusher.trigger('chat', 'message', {
            'chat': {
                'id': reverse_id(chat.id),
                'sender': {
                    'id': reverse_id(chat.sender.id),
                    'nid': chat.sender.nid,
                    'profile': {
                        'fullname': chat.sender.profile.fullname,
                        'media': sender_profile_media
                    }
                },
                'recipient': {
                    'id': reverse_id(chat.recipient.id),
                    'nid': chat.recipient.nid,
                    'profile': {
                        'fullname': chat.recipient.profile.fullname,
                        'media': recipient_profile_media
                    }
                }
            },
            'id': reverse_id(chat_message.id),
            'message': message,
            'latitude': str(latitude) if latitude else None,
            'longitude': str(longitude) if longitude else None,
            'mapurl': mapurl,
            'created': arrow.get(chat.created).format('YYYY-MM-DD HH:mm:ss'),
            'updated': arrow.get(chat.created).format('YYYY-MM-DD HH:mm:ss'),
            'medias': medias,
            'user': {
                'id': reverse_id(chat_message.user.id),
                'nid': chat_message.user.nid
            }
        })

        if g.current_user.id == chat.sender.id:
            
            notif = UserNotificationModel()
            notif.nid = chat.recipient.nid
            notif.title = g.current_user.profile.fullname
            notif.body = message
            notif.user_id = chat.recipient.id

            db.session.add(notif)
            db.session.commit()

            if chat.recipient.nid:
                sendnotif_queue.apply_async(kwargs={
                    'player_ids': [chat.recipient.nid],
                    'title': 'RingSatu',
                    'body': g.current_user.profile.fullname + ' mengirimkan pesan chat'
                }, countdown=60)

        elif g.current_user.id == chat.recipient.id:

            notif = UserNotificationModel()
            notif.nid = chat.sender.nid
            notif.title = g.current_user.profile.fullname
            notif.body = message
            notif.user_id = chat.sender.id

            db.session.add(notif)
            db.session.commit()

            if chat.sender.nid:
                sendnotif_queue.apply_async(kwargs={
                    'player_ids': [chat.sender.nid],
                    'title': 'RingSatu',
                    'body': g.current_user.profile.fullname + ' mengirimkan pesan chat'
                }, countdown=60)

        return jsonify({
            'success': True,
            'message': 'Pesan berhasil dikirimkan.',
            'data': {
                'message': result
            }
        }), 201


api.add_resource(ChatMessages, '/chats/<int:chat_id>')