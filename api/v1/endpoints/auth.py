# -*- coding: utf-8 -*-
import arrow
import bleach
import redis
import time
import re

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    jwt_refresh_token_required,
    get_jti,
    get_jwt_identity,
    get_raw_jwt
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    use_kwargs,
    parser
)
from slugify import slugify
from sqlalchemy.sql.expression import func, and_, or_

from api import cache, db, jwt, mail
from api.v1 import api
from api.v1.models.users import (
    UserModel,
    PermissionModel,
    RoleModel,
    ProfileModel
)
from api.v1.schemas.users import(
    UserSchema
)
from api.v1.models.histories import (
    HistoryModel
)
from api.v1.tasks import (
    sendsms_queue
)
from api.v1.utils import(
    reverse_id,
    parse_verification_token,
    get_clients_ip,
    normalize_phone_number,
    denormalize_phone_number,
    calculate_age
)
from api.v1.decorators import (
    anonymous_required,
    permission_required,
    current_user
)

ACCESS_EXPIRES = timedelta(days=365)
REFRESH_EXPIRES = timedelta(days=365)
REFRESH_REVOKED = timedelta(days=365)


revoked_store = redis.StrictRedis(
    host='127.0.0.1', port=18567, db=2, password='Pr0m0th3u5GAMON', decode_responses=True
)

@jwt.user_claims_loader
def add_claims_to_access_token(user):
    return {'id': reverse_id(user.id)}


@jwt.user_identity_loader
def user_identity_lookup(user):
    return reverse_id(user.id)


@jwt.unauthorized_loader
def unauthorized_loader_callback(callback):
    return jsonify({
        'success': False,
        'errors': {
            'code': 401,
            'messages': {
                'authorization': ['Anda tidak diizinkan mengakses sumber ini.']
            }
        }
    }), 401


@jwt.expired_token_loader
def expired_token_loader_callback():
    return jsonify({
        'success': False,
        'errors': {
            'code': 401,
            'messages': {
                'authorization': ['Token telah kadaluwarsa.']
            }
        }
    }), 401


@jwt.invalid_token_loader
def invalid_token_loader_callback(callback):
    return jsonify({
        'success': False,
        'errors': {
            'code': 401,
            'messages': {
                'authorization': ['Token tidak valid.']
            }
        }
    }), 401


@jwt.revoked_token_loader
def revoked_token_callback():
    return jsonify({
        'success': False,
        'errors': {
            'code': 401,
            'messages': {
                'authorization': ['Anda telah keluar. Harap masuk kembali untuk melanjutkan.']
            }
        }
    }), 401


@jwt.token_in_blacklist_loader
def check_if_token_is_revoked(callback):
    jti = callback['jti']
    entry = revoked_store.get(jti)
    if entry is None:
        return True
    return entry == 'true'


class Login(Resource):
    
    '''
    @method POST
    @endpoint /v1/auth/login
    @permission anonymous
    @return verification_code
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')  

    def validate_phone(value):
        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', value)

        if phone_match is None:
            raise ValidationError('No. handphone tidak valid. Harus diawali dengan 08, +628 atau 628') 

    login_args = {
        'identity': fields.Str(required=True, validate=[validate_required, validate_phone]),
        'ui': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['dashboard', 'mobile'], labels=['Dashboard', 'Mobile'], error='Pilihan ui tidak valid.')
        ]),
    }

    @cross_origin()
    @anonymous_required()
    @use_args(login_args, locations=['json'])
    def post(self, payload):

        ''' Login '''

        ui = payload['ui']
        phone = normalize_phone_number(payload['identity'])
        user = UserModel.query.filter_by(phone=phone).first()                  

        if user is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'login_failed': ['No. handphone tidak terdaftar.']
                    }
                }
            }), 422

        elif (user.role.slug == 'public' and ui == 'dashboard') or (ui == 'dashboard' and not user.check_permission('dashboard', 'viewall')):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'login_failed': ['Anda tidak memiliki izin untuk mengakses sumber ini.']
                    }
                }
            }), 422

        elif user.status != 'active':
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'account_status': ['Akun Anda tidak aktif atau telah ditangguhkan oleh administrator.']
                    }
                }
            }), 422

        else:

            verification_code = user.verify_me()
            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'verification_code': verification_code
                }                
            }), 200


api.add_resource(Login, '/auth/login')


class Register(Resource):
    
    '''
    @method POST
    @endpoint /v1/auth/register
    @permission anonymous
    @return success
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')  

    def validate_phone(value):
        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', value)

        if phone_match is None:
            raise ValidationError('No. handphone tidak valid. Harus diawali dengan 08, +628 atau 628') 

    register_args = {
        'fullname': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9\s]{1,255})$', error='Panjang nama lengkap maksimal 255 karakter. Hanya huruf, angka, dan spasi yang diperbolehkan.')
        ]),
        'phone': fields.Str(required=True, validate=[validate_required,validate_phone])
    }

    @cross_origin()
    @anonymous_required()
    @use_args(register_args, locations=['json'])
    def post(self, payload):

        ''' Register '''

        fullname = bleach.clean(payload['fullname']) 
        phone = normalize_phone_number(payload['phone'])
        user = UserModel.query.filter_by(phone=phone).first()                  

        if user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'register_failed': ['No. handphone sudah terdaftar.']
                    }
                }
            }), 422

        u = UserModel()
        u.phone = phone
        u.status = 'active'
        u.role_id = 10 # public role

        db.session.add(u)
        db.session.flush()

        profile = ProfileModel()
        profile.fullname = fullname
        profile.phone = phone
        profile.user_id = u.id
        profile.invitable = True

        db.session.add(profile) 
        db.session.commit()
        
        return jsonify({
            'success': True,
            'message': 'Selamat akun anda berhasil didaftarkan',
            'data': {}                
        }), 201


api.add_resource(Register, '/auth/register')


class Verify(Resource):
    
    '''
    @method POST
    @endpoint /v1/auth/verify
    @permission anonymous
    @return access_token
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Silakan isi semua kolom yang wajib diisi')   

    verify_args = {
        'code': fields.Str(required=True, validate=[validate_required]),
        'passkey': fields.Str(required=True, validate=[validate_required]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @anonymous_required()
    @use_args(verify_args, locations=['json'])
    def post(self, payload):

        ''' Verification '''

        code = payload['code']
        passkey = payload['passkey']

        data = parse_verification_token(code)

        list_keys = ['module', 'signature', 'expire']

        if not any(p in list_keys for p in data) or (data['expire'] <= int(time.time())):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'verification_code': ['Kode verifikasi salah atau telah kedaluwarsa.']
                    }
                }
            }), 422

        else:

            user = UserModel.query.filter(
                UserModel.passkey==str(passkey), 
                UserModel.verification_code==str(data['signature']), 
                UserModel.passkey_expire >= int(time.time())).first()  

            if user is None:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'passkey': ['Kode OTP salah atau telah kadaluwarsa']
                        }
                    }
                }), 422

            else:  

                user.passkey = None
                user.passkey_expire = None
                user.verification_code = None
                user.isonline = True

                user_agent = ua_parser.Parse(request.headers.get("User-Agent"))

                owner_id = None

                if user.role.slug != 'root':
                    owner_id = user.parents.id if user.parents else user.id
                
                history = HistoryModel()
                history.module_id = user.id
                history.module = 'User'
                history.action = 'Login'
                history.content = {}
                history.remote_address = get_clients_ip()
                history.user_agent = user_agent
                history.user_id = user.id
                history.owner_id = owner_id

                db.session.add(history)
                db.session.commit()              
            
                access_token = create_access_token(user)
                refresh_token = create_refresh_token(user)

                access_jti = get_jti(encoded_token=access_token)
                refresh_jti = get_jti(encoded_token=refresh_token)
                revoked_store.set(access_jti, 'false', ACCESS_EXPIRES)
                revoked_store.set(refresh_jti, 'false', REFRESH_EXPIRES)

                schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
                profile = schema.dump(user).data

                return jsonify({
                    'success': True,
                    'message': '',
                    'data': {
                        'user': profile,
                        'access_token': access_token,
                        'refresh_token': refresh_token
                    }  
                }), 200



    '''
    @method PUT
    @endpoint /v1/auth/verify
    @permission anonymous
    @return verification_code
    '''

    reverify_args = {
        'code': fields.Str(required=True, validate=[validate_required])
    }

    @cross_origin()
    @use_args(reverify_args, locations=['json'])
    def put(self, payload):
        
        ''' Reverify '''        
        
        code = payload['code']

        data = parse_verification_token(code)
        list_keys = ['signature', 'expire']

        if not any(p in list_keys for p in data):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'verification_code': ['Kode verifikasi salah atau telah kedaluwarsa.']
                    }
                }
            }), 422

        else:

            user = UserModel.query.filter_by(verification_code=data['signature']).first()
            if user is None:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'verification_code': ['Kode OTP salah atau telah kedaluwarsa.']
                        }
                    }
                }), 422
            else:
                
                verification_code = user.verify_me()
                return jsonify({
                    'success': True,
                    'message': '',
                    'data': {
                        'verification_code': verification_code
                    }
                }), 200


api.add_resource(Verify, '/auth/verify')


class RefreshToken(Resource):
        
    '''
    @method POST
    @endpoint /v1/auth/refresh
    @permission jwt_refresh_token_required
    @return new access token
    '''

    @cross_origin()
    @jwt_refresh_token_required
    def post(self):
        
        ''' Refresh Token '''
        
        userid = reverse_id(get_jwt_identity())
        user = UserModel.query.get(userid)

        if user is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 401,
                    'messages': {
                        'authorization': ['Anda tidak diizinkan mengakses sumber ini.']
                    }
                }
            }), 401

        access_token = create_access_token(user)
        access_jti = get_jti(encoded_token=access_token)

        revoked_store.set(access_jti, 'false', ACCESS_EXPIRES)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'access_token': access_token
            }
        }), 200
        

api.add_resource(RefreshToken, '/auth/refresh')


class AccessRevoke(Resource):
    
    '''
    @method DELETE
    @endpoint /v1/auth/revoke/access
    @permission jwt_required
    @return success message
    '''

    @cross_origin()
    @jwt_required
    def delete(self):
        
        ''' Delete Access Token (Logout) '''

        userid = reverse_id(get_jwt_identity())
        user = UserModel.query.get(userid)

        if user is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'code': ['Token tidak valid']
                    }
                }
            }), 422

        user.isonline = False

        owner_id = None

        if user.role.slug != 'root':
            owner_id = user.parents.id if user.parents else user.id

        history = HistoryModel()
        history.module_id = user.id
        history.module = 'User'
        history.action = 'Logout'
        history.content = {}
        history.remote_address = get_clients_ip()
        history.user_agent = ua_parser.Parse(request.headers.get("User-Agent"))
        history.user_id = user.id
        history.owner_id = owner_id

        db.session.add(history)
        db.session.commit()

        jti = get_raw_jwt()['jti']
        revoked_store.set(jti, 'true', ACCESS_EXPIRES)

        return jsonify({
            'success': True,
            'message': 'Anda telah keluar. Selamat beraktivitas.',
            'data': {}
        }), 200


api.add_resource(AccessRevoke, '/auth/revoke/access')


class RefreshRevoke(Resource):

    '''
    @method DELETE
    @endpoint /v1/auth/revoke/refresh
    @permission jwt_refresh_token_required
    @return success message
    '''

    @cross_origin()
    @jwt_refresh_token_required
    def delete(self):

        ''' Delete Refresh Token (Logout) '''

        jti = get_raw_jwt()['jti']
        revoked_store.set(jti, 'true', REFRESH_REVOKED)

        return jsonify({
            'success': True,
            'message': 'Anda telah keluar. Selamat beraktivitas.',
            'data': {}
        }), 200


api.add_resource(RefreshRevoke, '/auth/revoke/refresh')


class Device(Resource):

    '''
    @method PUT
    @endpoint /v1/auth/device
    @permission @jwt_required
    @return success message
    '''

    @cross_origin()
    @jwt_required
    @current_user()
    def put(self):

        ''' Update device id '''

        user = UserModel.query.get(g.current_user.id)
        nid = request.headers.get('nid')

        if not user.nid:
            user.nid = nid
            db.session.commit()

        elif user.nid != nid:
            user.nid = nid
            db.session.commit()

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'nid': user.nid
            }
        }), 200


api.add_resource(Device, '/auth/device')
