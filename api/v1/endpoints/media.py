# -*- coding: utf-8 -*-
import os
import time
import uuid

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)
from werkzeug.utils import secure_filename
from sqlalchemy import asc, desc
from sqlalchemy.sql.expression import func, and_, or_

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    use_kwargs,
    parser
)
from slugify import slugify
from api import cache, db, jwt
from api.v1 import api
from api.v1.models.media import(
    MediaModel
)
from api.v1.schemas.media import (
    MediaTmpSchema,
    MediaCategorySchema,
    MediaFinancialReportSchema,
    MediaInventorySchema,
    MediaAnnouncementSchema,
    MediaResidentReportSchema,
    MediaProductSchema,
    MediaResidentialSchema,
    MediaProfileSchema,
    MediaPanicSchema,
    MediaFreebiesSchema
)
from api.v1.models.settings import (
    CategoryModel
)
from api.v1.models.residentials import (
    AnnouncementModel,
    ResidentReportModel,
    FinancialReportModel,
    InventoryModel,
    InventoryMovementModel,
    InfoModel,
    CommentModel,
    PanicModel
)
from api.v1.models.marketplace import (
    ProductModel,
    FreebiesModel
)
from api.v1.models.users import (
    UserModel,
    ProfileModel
)
from api.v1.schemas.users import (
    UserSchema
)
from api.v1.utils import(
    allowed_file, 
    is_dir_exist, 
    reverse_id
)
from api.v1.decorators import (
    permission_required,
    current_user,
    ownership
)

class MediaTmp(Resource):
    
    """
    @method POST
    @endpoint /v1/media/tmp
    @permission jwt_required, media, create
    @return media object
    """

    @cross_origin()    
    @jwt_required
    @current_user()
    @permission_required('media', 'create')    
    def post(self):
        
        args = {'media': fields.Field(required=True)}
        data = parser.parse(args, locations=['files'])

        files = data['media']

        fn = secure_filename(files.filename)
        name, extension = os.path.splitext(files.filename)
        original_filename = files.filename
        filename = str(uuid.uuid4()) + extension

        mimetype = files.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Tipe file tidak diperbolehkan.']
                    }
                }
            }), 422

        uploaded_file_path = os.path.join(current_app.config.get('TMP_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        files.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('TMP_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih besar 8MB']
                    }
                }
            }), 422
        
        media = MediaModel()
        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype 
        media.ismain = 0      
        media.isorphan = 1
        media.isdeleted = False
        media.owner_id = None

        nexthour = datetime.now() + timedelta(hours=1)
        nexthour_timestamp = time.mktime(nexthour.timetuple())
        media.expired = nexthour_timestamp

        db.session.add(media)        
        db.session.commit()
        
        schema = MediaTmpSchema()
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 200


api.add_resource(MediaTmp, '/media/tmp')


class MediaTmpDelete(Resource):

    """
    @method DELETE
    @endpoint /v1/media/tmp/<int:media_id>
    @permission jwt_required, media, delete
    @return delete media object
    """

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    def delete(self, media_id):

        media_id = reverse_id(media_id)
        media = MediaModel.query.get(media_id)

        if media is None:
            return jsonify({
                'success': False,
                'code': 404,
                'errors': {
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus']
                    }
                }
            }), 404

        try:
            os.remove(os.path.join(current_app.config.get('TMP_FOLDER'), media.filename))
            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'File berhasil dihapus dan tidak bisa dikembalikan.',
                'data': {}
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,                
                'errors': {
                    'code': 422,
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaTmpDelete, '/media/tmp/<int:media_id>')


class MediaProfile(Resource):

    """
    @method POST
    @endpoint /v1/media/profile
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, profile_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        profile = g.current_user.profile

        media = MediaModel.query.filter_by(profile_id=profile.id).first()
        isnew = False

        if media and media.filename:
            os.remove(os.path.join(current_app.config.get('PROFILE_FOLDER'), media.filename))
        else:
            isnew = True
            media = MediaModel()

        media.profile_id = profile.id

        uploaded_file_path = os.path.join(current_app.config.get('PROFILE_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('PROFILE_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype

        if profile.profile_user.residential_id:
            media.residential_id = profile.profile_user.residential_id

        media.owner_id = g.owner_id

        if isnew == True:
            db.session.add(media)
        db.session.commit()

        user = UserModel.query.get(g.current_user.id)

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        if isnew == True:
            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'user': result
                }
            }), 201
        else:
            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'user': result
                }
            }), 200


    """
    @method DELETE
    @endpoint /v1/media/profile/<int:profile_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, profile_id):

        profile_id = reverse_id(profile_id)
        media = MediaModel.query.get(profile_id)

        if media is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaProfileSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('PROFILE_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'Foto profil berhasil dihapus dan tidak bisa dikembalikan.',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaProfile, '/media/profile/<int:profile_id>')


class MediaCategory(Resource):

    """
    @method POST
    @endpoint /v1/media/category/<int:category_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, category_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        category_id = reverse_id(category_id)
        category = CategoryModel.query.get(category_id)

        media = MediaModel.query.filter_by(category_id=category.id).first()
        isnew = False

        if media and media.filename:
            os.remove(os.path.join(current_app.config.get('CATEGORY_FOLDER'), media.filename))
        else:
            isnew = True
            media = MediaModel()

        media.category_id = category.id

        uploaded_file_path = os.path.join(current_app.config.get('CATEGORY_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('CATEGORY_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype
        media.residential_id = g.current_user.residential_id

        media.owner_id = g.owner_id

        if isnew == True:
            db.session.add(media)
        db.session.commit()

        schema = MediaCategorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        if isnew == True:
            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'media': result
                }
            }), 201
        else:
            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'media': result
                }
            }), 200


    """
    @method DELETE
    @endpoint /v1/media/category/<int:category_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, category_id):

        category_id = reverse_id(category_id)
        media = MediaModel.query.get(category_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaCategorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('CATEGORY_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'Media kategori berhasil dihapus dan tidak bisa dikembalikan.',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaCategory, '/media/category/<int:category_id>')


class MediaAnnouncement(Resource):

    """
    @method POST
    @endpoint /v1/media/announcement/<int:announcement_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, announcement_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        announcement_id = reverse_id(announcement_id)
        announcement = AnnouncementModel.query.get(announcement_id)

        if not announcement:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'announcement': ['Pemberitahuan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404


        media = MediaModel()
        media.announcement_id = announcement.id

        uploaded_file_path = os.path.join(current_app.config.get('ANNOUNCEMENT_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('ANNOUNCEMENT_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype
        media.residential_id = g.current_user.residential_id

        media.owner_id = g.owner_id

        db.session.add(media)
        db.session.commit()

        schema = MediaAnnouncementSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


    """
    @method DELETE
    @endpoint /v1/media/announcement/<int:announcement_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, announcement_id):

        announcement_id = reverse_id(announcement_id)

        media = MediaModel.query.get(announcement_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaAnnouncementSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('ANNOUNCEMENT_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'Media berita berhasil dihapus dan tidak bisa dikembalikan.',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaAnnouncement, '/media/announcement/<int:announcement_id>')


class MediaResidentReport(Resource):

    """
    @method POST
    @endpoint /v1/media/residentreport/<int:residentreport_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, residentreport_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        residentreport_id = reverse_id(residentreport_id)
        residentreport = ResidentReportModel.query.get(residentreport_id)
        
        if not residentreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'residentreport': ['Laporan warga tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404


        media = MediaModel()
        media.resident_report_id = residentreport.id

        uploaded_file_path = os.path.join(current_app.config.get('RESIDENT_REPORT_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('RESIDENT_REPORT_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype

        if g.current_user.residential_id:
            media.residential_id = g.current_user.residential_id

        media.owner_id = g.owner_id

        db.session.add(media)
        db.session.commit()

        schema = MediaResidentReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


    """
    @method DELETE
    @endpoint /v1/media/residentreport/<int:residentreport_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, residentreport_id):

        residentreport_id = reverse_id(residentreport_id)

        media = MediaModel.query.get(residentreport_id)
        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaResidentReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('RESIDENT_REPORT_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'Media laporan warga berhasil dihapus dan tidak bisa dikembalikan.',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaResidentReport, '/media/residentreport/<int:residentreport_id>')


class MediaFinancialReport(Resource):

    """
    @method POST
    @endpoint /v1/media/financialreport/<int:financialreport_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, financialreport_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        financialreport_id = reverse_id(financialreport_id)
        financialreport = FinancialReportModel.query.get(financialreport_id)

        if not financialreport:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'financialreport': ['Laporan keuangan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404


        media = MediaModel()
        media.financial_report_id = financialreport.id

        uploaded_file_path = os.path.join(current_app.config.get('FINANCIAL_REPORT_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('FINANCIAL_REPORT_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype

        if g.current_user.residential_id:
            media.residential_id = g.current_user.residential_id

        media.owner_id = g.owner_id

        db.session.add(media)
        db.session.commit()

        schema = MediaResidentReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


    """
    @method DELETE
    @endpoint /v1/media/financialreport/<int:financialreport_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, financialreport_id):

        financialreport_id = reverse_id(financialreport_id)

        media = MediaModel.query.get(financialreport_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaFinancialReportSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('FINANCIAL_REPORT_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'Media laporan keuangan berhasil dihapus dan tidak bisa dikembalikan.',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaFinancialReport, '/media/financialreport/<int:financialreport_id>')


class MediaInventory(Resource):

    """
    @method POST
    @endpoint /v1/media/inventory/<int:inventory_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, inventory_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        inventory_id = reverse_id(inventory_id)
        inventory = InventoryModel.query.get(inventory_id)


        media = MediaModel()
        media.financial_report_id = inventory.id

        uploaded_file_path = os.path.join(current_app.config.get('INVENTORY_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('INVENTORY_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype
        
        if g.current_user.residential_id:
            media.residential_id = g.current_user.residential_id

        media.owner_id = g.owner_id

        db.session.add(media)
        db.session.commit()

        schema = MediaInventorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201



    """
    @method DELETE
    @endpoint /v1/media/inventory/<int:inventory_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, inventory_id):

        inventory_id = reverse_id(inventory_id)

        media = MediaModel.query.get(inventory_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaInventorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('INVENTORY_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'Media inventaris berhasil dihapus dan tidak bisa dikembalikan.',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaInventory, '/media/inventory/<int:inventory_id>')


class MediaProduct(Resource):

    """
    @method POST
    @endpoint /v1/media/product/<int:product_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, product_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        product_id = reverse_id(product_id)
        product = ProductModel.query.get(product_id)

        media = MediaModel()
        media.product_id = product.id

        uploaded_file_path = os.path.join(current_app.config.get('PRODUCT_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('PRODUCT_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype
        media.residential_id = None

        media.owner_id = g.owner_id

        db.session.add(media)
        db.session.commit()

        schema = MediaProductSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


    """
    @method DELETE
    @endpoint /v1/media/product/<int:product_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, product_id):

        product_id = reverse_id(product_id)
        media = MediaModel.query.get(product_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaProductSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('PRODUCT_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaProduct, '/media/product/<int:product_id>')


class MediaInfo(Resource):

    """
    @method POST
    @endpoint /v1/media/info/<int:info_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, info_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        info_id = reverse_id(info_id)
        info = InfoModel.query.get(info_id)

        media = MediaModel()
        media.info_id = info.id

        uploaded_file_path = os.path.join(current_app.config.get('INFO_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('INFO_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype
        media.residential_id = None

        media.owner_id = g.owner_id

        db.session.add(media)
        db.session.commit()

        schema = MediaInfoSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


    """
    @method DELETE
    @endpoint /v1/media/info/<int:info_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, info_id):

        info_id = reverse_id(info_id)
        media = MediaModel.query.get(info_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaInfoSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('INFO_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaInfo, '/media/info/<int:info_id>')


class MediaComment(Resource):

    """
    @method POST
    @endpoint /v1/media/comment/<int:comment_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, comment_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        comment_id = reverse_id(comment_id)
        comment = CommentModel.query.get(comment_id)

        media = MediaModel()
        media.comment_id = comment.id

        uploaded_file_path = os.path.join(current_app.config.get('COMMENT_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('COMMENT_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype
        media.residential_id = None

        media.owner_id = g.owner_id

        db.session.add(media)
        db.session.commit()

        schema = MediaCommentSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


    """
    @method DELETE
    @endpoint /v1/media/comment/<int:comment_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, comment_id):

        comment_id = reverse_id(comment_id)
        media = MediaModel.query.get(comment_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaCommentSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('COMMENT_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaComment, '/media/comment/<int:comment_id>')

class MediaPanic(Resource):

    """
    @method POST
    @endpoint /v1/media/panic/<int:panic_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, panic_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        panic_id = reverse_id(panic_id)
        media = MediaModel.query.filter_by(panic_id=panic_id).first()
        isnew = False

        if media and media.filename:
            os.remove(os.path.join(current_app.config.get('PANIC_FOLDER'), media.filename))
        else:
            isnew = True
            media = MediaModel()

        media.panic_id = panic_id

        uploaded_file_path = os.path.join(current_app.config.get('PANIC_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('PANIC_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype
        media.residential_id = None
        media.owner_id = g.owner_id

        db.session.add(media)
        db.session.commit()

        schema = MediaPanicSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


    """
    @method DELETE
    @endpoint /v1/media/panic/<int:panic_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, panic_id):

        panic_id = reverse_id(panic_id)
        media = MediaModel.query.get(panic_id)

        if media is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaPanicSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('PANIC_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': 'Foto profil berhasil dihapus dan tidak bisa dikembalikan.',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaPanic, '/media/panic/<int:panic_id>')


class MediaFreebies(Resource):

    """
    @method POST
    @endpoint /v1/media/freebies/<int:freebies_id>
    @permission jwt_required, media, editown
    @return media object
    """

    post_args = {
        'media': fields.Field(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('media', 'editown')
    @use_args(post_args, locations=['files'])
    def post(self, payload, freebies_id):

        media_file = payload['media']

        fn = secure_filename(media_file.filename)
        name, extension = os.path.splitext(media_file.filename)
        original_filename = media_file.filename
        filename = str(uuid.uuid4()) + extension
        mimetype = media_file.content_type

        if not allowed_file(filename):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_type': ['Hanya JPG, JPEG dan PNG tipe file yang diperbolehkan.']
                    }
                }
            }), 422

        freebies_id = reverse_id(freebies_id)
        freebies = FreebiesModel.query.get(freebies_id)

        media = MediaModel()
        media.freebies_id = freebies.id

        uploaded_file_path = os.path.join(current_app.config.get('FREEBIES_FOLDER'), filename)

        is_dir_exist(uploaded_file_path)
        media_file.save(uploaded_file_path)

        size = os.path.getsize(uploaded_file_path)

        if size > (8 * 1024 * 1024):
            os.remove(os.path.join(current_app.config.get('FREEBIES_FOLDER'), filename))
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'file_size': ['Ukuran file tidak boleh lebih dari 8MB.']
                    }
                }
            }), 422

        media.filename = filename
        media.original_filename = original_filename
        media.size = size
        media.mimetype = mimetype
        media.residential_id = None

        media.owner_id = g.owner_id

        db.session.add(media)
        db.session.commit()

        schema = MediaFreebiesSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'media': result
            }
        }), 201


    """
    @method DELETE
    @endpoint /v1/media/freebies/<int:freebies_id>
    @permission jwt_required, media, deleteown
    @return media object
    """

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }
    
    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('media', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, freebies_id):

        freebies_id = reverse_id(freebies_id)
        media = MediaModel.query.get(freebies_id)

        if not media:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'media': ['File tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = MediaFreebiesSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(media).data

        try:
            os.remove(os.path.join(current_app.config.get('FREEBIES_FOLDER'), media.filename))

            db.session.delete(media)
            db.session.commit()

            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'media': result
                }
            }), 200

        except Exception as e:
            return jsonify({
                'success': False,
                'code': 422,
                'errors': {
                    'messages': {
                        'media': [str(e)]
                    }
                }
            }), 422


api.add_resource(MediaFreebies, '/media/freebies/<int:freebies_id>')