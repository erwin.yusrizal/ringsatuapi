# -*- coding: utf-8 -*-
import arrow
import bleach
import time
import re
import os

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)

from flask import (
    g,
    current_app,
    jsonify
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    use_kwargs,
    parser
)
from slugify import slugify
from sqlalchemy import extract
from sqlalchemy.sql.expression import func, and_, or_

from api import cache, db, jwt, mail
from api.v1 import api

from api.v1.models.media import(
    MediaModel
)
from api.v1.models.users import (
    UserModel
)
from api.v1.models.settings import (
    CategoryModel
)
from api.v1.models.marketplace import (
    ProductModel,
    FreebiesModel,
    OrderModel,
    OrderDetailModel
)
from api.v1.schemas.marketplace import(
    ProductSchema,
    OrderSchema,
    OrderDetailSchema,
    FreebiesSchema
)
from api.v1.utils import(
    reverse_id,
    is_dir_exist,
    is_file_exist,
    generate_unique_id,
    normalize_phone_number,
    denormalize_phone_number
)
from api.v1.utils.pagination import (
    Pagination
)
from api.v1.decorators import (
    permission_required,
    current_user,
    ownership
)

class Freebies(Resource):
    
    '''
    @method GET
    @endpoint /v1/freebies
    @permission jwt_required
    @return product list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dipublikasikan'], error='Pilihan status tidak valid.')
        ], missing=None),
        'author': fields.Int(missing=None),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('freebies', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Freebies '''

        q = args['q']
        status = args['status']
        author_id = reverse_id(args['author']) if args['author'] else None
        latitude = args['latitude']
        longitude = args['longitude']
        isdeleted = args['archived']

        if not author_id and not status:
            status = 'publish'

        page = args['page']
        perpage = args['perpage']

        total = FreebiesModel.count_all(q, status, author_id, latitude, longitude, isdeleted)
        query = FreebiesModel.get_all(q, status, author_id, latitude, longitude, isdeleted, page, perpage)

        if latitude and longitude:

            freebies = []

            for index, q in enumerate(query):
                schema = FreebiesSchema(only=args['only'], exclude=args['exclude'])
                freebies.append(schema.dump(q[0]).data)

        else:
            schema = FreebiesSchema(only=args['only'], exclude=args['exclude'], many=True)
            freebies = schema.dump(query).data
            
        pagination = Pagination('api.freebies', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'freebies': freebies,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/freebies
    @permission jwt_required
    @return new product object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {     
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang judul barang maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul barang maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'description': fields.Str(required=True),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dipublikasikan'], error='Pilihan status tidak valid.')
        ]),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('freebies', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Freebies '''

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        latitude = payload['latitude']
        longitude = payload['longitude']
        status = payload['status']
        media_ids = [reverse_id(x) for x in payload['medias']] if payload['medias'] else None

        freebies_exist = FreebiesModel.query.filter(FreebiesModel.slug==slug).first()
        if freebies_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'freebies': ['Judul barang sudah terdaftar.']
                    }
                }
            }), 422

        freebies = FreebiesModel()
        freebies.title = title
        freebies.slug = slug
        freebies.description = description
        freebies.latitude = latitude
        freebies.longitude = longitude
        freebies.isdeleted = False
        freebies.status = status
        freebies.user_id = g.current_user.id

        db.session.add(freebies)
        db.session.commit()

        if media_ids:
            for media_id in media_ids:
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
                    media.freebies_id = freebies.id
                    media.residential_id = None

                    media.owner_id = None

                    media.isorphan = False
                    media.expired = None

                    file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                    new_path = os.path.join(current_app.config.get('FREEBIES_FOLDER'), media.filename)

                    is_dir_exist(new_path)

                    if is_file_exist(file_path):
                        os.rename(file_path, new_path)

                    db.session.commit()     

        schema = FreebiesSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(freebies).data

        return jsonify({
            'success': True,
            'message': 'Barang berhasil ditambahkan.',
            'data': {
                'freebie': result
            }
        }), 201


api.add_resource(Freebies, '/freebies')


class Freebie(Resource):
    
    '''
    @method GET
    @endpoint /v1/freebies/<int:freebies_id>
    @permission jwt_required
    @return freebies object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('freebies', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, freebies_id):

        ''' View Freebies '''

        freebies_id = reverse_id(freebies_id)
        freebies = FreebiesModel.query.get(freebies_id)

        if not freebies:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'freebies': ['Barang tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = FreebiesSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(freebies).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'freebie': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/freebiess/<int:freebies_id>
    @permission jwt_required
    @return updated freebies object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang judul barang maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul barang maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'description': fields.Str(required=True),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dipublikasikan'], error='Pilihan status tidak valid.')
        ]),
        'medias': fields.List(fields.Int(), missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('freebies', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, freebies_id):

        ''' Edit Freebies '''

        freebies_id = reverse_id(freebies_id)
        freebies = FreebiesModel.get_by(freebies_id, g.current_user.id)

        if not freebies:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'freebies': ['Barang tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        latitude = payload['latitude']
        longitude = payload['longitude']
        status = payload['status']
        media_ids = [reverse_id(x) for x in payload['medias']] if payload['medias'] else None

        freebies_exist = FreebiesModel.query.filter(FreebiesModel.slug==slug, FreebiesModel.id!=freebies.id).first()
        if freebies_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'freebies': ['Judul barang sudah terdaftar.']
                    }
                }
            }), 422

        freebies.title = title
        freebies.slug = slug
        freebies.description = description
        freebies.latitude = latitude
        freebies.longitude = longitude
        freebies.isdeleted = False
        freebies.status = status
        freebies.user_id = g.current_user.id

        db.session.commit()

        schema = FreebiesSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(product).data

        return jsonify({
            'success': True,
            'message': 'Barang berhasil diubah.',
            'data': {
                'freebie': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/freebiess/<int:freebies_id>
    @permission jwt_required
    @return deleted freebies object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('freebies', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, freebies_id):

        ''' Delete Freebies '''  

        freebies_id = reverse_id(freebies_id)
        freebies = FreebiesModel.query.filter_by(id=freebies_id, user_id=g.current_user.id).first()

        if not freebies:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'freebies': ['Barang tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = FreebiesSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(freebies).data

        if freebies.isdeleted:
            if freebies.medias:
                for media in freebies.medias:
                    os.remove(os.path.join(current_app.config.get('FREEBIES_FOLDER'), media.filename))
            db.session.delete(freebies)
        else:
            freebies.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Barang berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'freebie': result
            }
        }), 200


api.add_resource(Freebie, '/freebies/<int:freebies_id>')


class Products(Resource):
    
    '''
    @method GET
    @endpoint /v1/products
    @permission jwt_required
    @return product list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(validate=[
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dijual'], error='Pilihan status tidak valid.')
        ], missing=None),
        'category': fields.Int(missing=None),
        'author': fields.Int(missing=None),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('product', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Products '''

        q = args['q']
        status = args['status']
        category_id = reverse_id(args['category']) if args['category'] else None
        author_id = reverse_id(args['author']) if args['author'] else None
        latitude = args['latitude']
        longitude = args['longitude']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = ProductModel.count_all(q, status, category_id, author_id, latitude, longitude, isdeleted)
        query = ProductModel.get_all(q, status, category_id, author_id, latitude, longitude, isdeleted, page, perpage)

        if latitude and longitude:

            products = []

            for index, q in enumerate(query):
                schema = ProductSchema(only=args['only'], exclude=args['exclude'])
                products.append(schema.dump(q[0]).data)

        else:
            schema = ProductSchema(only=args['only'], exclude=args['exclude'], many=True)
            products = schema.dump(query).data
            
        pagination = Pagination('api.products', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'products': products,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/products
    @permission jwt_required
    @return new product object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {     
        'condition': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['new', 'used'], labels=['Baru', 'Bekas'], error='Pilihan kondisi barang/jasa tidak valid.')
        ]),   
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang judul barang/jasa maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul barang/jasa maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi barang/jasa maksimal 255 karakter.')
        ]),
        'minorder': fields.Int(required=True),
        'price': fields.Int(required=True),
        'discount': fields.Int(missing=0),
        'isnegotiable': fields.Bool(missing=False),
        'isdeliverable': fields.Bool(missing=False),
        'delivery_cost': fields.Int(missing=None),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dijual'], error='Pilihan status tidak valid.')
        ]),
        'category': fields.Int(required=True),
        'unit': fields.Int(required=True),
        'medias': fields.List(fields.Int(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('product', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Product '''

        condition = payload['condition']
        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        minorder = payload['minorder']
        price = payload['price']
        discount = payload['discount']
        isnegotiable = payload['isnegotiable']
        isdeliverable = payload['isdeliverable']
        delivery_cost = payload['delivery_cost']
        latitude = payload['latitude']
        longitude = payload['longitude']
        category_id = reverse_id(payload['category'])
        unit_id = reverse_id(payload['unit'])
        status = payload['status']
        media_ids = [reverse_id(x) for x in payload['medias']] if payload['medias'] else None

        category = CategoryModel.query.get(category_id)

        if not category:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'category': ['Nama kategori tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        product_exist = ProductModel.query.filter(ProductModel.slug==slug).first()
        if product_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'product': ['Produk sudah terdaftar.']
                    }
                }
            }), 422

        product = ProductModel()
        product.conditions = condition
        product.title = title
        product.slug = slug
        product.description = description
        product.minorder = minorder
        product.price = price
        product.discount = discount
        product.isnegotiable = isnegotiable
        product.isdeliverable = isdeliverable
        product.delivery_cost = delivery_cost
        product.latitude = latitude
        product.longitude = longitude
        product.isdeleted = False
        product.status = status
        product.user_id = g.current_user.id
        product.category_id = category_id
        product.unit_id = unit_id

        db.session.add(product)
        db.session.commit()

        if media_ids:
            for media_id in media_ids:
                media = MediaModel.query.get(media_id)

                if media and media.isorphan:
                    media.product_id = product.id
                    media.residential_id = None

                    media.owner_id = None

                    media.isorphan = False
                    media.expired = None

                    file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                    new_path = os.path.join(current_app.config.get('PRODUCT_FOLDER'), media.filename)

                    is_dir_exist(new_path)

                    if is_file_exist(file_path):
                        os.rename(file_path, new_path)

                    db.session.commit()     

        schema = ProductSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(product).data

        return jsonify({
            'success': True,
            'message': 'Produk berhasil ditambahkan.',
            'data': {
                'product': result
            }
        }), 201


api.add_resource(Products, '/products')


class Product(Resource):
    
    '''
    @method GET
    @endpoint /v1/products/<int:product_id>
    @permission jwt_required
    @return product object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('product', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, product_id):

        ''' View Product '''

        product_id = reverse_id(product_id)
        product = ProductModel.query.get(product_id)

        if not product:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'product': ['Produk tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = ProductSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(product).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'product': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/products/<int:product_id>
    @permission jwt_required
    @return updated product object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'condition': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['new', 'used'], labels=['Baru', 'Bekas'], error='Pilihan kondisi barang/jasa tidak valid.')
        ]),  
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang judul produk maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang judul produk maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi produk maksimal 255 karakter.')
        ]),
        'minorder': fields.Int(required=True),
        'price': fields.Int(required=True),
        'discount': fields.Int(missing=0),
        'isnegotiable': fields.Bool(missing=False),
        'isdeliverable': fields.Bool(missing=False),
        'delivery_cost': fields.Int(missing=None),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['draft', 'publish'], labels=['Disimpan', 'Dijual'], error='Pilihan status tidak valid.')
        ]),
        'category': fields.Int(required=True),
        'unit': fields.Int(required=True),
        'medias': fields.List(fields.Int(), missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('product', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, product_id):

        ''' Edit Category '''

        product_id = reverse_id(product_id)
        product = ProductModel.get_by(product_id, g.current_user.id)

        if not product:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'product': ['Kategori tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        condition = payload['condition']
        title = bleach.clean(payload['title'], strip=True)
        slug = slugify(title) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        minorder = payload['minorder']
        price = payload['price']
        discount = payload['discount']
        isnegotiable = payload['isnegotiable']
        isdeliverable = payload['isdeliverable']
        delivery_cost = payload['delivery_cost']
        latitude = payload['latitude']
        longitude = payload['longitude']
        category_id = reverse_id(payload['category'])
        unit_id = reverse_id(payload['unit'])
        status = payload['status']
        media_ids = [reverse_id(x) for x in payload['medias']] if payload['medias'] else None

        category = CategoryModel.query.get(category_id)

        if not category:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'category': ['Nama kategori tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        product_exist = ProductModel.query.filter(ProductModel.slug==slug, ProductModel.id!=product.id).first()
        if product_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'product': ['Produk sudah terdaftar.']
                    }
                }
            }), 422

        product.conditions = condition
        product.title = title
        product.slug = slug
        product.description = description
        product.minorder = minorder
        product.price = price
        product.discount = discount
        product.isnegotiable = isnegotiable
        product.isdeliverable = isdeliverable
        product.delivery_cost = delivery_cost
        product.latitude = latitude
        product.longitude = longitude
        product.isdeleted = False
        product.status = status
        product.user_id = g.current_user.id
        product.category_id = category_id
        product.unit_id = unit_id

        db.session.commit()

        schema = ProductSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(product).data

        return jsonify({
            'success': True,
            'message': 'Produk berhasil diubah.',
            'data': {
                'product': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/products/<int:product_id>
    @permission jwt_required
    @return deleted product object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('product', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, product_id):

        ''' Delete Product '''  

        product_id = reverse_id(product_id)
        product = ProductModel.query.filter_by(id=product_id, user_id=g.current_user.id).first()

        if not product:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'product': ['Produk tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = ProductSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(product).data

        if product.isdeleted:
            if product.medias:
                for media in product.medias:
                    os.remove(os.path.join(current_app.config.get('PRODUCT_FOLDER'), media.filename))
            db.session.delete(product)
        else:
            product.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Produk berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'product': result
            }
        }), 200


api.add_resource(Product, '/products/<int:product_id>')


class Orders(Resource):
    
    '''
    @method GET
    @endpoint /v1/orders
    @permission jwt_required
    @return order list
    '''  

    get_args = {
        'po': fields.Str(missing=None),
        'buyer': fields.Int(missing=None),
        'seller': fields.Int(missing=None),
        'status': fields.Int(validate=validate.OneOf(choices=['expired', 'waiting', 'approved', 'rejected', 'ondelivery', 'delivered'], error='Pilihan status tidak valid.'), missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('order', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Orders '''

        q = args['q']
        buyer_id = reverse_id(args['buyer']) if args['buyer'] else None
        seller_id = reverse_id(args['seller']) if args['seller'] else None
        status = args['status']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = OrderModel.count_all(q, status, buyer, seller, isdeleted)
        query = OrderModel.get_all(q, status, buyer, seller, isdeleted, page, perpage)

        schema = OrderSchema(only=args['only'], exclude=args['exclude'], many=True)
        orders = schema.dump(query).data
        pagination = Pagination('api.orders', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'orders': orders,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/orders
    @permission jwt_required
    @return new order object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = { 
        'seller': fields.Int(required=True),
        'buyer': fields.Int(required=True),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['expired', 'approved', 'rejected', 'ondelivery', 'delivered'], labels=['Kadaluwarsa', 'Disetujui', 'Ditolak', 'Sedang Diantar', 'Sudah Sampai'], error='Pilihan status tidak valid.')
        ]),
        'carts': fields.Dict(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    
    @permission_required('product', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Order '''

        seller_id = reverse_id(payload['seller'])
        buyer_id = reverse_id(payload['buyer'])
        status = payload['status']
        carts = payload['carts']

        seller = UserModel.query.get(seller_id)
        if not seller:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'seller': ['Penjual tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        buyer = UserModel.query.get(buyer_id)
        if not buyer:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'buyer': ['Pembeli tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        now = arrow.get().format('YYYY/MM/DD')
        po = 'ORDER/' + now + '/' + generate_unique_id(5)

        tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
        expired = time.mktime(nextminutes.timetuple())

        order = OrderModel()
        order.po = po
        order.expired = expired
        order.status = status
        order.seller_id = seller_id
        order.buyer_id = buyer_id

        for cart in carts:
            item = OrderDetailModel()
            item.quantity = cart['quantity']
            item.price = cart['price']
            item.discount = cart['discount']
            item.cost = cart['cost']
            item.total = cart['total']
            item.note = cart['note']
            item.product_id = cart['product_id']

            order.details.append(item)

        db.session.add(order)
        db.session.commit()

        schema = OrderModel(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(order).data

        return jsonify({
            'success': True,
            'message': 'Pesanan berhasil ditambahkan.',
            'data': {
                'order': result
            }
        }), 201


api.add_resource(Orders, '/orders')


class Order(Resource):
    
    '''
    @method GET
    @endpoint /v1/orders/<int:order_id>
    @permission jwt_required
    @return order object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('order', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, order_id):

        ''' View Order '''

        order_id = reverse_id(order_id)
        order = OrderModel.query.get(order_id)

        if not order:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'order': ['Pesanan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = OrderSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(order).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'order': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/orders/<int:order_id>
    @permission jwt_required
    @return updated order object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'seller': fields.Int(required=True),
        'buyer': fields.Int(required=True),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['expired', 'approved', 'rejected', 'ondelivery', 'delivered'], labels=['Kadaluwarsa', 'Disetujui', 'Ditolak', 'Sedang Diantar', 'Sudah Sampai'], error='Pilihan status tidak valid.')
        ]),
        'carts': fields.Dict(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('order', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, order_id):

        ''' Edit Order '''

        order_id = reverse_id(order_id)
        order = OrderModel.get_by(order_id, g.current_user)

        if not order:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'order': ['Pesanan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        seller_id = reverse_id(payload['seller'])
        buyer_id = reverse_id(payload['buyer'])
        status = payload['status']
        carts = payload['carts']

        seller = UserModel.query.get(seller_id)
        if not seller:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'seller': ['Penjual tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        buyer = UserModel.query.get(buyer_id)
        if not buyer:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'buyer': ['Pembeli tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        order.expired = expired
        order.status = status
        order.seller_id = seller_id
        order.buyer_id = buyer_id

        for cart in carts:
            item = OrderDetailModel()
            item.quantity = cart['quantity']
            item.price = cart['price']
            item.discount = cart['discount']
            item.cost = cart['cost']
            item.total = cart['total']
            item.note = cart['note']
            item.product_id = cart['product_id']

            order.details.append(item)

        db.session.commit()

        schema = OrderSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(order).data

        return jsonify({
            'success': True,
            'message': 'Pesanan berhasil diubah.',
            'data': {
                'order': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/orders/<int:order_id>
    @permission jwt_required
    @return deleted order object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('order', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, order_id):

        ''' Delete Order '''  

        order_id = reverse_id(order_id)
        order = OrderModel.get_by(order_id, g.current_user.id)

        if not order:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'order': ['Pesanan tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = OrderSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(order).data

        if order.isdeleted:
            db.session.delete(order)
        else:
            order.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Pesanan berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'order': result
            }
        }), 200


api.add_resource(Order, '/orders/<int:order_id>')