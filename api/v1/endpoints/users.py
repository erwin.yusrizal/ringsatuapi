# -*- coding: utf-8 -*-
import arrow
import bleach
import redis
import time
import re
import os

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)

from flask import (
    g,
    current_app,
    jsonify,
    request,
    url_for
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    use_kwargs,
    parser
)
from slugify import slugify
from sqlalchemy.sql.expression import func, and_, or_

from api import cache, db, jwt, mail
from api.v1 import api

from api.v1.models.media import (
    MediaModel
)

from api.v1.models.users import (
    UserModel,
    RoleModel,
    PermissionModel,
    ProfileModel,
    BankAccountModel,
    UserNotificationModel
)
from api.v1.schemas.users import(
    UserSchema,
    PermissionSchema,
    RoleSchema,
    ProfileSchema,
    BankAccountSchema,
    UserNotificationSchema
)
from api.v1.models.residentials import (
    ResidentialModel
)
from api.v1.models.settings import (
    BankModel
)
from api.v1.utils import(
    calculate_age,
    reverse_id,
    normalize_phone_number,
    denormalize_phone_number,
    is_dir_exist,
    is_file_exist
)
from api.v1.utils.pagination import (
    Pagination
)
from api.v1.decorators import (
    permission_required,
    current_user,
    ownership
)

class Me(Resource):

    '''
    @method GET
    @endpoint /v1/me
    @permission jwt_required
    @return user object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' View Me '''

        user = UserModel.query.get(g.current_user.id)

        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'me': result
            }
        }), 200

    '''
    @method PUT
    @endpoint /v1/profile
    @permission jwt_required
    @return user object
    ''' 

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'gender': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['m', 'f'], labels=['Laki-Laki', 'Perempuan'], error='Pilihan jenis kelamin tidak valid.')
        ]),
        'birthday': fields.Date(required=True),
        'fullname': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9\s]{1,255})$', error='Panjang nama lengkap maksimal 255 karakter. Hanya huruf, angka, dan spasi yang diperbolehkan.')
        ]),
        'invitable': fields.Bool(missing=False),
        'phone': fields.Str(required=True, validate=[validate_required]),
        'address': fields.Str(required=True),
        'region': fields.Int(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload):

        ''' Update Me '''

        fullname = bleach.clean(payload['fullname'], strip=True)
        phone = normalize_phone_number(payload['phone'])
        birthday = payload['birthday']
        gender = payload['gender']
        address = bleach.clean(payload['address'], strip=True) if payload['address'] else None
        region = reverse_id(payload['region']) if payload['region'] else None
        invitable = payload['invitable']

        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', phone)

        if phone_match is None:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'phone': ['No. handphone tidak valid. Harus diawali dengan 08, +628 atau 628']
                    }
                }
            }), 422

        if birthday:
            bday = arrow.get(birthday).datetime
            age = calculate_age(bday)

            if age < 10:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'birthday': ['Pengguna harus berusia minimal 10 tahun.']
                        }
                    }
                }), 422

        user = UserModel.query.get(g.current_user.id)

        user.profile.fullname = fullname

        if phone != user.phone:
            user.phone = phone
            user.profile.phone = phone

        user.profile.birthday = birthday
        user.profile.gender = gender
        user.profile.address = address
        user.profile.profile_region_id = region
        user.profile.invitable = invitable

        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'me': result
            }
        }), 200

api.add_resource(Me, '/me')


class Permissions(Resource):
    
    '''
    @method GET
    @endpoint /v1/permissions
    @permission jwt_required
    @return permissions list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Permissions '''

        q = args['q']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = PermissionModel.count_all(q, isdeleted)
        query = PermissionModel.get_all(q, isdeleted, page, perpage)

        schema = PermissionSchema(only=args['only'], exclude=args['exclude'], many=True)
        permissions = schema.dump(query).data
        pagination = Pagination('api.permissions', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'permissions': permissions,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/permissions
    @permission jwt_required
    @return new permission object
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama hak akses maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama hak akses maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi hak akses maksimal 255 karakter.')
        ]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create Permission '''

        name = payload['name']
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        permission = PermissionModel.query.filter_by(slug=slug).first()

        if permission:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'permission': ['Nama hak akses sudah terdaftar.']
                    }
                }
            }), 422

        permission = PermissionModel()
        permission.name = name
        permission.slug = slug
        permission.description = description
        permission.isdeleted = False

        db.session.add(permission)
        db.session.commit()

        schema = PermissionSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(permission).data

        return jsonify({
            'success': True,
            'message': 'Hak akses baru berhasil ditambahkan.',
            'data': {
                'permission': result
            }
        }), 201


api.add_resource(Permissions, '/permissions')

class Permission(Resource):
    
    '''
    @method GET
    @endpoint /v1/permissions/<int:permission_id>
    @permission jwt_required
    @return permission object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'viewother')
    @use_args(get_args, locations=['query'])
    def get(self, args, permission_id):

        ''' View Permission '''

        permission_id = reverse_id(permission_id)
        permission = PermissionModel.get_by(permission_id)

        if not permission:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'permission': ['Hak akses tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = PermissionSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(permission).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'permission': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/permissions/<int:permission_id>
    @permission jwt_required
    @return updated permission object
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama hak akses maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama hak akses maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Deskripsi hak akses maksimal 255 karakter')
        ]),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'editother')
    @use_args(put_args, locations=['json'])
    def put(self, payload, permission_id):

        ''' Edit Permission '''

        permission_id = reverse_id(permission_id)
        permission = PermissionModel.get_by(permission_id)

        if not permission:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'permission': ['Hak akses tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        name = payload['name']
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        isdeleted = payload['archived']

        existing_permission = PermissionModel.query.filter(PermissionModel.slug==slug, PermissionModel.id!=permission_id).first()

        if existing_permission:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'permission': ['Nama hak akses sudah terdaftar.']
                    }
                }
            }), 422

        permission.name = name
        permission.slug = slug
        permission.isdeleted = isdeleted

        if description:
            permission.description = description

        db.session.commit()

        schema = PermissionSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(permission).data

        return jsonify({
            'success': True,
            'message': 'Hak akses berhasil diubah.',
            'data': {
                'permission': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/permissions/<int:permission_id>
    @permission jwt_required
    @return deleted permission object
    ''' 

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('permission', 'editother')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, permission_id):

        ''' Delete Permission '''

        permission_id = reverse_id(permission_id)
        permission = PermissionModel.get_by(permission_id)

        if not permission:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'permission': ['Hak akses tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = PermissionSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(permission).data

        if permission.isdeleted:
            db.session.delete(permission)            
        else:
            permission.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Hak akses berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'permission': result
            }
        }), 200

api.add_resource(Permission, '/permissions/<int:permission_id>')


class Roles(Resource):
    
    '''
    @method GET
    @endpoint /v1/roles
    @permission jwt_required
    @return roles list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('role', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Roles '''

        q = args['q']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']
        offset = (page - 1) * perpage

        query = RoleModel.get_all(q, g.owner_id, isdeleted, page, perpage)
        total = RoleModel.count_all(q, g.owner_id, isdeleted)

        schema = RoleSchema(only=args['only'], exclude=args['exclude'], many=True)
        roles = schema.dump(query).data
        pagination = Pagination('api.roles', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'roles': roles,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/roles
    @permission jwt_required
    @return roles list
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama role maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9\-]{1,255})$', error='Panjang nama role maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi role maksimal 255 karakter.')
        ]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('role', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create Role '''

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        role = RoleModel.query.filter(RoleModel.slug==slug)

        if g.owner_id:
            role = role.filter(RoleModel.owner_id==g.owner_id)

        if role.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'role': ['Nama role sudah terdaftar.']
                    }
                }
            }), 422

        role = RoleModel()
        role.name = name
        role.slug = slug
        role.description = description
        role.isdeleted = False
        role.owner_id = g.owner_id

        query = PermissionModel.query.all()
        permissions = {}

        for q in query:
            permissions[q.slug] = {
                'menu': 0, 
                'create': 0,
                'viewall': 0,
                'viewown': 0,
                'viewother': 0,
                'editown': 0,
                'editother': 0,
                'deleteown': 0,
                'deleteother': 0
            } 

        role.permissions = permissions

        db.session.add(role)
        db.session.commit()

        schema = RoleSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(role).data

        return jsonify({
            'success': True,
            'message': 'Role baru berhasil ditambahkan.',
            'data': {
                'role': result
            }
        }), 201


api.add_resource(Roles, '/roles')


class Role(Resource):
    
    '''
    @method GET
    @endpoint /v1/roles/<int:role_id>
    @permission jwt_required
    @return role object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('role', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, role_id):

        ''' View Role '''

        role_id = reverse_id(role_id)
        role = RoleModel.get_by(role_id, g.owner_id)

        if not role:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'role': ['Role tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = RoleSchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(role).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'role': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/roles/<int:role_id>
    @permission jwt_required
    @return updated role object
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama role maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9\-]{1,255})$', error='Panjang nama role maksimal 255 karakter. Hanya huruf, angka, tanda minus dan spasi yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi maksimal 255 karakter.')
        ]),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('role', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, role_id):

        ''' Edit Role '''        

        role_id = reverse_id(role_id)
        role = RoleModel.get_by(role_id, g.owner_id)

        if not role:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'role': ['Role tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        if role.slug in ['orphan', 'root', 'owner', 'leader', 'vice', 'security', 'finance', 'secretary', 'resident', 'public']:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'role': ['Role default tidak bisa ubah.']
                    }
                }
            }), 422

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        isdeleted = payload['archived']

        existing_role = RoleModel.query.filter(RoleModel.slug==slug, RoleModel.id!=role_id)

        if g.owner_id:
            existing_role = existing_role.filter(RoleModel.owner_id==g.owner_id)

        if existing_role.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'role': ['Nama role sudah terdaftar.']
                    }
                }
            }), 422

        role.name = name
        role.slug = slug
        role.isdeleted = isdeleted

        if description:
            role.description = description

        db.session.commit()

        schema = RoleSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(role).data

        return jsonify({
            'success': True,
            'message': 'Role berhasil diubah.',
            'data': {
                'role': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/roles/<int:role_id>
    @permission jwt_required
    @return deleted role object
    ''' 

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('role', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, role_id):

        ''' Delete Roles '''
        
        role_id = reverse_id(role_id)
        role = RoleModel.get_by(role_id, g.owner_id)

        if not role:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'role': ['Role tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404        

        schema = RoleSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(role).data

        if role.isdeleted:
            if role.users.all():
                UserModel.query.filter_by(role_id=role.id).update({'role_id': 1})
            db.session.delete(role)

        else:
            role.isdeleted=True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Role berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'role': result
            }
        }), 200

api.add_resource(Role, '/roles/<int:role_id>')


class RolePermissions(Resource):
    
    '''
    @method GET
    @endpoint /v1/roles/<int:role_id>/permissions
    @permission jwt_required
    @return role object
    '''  

    put_args = {
        'permissions': fields.Dict(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('role', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, role_id):

        ''' Edit Role Permissions '''

        role_id = reverse_id(role_id)
        permissions = payload['permissions']
        role = RoleModel.get_by(role_id, g.owner_id)

        if not role:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'role': ['Role tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        default_permissions = PermissionModel.query.all()
        default_permissions_list = []
        permission_keys = ['create', 'viewall', 'viewown', 'viewother', 'editown', 'editother', 'deleteown', 'deleteother']
        role_permissions = {}

        for dp in default_permissions:
            default_permissions_list.append(dp.slug)

        for key, value in permissions.iteritems():
            if key in default_permissions_list and any(pk in value for pk in permission_keys):
                role_permissions[key] = permissions[key]

        if role_permissions:
            role.permissions = role_permissions
            db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Hak akses role berhasil diubah.',
            'data': {
                'permissions': role.permissions
            }
        }), 200        


api.add_resource(RolePermissions, '/roles/<int:role_id>/permissions')


class Users(Resource):
    
    '''
    @method GET
    @endpoint /v1/users
    @permission jwt_required
    @return users list
    '''
    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    get_args = {
        'q': fields.Str(missing=None),
        'status': fields.Str(validate=[
            validate_required,
            validate.OneOf(choices=['inactive', 'active', 'banned'], labels=['Belum Aktif', 'Aktif', 'Ditangguhkan'], error='Pilihan status tidak valid.')
        ], missing=None),
        'parent': fields.Int(missing=None),
        'role': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'online': fields.Bool(missing=False),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Users '''

        owner = 'root'

        if g.current_user.role.slug != 'root':
            owner = g.current_user.parent_id if g.current_user.parent_id else g.current_user.id

        q = args['q']
        status = args['status']
        parent_id = reverse_id(args['parent']) if args['parent'] else None
        role_id = reverse_id(args['role']) if args['role'] else None
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        isonline = args['online']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        if g.current_user.role.slug not in ['root', 'owner'] and not residential_id:
            residential_id = g.current_user.residential.id

        total = UserModel.count_all(owner, q, status, residential_id, role_id, parent_id, isonline, isdeleted)
        query = UserModel.get_all(owner, q, status, residential_id, role_id, parent_id, isonline, isdeleted, page, perpage)

        schema = UserSchema(only=args['only'], exclude=args['exclude'], many=True)
        users = schema.dump(query).data
        pagination = Pagination('api.users', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'users': users,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/users
    @permission jwt_required
    @return success message
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Silakan isi semua kolom yang wajib diisi.')   

    def validate_phone(value):
        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', value)

        if phone_match is None:
            raise ValidationError('No. handphone tidak valid. Harus diawali dengan 08, +628 atau 628')

    def validate_age(value):
        birthday = arrow.get(value).datetime
        age = calculate_age(birthday)

        if age < 10:
            raise ValidationError('Pengguna harus berusia minimal 10 tahun.')

    post_args = {
        'gender': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['m', 'f'], labels=['Laki-Laki', 'Perempuan'], error='Pilihan jenis kelamin tidak valid.')
        ]),
        'birthday': fields.Date(required=True, validate=[validate_age]),
        'relationship_status': fields.Str(validate=[
            validate_required,
            validate.OneOf(choices=['husband', 'wife', 'children', 'parents', 'sibling', 'cousin', 'nephew', 'assistant'], labels=['Suami', 'Istri', 'Anak', 'Orang Tua', 'Saudara Kandung', 'Sepupu', 'Keponakan', 'Asisten Rumah Tangga'], error='Pilihan status di keluarga tidak valid.')
        ], missing=None),
        'fullname': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama lengkap maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'phone': fields.Str(required=True, validate=[
            validate_required,validate_phone
        ]),        
        'idcard': fields.Str(validate=[
            validate_required,
            validate.OneOf(choices=['ktp', 'sim', 'passport', 'badge'], labels=['KTP', 'SIM', 'Paspor', 'Badge'], error='Pilihan jenis identitas tidak valid.')
        ], missing=None),
        'idcard_number': fields.Str(missing=None),
        'address': fields.Str(missing=None),
        'profile_region_id': fields.Int(missing=None),
        'residential_status': fields.Str(validate=[
            validate.OneOf(choices=['rent', 'owner', 'staffing'], labels=['Sewa', 'Pemilik', 'Rumah Dinas'], error='Pilihan status kepemilikan rumah tidak valid.')
        ], missing=None),
        'landlord_name': fields.Str(missing=None),
        'landlord_address': fields.Str(missing=None),
        'landlord_phone': fields.Str(missing=None),
        'landlord_region_id': fields.Int(missing=None),
        'store_name': fields.Str(missing=None),
        'store_about': fields.Str(missing=None),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['inactive', 'active', 'banned'], labels=['Tidak Aktif', 'Aktif', 'Ditangguhkan'], error='Pilihan status pengguna tidak valid.')
        ]),
        'balance': fields.Int(missing=0),
        'point': fields.Int(missing=0),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'role': fields.Int(required=True),
        'parent': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'media': fields.Int(missing=None),
        'bank_accounts': fields.List(fields.Dict(), missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Add User '''

        gender = payload['gender']
        birthday = payload['birthday']
        relationship_status = payload['relationship_status'] if payload['relationship_status'] else None
        fullname = bleach.clean(payload['fullname'], strip=True)
        phone = normalize_phone_number(payload['phone'])
        idcard = payload['idcard'] if payload['idcard'] else None
        idcard_number = bleach.clean(payload['idcard_number'], strip=True) if payload['idcard_number'] else None
        address = bleach.clean(payload['address'], strip=True) if payload['address'] else None
        profile_region_id = reverse_id(payload['profile_region_id']) if payload['profile_region_id'] else None
        landlord_name = bleach.clean(payload['landlord_name'], strip=True) if payload['landlord_name'] else None
        landlord_address = bleach.clean(payload['landlord_address'], strip=True) if payload['landlord_address'] else None
        landlord_phone = normalize_phone_number(payload['landlord_phone']) if payload['landlord_phone'] else None
        landlord_region_id = reverse_id(payload['landlord_region_id']) if payload['landlord_region_id'] else None
        store_name = bleach.clean(payload['store_name'], strip=True) if payload['store_name'] else None
        store_about = bleach.clean(payload['store_about'], strip=True) if payload['store_about'] else None
        residential_status = payload['residential_status'] if payload['residential_status'] else None
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None
        status = payload['status']
        balance = payload['balance']
        point = payload['point']
        latitude = payload['latitude']
        longitude = payload['longitude']
        role_id = reverse_id(payload['role'])
        parent_id = reverse_id(payload['parent']) if payload['parent'] else None
        media_id = reverse_id(payload['media']) if payload['media'] else None
        bank_accounts = payload['bank_accounts']

        if residential_status == 'rent' and (not landlord_name or not landlord_address or not landlord_phone or not landlord_region_id):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'landlord_name': ['Data pemilik rumah wjaib diisi.']
                    }
                }
            }), 422

        role = RoleModel.get_by(role_id)
        if role.slug in ['root', 'owner'] and g.current_user.role.slug != 'root':
            return jsonify({
                'success': False,
                'errors': {
                    'code': 403,
                    'messages': {
                        'forbidden': ['Anda tidak memiliki izin untuk melakukan permintaan ini.']
                    }
                }
            }), 403

        if role.slug not in ['root', 'owner'] and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Warga perumahan wajid diisi.']
                    }
                }
            }), 422
            
        if residential_id:
            residential = ResidentialModel.query.get(residential_id)
            if not residential:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 404,
                        'messages': {
                            'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                        }
                    }
                }), 404

        user_exist = UserModel.query.join(ProfileModel).filter(UserModel.phone==phone)

        if residential_id:
            user_exist = user_exist.filter(ResidentialModel.id==residential_id)

        if parent_id:
            user_exist = user_exist.filter(UserModel.parent_id==parent_id)

        if user_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'user': ['Pengguna sudah terdaftar.']
                    }
                }
            }), 422

        user = UserModel()
        user.phone = phone
        user.role_id = role.id
        user.status = status
        user.relationship_status = relationship_status        
        user.isdeleted = False
        user.isonline = False

        if parent_id:
            user.parent_id = parent_id
        elif role.slug in ['root', 'owner']:
            user.parent_id = None
        else:
            user.parent_id = residential.owner_id

        db.session.add(user)
        db.session.flush()

        profile = ProfileModel()
        profile.gender = gender
        profile.birthday = birthday
        profile.fullname = fullname
        profile.phone = phone
        profile.idcard = idcard
        profile.idcard_number = idcard_number
        profile.address = address
        profile.profile_region_id = profile_region_id
        profile.residential_status = residential_status
        profile.landlord_name = landlord_name
        profile.landlord_address = landlord_address
        profile.landlord_phone = landlord_phone
        profile.landlord_region_id = landlord_region_id
        profile.store_name = store_name
        profile.store_about = store_about
        profile.balance = balance
        profile.point = point
        profile.latitude = latitude
        profile.longitude = longitude
        profile.invitable = True

        profile.user_id = user.id

        db.session.add(profile)   

        if role.slug not in ['root', 'owner'] and residential:
            residential.users.append(user)

        if bank_accounts:
            for account in bank_accounts:
                if account['bank_id'] and account['account_name'] and account['account_number']:
                    bank_account = BankAccountModel()
                    bank_account.account_name = account['account_name']
                    bank_account.account_number = account['account_number']
                    bank_account.branch = account['branch']
                    bank_account.description = account['description']
                    bank_account.bank_id = reverse_id(account['bank_id'])
                    bank_account.holder_id = user.id

                    owner_id = None
                    if user.role.slug != 'root':
                        owner_id =  user.parent_id if user.parent_id else user.id

                    bank_account.owner_id = owner_id
                    bank_account.residential_id = user.residential_id

                    db.session.add(bank_account)


        db.session.commit()

        if media_id:
            media = MediaModel.query.get(media_id)

            if media and media.isorphan:
                media.profile_id = profile.id
                media.residential_id = user.residential_id
                
                owner_id = None
                if user.role.slug != 'root':
                    owner_id = user.parent_id if user.parent_id else user.id

                media.owner_id = owner_id

                media.isorphan = False
                media.expired = None

                file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                new_path = os.path.join(current_app.config.get('PROFILE_FOLDER'), media.filename)

                is_dir_exist(new_path)

                if is_file_exist(file_path):
                    os.rename(file_path, new_path)

                db.session.commit()            

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': 'Pengguna baru berhasil didaftarkan.',
            'data': {
                'user': result
            }
        }), 201


api.add_resource(Users, '/users')


class User(Resource):

    '''
    @method GET
    @endpoint /v1/users/<int:user_id>
    @permission jwt_required
    @return user object
    ''' 

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, user_id):

        ''' View User '''

        owner_id = None

        if g.current_user.role.slug != 'root':
            owner_id = g.current_user.parent_id if g.current_user.parent_id else g.current_user.id

        user_id = reverse_id(user_id)
        query = UserModel.get_by(user_id, owner_id)

        if not query:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404


        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        user = schema.dump(query).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': user
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/users/<int:user_id>
    @permission jwt_required
    @return updated user object
    '''

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Silakan isi semua kolom yang wajib diisi.')   

    def validate_phone(value):
        phone_match = re.search(r'\(?(?:\+628|628|08)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}', value)

        if phone_match is None:
            raise ValidationError('No. handphone tidak valid. Harus diawali dengan 08, +628 atau 628')

    def validate_age(value):
        birthday = arrow.get(value).datetime
        age = calculate_age(birthday)

        if age < 10:
            raise ValidationError('Pengguna harus berusia minimal 10 tahun.')

    put_args = {
        'gender': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['m', 'f'], labels=['Laki-Laki', 'Perempuan'], error='Pilihan jenis kelamin tidak valid.')
        ]),
        'birthday': fields.Date(required=True, validate=[validate_age]),
        'relationship_status': fields.Str(validate=[
            validate_required,
            validate.OneOf(choices=['husband', 'wife', 'children', 'parents', 'sibling', 'cousin', 'nephew', 'assistant'], labels=['Suami', 'Istri', 'Anak', 'Orang Tua', 'Saudara Kandung', 'Sepupu', 'Keponakan', 'Asisten Rumah Tangga'], error='Pilihan status di keluarga tidak valid.')
        ], missing=None),
        'fullname': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama lengkap maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'phone': fields.Str(required=True, validate=[
            validate_required,validate_phone
        ]),        
        'idcard': fields.Str(validate=[
            validate_required,
            validate.OneOf(choices=['ktp', 'sim', 'passport', 'badge'], labels=['KTP', 'SIM', 'Paspor', 'Badge'], error='Pilihan jenis identitas tidak valid.')
        ], missing=None),
        'idcard_number': fields.Str(missing=None),
        'address': fields.Str(missing=None),
        'profile_region_id': fields.Int(missing=None),
        'residential_status': fields.Str(validate=[
            validate.OneOf(choices=['rent', 'owner', 'staffing'], labels=['Sewa', 'Pemilik', 'Rumah Dinas'], error='Pilihan status kepemilikan rumah tidak valid.')
        ], missing=None),
        'landlord_name': fields.Str(missing=None),
        'landlord_address': fields.Str(missing=None),
        'landlord_phone': fields.Str(missing=None),
        'landlord_region_id': fields.Int(missing=None),
        'store_name': fields.Str(missing=None),
        'store_about': fields.Str(missing=None),
        'status': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['inactive', 'active', 'banned'], labels=['Tidak Aktif', 'Aktif', 'Ditangguhkan'], error='Pilihan status pengguna tidak valid.')
        ]),
        'balance': fields.Int(missing=0),
        'point': fields.Int(missing=0),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'role': fields.Int(required=True),
        'parent': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'media': fields.Int(missing=None),
        'bank_accounts': fields.List(fields.Dict(), missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, user_id):

        ''' Edit User '''

        user_id = reverse_id(user_id)
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        gender = payload['gender']
        birthday = payload['birthday']
        relationship_status = payload['relationship_status'] if payload['relationship_status'] else None
        fullname = bleach.clean(payload['fullname'], strip=True)
        phone = normalize_phone_number(payload['phone'])
        idcard = payload['idcard'] if payload['idcard'] else None
        idcard_number = bleach.clean(payload['idcard_number'], strip=True) if payload['idcard_number'] else None
        address = bleach.clean(payload['address'], strip=True) if payload['address'] else None
        profile_region_id = reverse_id(payload['profile_region_id']) if payload['profile_region_id'] else None
        landlord_name = bleach.clean(payload['landlord_name'], strip=True) if payload['landlord_name'] else None
        landlord_address = bleach.clean(payload['landlord_address'], strip=True) if payload['landlord_address'] else None
        landlord_phone = normalize_phone_number(payload['landlord_phone']) if payload['landlord_phone'] else None
        landlord_region_id = reverse_id(payload['landlord_region_id']) if payload['landlord_region_id'] else None
        store_name = bleach.clean(payload['store_name'], strip=True) if payload['store_name'] else None
        store_about = bleach.clean(payload['store_about'], strip=True) if payload['store_about'] else None
        residential_status = payload['residential_status'] if payload['residential_status'] else None
        residential_id = reverse_id(payload['residential']) if payload['residential'] else None
        status = payload['status']
        balance = payload['balance']
        point = payload['point']
        latitude = payload['latitude']
        longitude = payload['longitude']
        role_id = reverse_id(payload['role'])
        parent_id = reverse_id(payload['parent']) if payload['parent'] else None        
        bank_accounts = payload['bank_accounts']
        isdeleted = payload['archived']

        if residential_status == 'rent' and (not landlord_name or not landlord_phone or not landlord_region_id):
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'landlord_name': ['Data pemilik rumah wjaib diisi.']
                    }
                }
            }), 422

        role = RoleModel.get_by(role_id)
        if role.slug in ['root', 'owner'] and g.current_user.role.slug != 'root':
            return jsonify({
                'success': False,
                'errors': {
                    'code': 403,
                    'messages': {
                        'forbidden': ['Anda tidak memiliki izin untuk melakukan permintaan ini.']
                    }
                }
            }), 403

        if role.slug not in ['root', 'owner'] and not residential_id:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'residential': ['Warga perumahan wajid diisi.']
                    }
                }
            }), 422

        if residential_id:
            residential = ResidentialModel.query.get(residential_id)
            if not residential:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 404,
                        'messages': {
                            'residential': ['Perumahan tidak ditemukan atau sudah dihapus.']
                        }
                    }
                }), 404

        user_exist = UserModel.query.filter(UserModel.phone==phone, UserModel.id!=user.id)

        if residential_id:
            user_exist = user_exist.filter(UserModel.residential_id==residential_id)

        if parent_id:
            user_exist = user_exist.filter(UserModel.parent_id==parent_id)

        if user_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'user': ['Pengguna sudah terdaftar.']
                    }
                }
            }), 422

        user.phone = phone
        user.role_id = role.id
        user.status = status
        user.relationship_status = relationship_status
        user.isdeleted = isdeleted        

        if parent_id:
            user.parent_id = parent_id
        elif role.slug in ['root', 'owner']:
            user.parent_id = None
        else:
            user.parent_id = residential.owner_id

        profile = ProfileModel.query.filter_by(user_id=user.id).first()
        profile.gender = gender
        profile.birthday = birthday
        profile.fullname = fullname
        profile.phone = phone
        profile.idcard = idcard
        profile.idcard_number = idcard_number
        profile.address = address
        profile.profile_region_id = profile_region_id
        profile.residential_status = residential_status
        profile.landlord_name = landlord_name
        profile.landlord_address = landlord_address
        profile.landlord_phone = landlord_phone
        profile.landlord_region_id = landlord_region_id
        profile.store_name = store_name
        profile.store_about = store_about
        profile.balance = balance
        profile.point = point
        profile.latitude = latitude
        profile.longitude = longitude

        if role.slug not in ['root', 'owner'] and residential:
            residential.users.append(user)

        if bank_accounts:
            for account in bank_accounts:
                if account['bank_id'] and account['account_name'] and account['account_number']:
                    bank_account = BankAccountModel()
                    bank_account.account_name = account['account_name']
                    bank_account.account_number = account['account_number']
                    bank_account.branch = account['branch']
                    bank_account.description = account['description']
                    bank_account.bank_id = reverse_id(account['bank_id'])
                    bank_account.holder_id = user.id

                    owner_id = None
                    if user.role.slug != 'root':
                        owner_id =  user.parent_id if user.parent_id else user.id

                    bank_account.owner_id = owner_id
                    bank_account.residential_id = user.residential_id

                    db.session.add(bank_account)

        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': 'Pengguna berhasil diubah.',
            'data': {
                'user': result
            }
        }), 200



    '''
    @method DELETE
    @endpoint /v1/users/<int:user_id>
    @permission jwt_required
    @return deleted user object
    '''
    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, user_id):

        ''' Delete User '''

        user_id = reverse_id(user_id)
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        if user.isdeleted:
            db.session.delete(user)
        else:
            user.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Pengguna berhasil dihapus.',
            'data': {
                'user': result
            }
        }), 200


api.add_resource(User, '/users/<int:user_id>')


class UserStore(Resource):

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Silakan isi semua kolom yang wajib diisi.')  

    put_args = {
        'store_name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/]{1,255})$', error='Panjang nama lengkap maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring dan spasi yang diperbolehkan.')]),
        'store_about': fields.Str(required=True, validate=[validate_required]),
        'store_open': fields.Str(missing=None),
        'store_close': fields.Str(missing=None),
        'latitude': fields.Decimal(missing=None),
        'longitude': fields.Decimal(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('user', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload):

        ''' Edit User Store '''

        user_id = g.current_user.id
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        store_name = bleach.clean(payload['store_name'], strip=True)
        store_about = bleach.clean(payload['store_about'], strip=True)
        store_open = payload['store_open']
        store_close = payload['store_close']
        latitude = payload['latitude']
        longitude = payload['longitude']

        user.profile.store_name = store_name
        user.profile.store_about = store_about
        user.profile.store_open = store_open
        user.profile.store_close = store_close
        user.profile.latitude = latitude
        user.profile.longitude = longitude

        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        profile = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': profile
            }
        }), 200

api.add_resource(UserStore, '/users/store')


class UserBankAccounts(Resource):

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Silakan isi semua kolom yang wajib diisi.') 


    '''
    @method GET
    @endpoint /v1/users/banks
    @permission jwt_required
    @return bank account list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('bankaccount', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Bank Accounts ''' 

        user_id = g.current_user.id
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        profile = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': profile
            }
        }), 200

    
    '''
    @method POST
    @endpoint /v1/users/banks
    @permission jwt_required
    @return bank account create
    ''' 

    post_args = {
        'account_name': fields.Str(required=True, validate=[validate_required]),
        'account_number': fields.Str(required=True, validate=[validate_required]),
        'branch': fields.Str(missing=None),
        'bank': fields.Int(required=True),
        'description': fields.Str(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('bankaccount', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Add User Bank Account '''

        user_id = g.current_user.id
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        account_name = bleach.clean(payload['account_name'], strip=True)
        account_number = bleach.clean(payload['account_number'], strip=True)
        branch = bleach.clean(payload['branch']) if payload['branch'] else None
        description = bleach.clean(payload['description']) if payload['description'] else None
        bank_id = reverse_id(payload['bank'])

        bank = BankModel.query.get(bank_id)

        if not bank:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'bank': ['Bank tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        account_exist = BankAccountModel.query.filter(BankAccountModel.account_number==account_number, BankAccountModel.bank_id==bank_id).first()

        if account_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'bankaccount': ['Rekening bank sudah terdaftar.']
                    }
                }
            }), 422
        
        bankaccount = BankAccountModel()
        bankaccount.account_name = account_name
        bankaccount.account_number = account_number
        bankaccount.branch = branch
        bankaccount.description = description
        bankaccount.bank_id = bank_id
        bankaccount.holder_id = g.current_user.id
        bankaccount.owner_id = g.owner_id
        bankaccount.residential_id = g.current_user.residential.id

        db.session.add(bankaccount)
        db.session.commit()

        user = UserModel.query.get(user_id)

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        profile = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': profile
            }
        }), 200

api.add_resource(UserBankAccounts, '/users/banks')

class UserBankAccount(Resource):

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Silakan isi semua kolom yang wajib diisi.') 


    '''
    @method GET
    @endpoint /v1/users/banks
    @permission jwt_required
    @return bank account list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('bankaccount', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, account_id):

        ''' View Bank Account ''' 
        account_id = reverse_id(account_id)
        user_id = g.current_user.id
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        bankaccount = BankAccountModel.query.filter_by(id=account_id, holder_id=user.id).first()

        if not bankaccount:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'bankaccount': ['Rekening tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = BankAccountSchema(only=args['only'], exclude=args['exclude'])
        account = schema.dump(bankaccount).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'bankaccount': account
            }
        }), 200

    
    '''
    @method POST
    @endpoint /v1/users/banks/<int:account_id>
    @permission jwt_required
    @return bank account edit
    ''' 

    put_args = {
        'account_name': fields.Str(required=True, validate=[validate_required]),
        'account_number': fields.Str(required=True, validate=[validate_required]),
        'branch': fields.Str(missing=None),
        'description': fields.Str(missing=None),
        'bank': fields.Int(required=True),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('bankaccount', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, account_id):

        ''' Edit User Bank Account '''

        account_id = reverse_id(account_id)
        user_id = g.current_user.id
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        bankaccount = BankAccountModel.query.filter_by(id=account_id, holder_id=user.id).first()

        if not bankaccount:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'bankaccount': ['Rekening tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        account_name = bleach.clean(payload['account_name'], strip=True)
        account_number = bleach.clean(payload['account_number'], strip=True)
        branch = bleach.clean(payload['branch']) if payload['branch'] else None
        description = bleach.clean(payload['description']) if payload['description'] else None
        bank_id = reverse_id(payload['bank'])

        bank = BankModel.query.get(bank_id)

        if not bank:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'bank': ['Bank tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        account_exist = BankAccountModel.query.filter(BankAccountModel.account_number == account_number, BankAccountModel.bank_id==bank_id, BankAccountModel.id!=account_id).first()

        if account_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'bankaccount': ['Rekening bank sudah terdaftar.']
                    }
                }
            }), 422
        
        bankaccount.account_name = account_name
        bankaccount.account_number = account_number
        bankaccount.branch = branch
        bankaccount.description = description
        bankaccount.bank_id = bank_id
        bankaccount.holder_id = g.current_user.id
        bankaccount.owner_id = g.owner_id
        bankaccount.residential_id = g.current_user.residential.id

        db.session.commit()

        user = UserModel.query.get(user.id)

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        profile = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': profile
            }
        }), 200

    '''
    @method DELETE
    @endpoint /v1/users/banks/<int:account_id>
    @permission jwt_required
    @return deleted bank account object
    '''
    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('bankaccount', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, account_id):

        ''' Delete Bank Account '''

        account_id = reverse_id(account_id)
        user_id = g.current_user.id
        user = UserModel.get_by(user_id)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        bankaccount = BankAccountModel.query.filter_by(id=account_id,holder_id=user.id).first()
        if not bankaccount:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'bankaccounr': ['Rekening bank tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        db.session.delete(bankaccount)
        db.session.commit()

        schema = UserSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': 'Rekening bank berhasil dihapus.',
            'data': {
                'user': result
            }
        }), 200

api.add_resource(UserBankAccount, '/users/banks/<int:account_id>')


class UserBy(Resource):

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Silakan isi semua kolom yang wajib diisi.') 


    '''
    @method GET
    @endpoint /v1/users/by
    @permission jwt_required
    @return user list
    '''  

    get_args = {
        'id': fields.Int(missing=None),
        'parent': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'phone': fields.Str(missing=None),
        'invitable': fields.Bool(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('user', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All User by Phone ''' 

        user_id = reverse_id(args['id']) if args['id'] else None
        parent_id = reverse_id(args['parent']) if args['parent'] else None
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        phone = normalize_phone_number(args['phone']) if args['phone'] else None
        invitable = args['invitable']

        user = UserModel.get_by(user_id, phone, parent_id, residential_id, invitable)

        if not user:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'user': ['Pengguna tidak ditemukan atau sudah dihapus.']
                    }
                },
                'phone': phone
            }), 404

        schema = UserSchema(only=args['only'], exclude=args['exclude'])
        profile = schema.dump(user).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'user': profile
            }
        }), 200

api.add_resource(UserBy, '/users/by')


class UserNotifications(Resource):

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError('Silakan isi semua kolom yang wajib diisi.') 


    '''
    @method GET
    @endpoint /v1/users/notifications
    @permission jwt_required
    @return user list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'user': fields.Int(missing=None),
        'residential': fields.Int(missing=None),
        'total_only': fields.Bool(missing=None),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('user', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Notifications ''' 

        q = args['q']
        user_id = reverse_id(args['user']) if args['user'] else g.current_user.id
        residential_id = reverse_id(args['residential']) if args['residential'] else None
        total_only = args['total_only']

        if total_only:
            total = UserNotificationModel.count_all(q, user_id, residential_id)
            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'total_notification': total
                }
            }), 200

        else:

            page = args['page']
            perpage = args['perpage']

            total = UserNotificationModel.count_all(q, user_id, residential_id)
            notifications = UserNotificationModel.get_all(q, user_id, residential_id, page, perpage)

            schema = UserNotificationSchema(only=args['only'], exclude=args['exclude'], many=True)
            result = schema.dump(notifications).data

            pagination = Pagination('api.usernotifications', total, **args)

            return jsonify({
                'success': True,
                'message': '',
                'data': {
                    'notifications': result,
                    'pagination': pagination.paginate
                }
            }), 200

api.add_resource(UserNotifications, '/users/notifications')