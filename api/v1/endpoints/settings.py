# -*- coding: utf-8 -*-
import arrow
import bleach
import time
import re
import os

from ua_parser import user_agent_parser as ua_parser

from datetime import (
    datetime,
    timedelta
)

from flask import (
    g,
    current_app,
    jsonify
)
from flask_cors import cross_origin
from flask_jwt_extended import (
    jwt_required
)
from flask_restful import (
    Resource
)
from webargs import (
    fields,
    validate,
    missing,
    ValidationError
)
from webargs.flaskparser import (
    use_args,
    use_kwargs,
    parser
)
from slugify import slugify
from sqlalchemy.sql.expression import func, and_, or_

from api import cache, db, jwt, mail
from api.v1 import api

from api.v1.models.media import(
    MediaModel
)
from api.v1.models.settings import (
    CategoryModel,
    BankModel,
    RegionModel,
    PaymentMethodModel,
    UnitModel,
    ContactModel
)
from api.v1.schemas.settings import(
    CategorySchema,
    BankSchema,
    RegionSchema,
    PaymentMethodSchema,
    UnitSchema,
    ContactSchema
)
from api.v1.utils import(
    reverse_id,
    is_dir_exist,
    is_file_exist,
    normalize_phone_number
)
from api.v1.utils.pagination import (
    Pagination
)
from api.v1.decorators import (
    permission_required,
    current_user,
    ownership
)



class Banks(Resource):
    
    '''
    @method GET
    @endpoint /v1/banks
    @permission jwt_required
    @return banks list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @permission_required('bank', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Banks '''

        q = args['q']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = BankModel.count_all(q, isdeleted)
        query = BankModel.get_all(q, isdeleted, page, perpage)

        schema = BankSchema(only=args['only'], exclude=args['exclude'], many=True)
        banks = schema.dump(query).data
        pagination = Pagination('api.banks', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'banks': banks,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/banks
    @permission jwt_required
    @return new bank object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'code': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([\d]{1,6})$', error='Panjang kode bank maksimal 6 karakter. Hanya huruf, angka yang diperbolehkan.')
        ]),
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama bank maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama bank maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi bank maksimal 255 karakter.')
        ]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('bank', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Bank '''

        code = payload['code']
        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        if BankModel.query.filter(BankModel.code==code, BankModel.slug==slug).first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'bank': ['Nama bank sudah terdaftar.']
                    }
                }
            }), 422

        bank = BankModel()
        bank.code = code
        bank.name = name
        bank.slug = slug
        bank.description = description
        bank.isdeleted = False

        db.session.add(bank)
        db.session.commit()

        schema = BankSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(bank).data

        return jsonify({
            'success': True,
            'message': 'Bank berhasil ditambahkan.',
            'data': {
                'bank': result
            }
        }), 201


api.add_resource(Banks, '/banks')


class Bank(Resource):
    
    '''
    @method GET
    @endpoint /v1/banks/<int:bank_id>
    @permission jwt_required
    @return banks list
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('bank', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, bank_id):

        ''' View Bank '''

        bank_id = reverse_id(bank_id)
        bank = BankModel.get_by(bank_id)

        if not bank:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'bank': ['Bank tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = BankSchema(only=args['only'], exclude=args['exclude'], many=True)
        result = schema.dump(bank).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'bank': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/banks/<int:bank_id>
    @permission jwt_required
    @return updated bank object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'code': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([\d]{1,6})$', error='Panjang kode bank maksimal 6 karakter. Hanya huruf, angka yang diperbolehkan.')
        ]),
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama bank maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama bank maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi bank maksimal 255 karakter.')
        ]),
        'archived': fields.Bool(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('bank', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, bank_id):

        ''' Edit Bank '''

        bank_id = reverse_id(bank_id)
        bank = BankModel.get_by(bank_id)

        if not bank:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'bank': ['Bank tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        code = payload['code']
        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        isdeleted = payload['archived']

        if BankModel.query.filter(BankModel.code==code, BankModel.slug==slug, BankModel.id!=bank_id).first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'bank': ['Nama bank sudah terdaftar.']
                    }
                }
            }), 422

        bank.code = code
        bank.name = name
        bank.slug = slug
        bank.description = description
        bank.isdeleted = isdeleted

        db.session.commit()

        schema = BankSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(bank).data

        return jsonify({
            'success': True,
            'message': 'Bank berhasil diubah.',
            'data': {
                'bank': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/banks/<int:bank_id>
    @permission jwt_required
    @return deleted bank object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('bank', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, bank_id):

        ''' Delete Bank '''

        bank_id = reverse_id(bank_id)
        bank = BankModel.get_by(bank_id)

        if not bank:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'bank': ['Bank tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = BankSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(bank).data

        if bank.isdeleted:
            db.session.delete(bank)
        else:
            bank.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Bank berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'bank': result
            }
        }), 200


api.add_resource(Bank, '/banks/<int:bank_id>')

class Units(Resource):
    
    '''
    @method GET
    @endpoint /v1/units
    @permission jwt_required
    @return banks list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('unit', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Units '''

        q = args['q']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = UnitModel.count_all(q, g.owner_id, isdeleted)
        query = UnitModel.get_all(q, g.owner_id, isdeleted, page, perpage)

        schema = UnitSchema(only=args['only'], exclude=args['exclude'], many=True)
        units = schema.dump(query).data
        pagination = Pagination('api.units', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'units': units,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/units
    @permission jwt_required
    @return new payment term object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama unit maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama unit maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'symbol': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9]{1,10})$', error='Panjang simbol unit maksimal 10 karakter. Hanya huruf dan angka yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi unit maksimal 255 karakter.')
        ]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('unit', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Unit '''

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        symbol = bleach.clean(payload['symbol'], strip=True) if payload['symbol'] else None
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        unit_exist = UnitModel.query.filter(UnitModel.slug==slug)

        if g.owner_id:
            unit_exist = unit_exist.filter(UnitModel.owner_id==g.owner_id)

        if unit_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'unit': ['Nama unit sudah terdaftar.']
                    }
                }
            }), 422

        unit = UnitModel()
        unit.name = name
        unit.slug = slug
        unit.symbol = symbol
        unit.description = description
        unit.owner_id = g.owner_id
        unit.isdeleted = False
        unit.author_id = g.current_user.id

        db.session.add(unit)
        db.session.commit()

        schema = UnitSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(unit).data

        return jsonify({
            'success': True,
            'message': 'Unit berhasil ditambahkan.',
            'data': {
                'unit': result
            }
        }), 201


api.add_resource(Units, '/units')


class Unit(Resource):
    
    '''
    @method GET
    @endpoint /v1/units/<int:unit_id>
    @permission jwt_required
    @return unit object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('unit', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, unit_id):

        ''' View Unit '''

        unit_id = reverse_id(unit_id)
        unit = UnitModel.get_by(unit_id, g.owner_id)

        if not unit:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'unit': ['Unit tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = UnitSchema(only=args['only'], exclude=args['exclude'], many=True)
        result = schema.dump(unit).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'unit': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/units/<int:unit_id>
    @permission jwt_required
    @return updated unit object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama unit maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama unit maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'symbol': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9]{1,10})$', error='Panjang simbol unit maksimal 10 karakter. Hanya huruf dan angka yang diperbolehkan.')
        ]),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi unit maksimal 255 karakter.')
        ]),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('unit', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, unit_id):

        ''' Edit Unit '''

        unit_id = reverse_id(unit_id)
        unit = UnitModel.get_by(unit_id, g.owner_id)

        if not unit:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'unit': ['Unit tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        symbol = bleach.clean(payload['symbol'], strip=True) if payload['symbol'] else None
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        isdeleted = payload['archived']

        unit_exist = UnitModel.query.filter(UnitModel.slug==slug, UnitModel.id!=unit_id)

        if g.owner_id:
            unit_exist = unit_exist.filter(UnitModel.owner_id==g.owner_id)

        if unit_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'unit': ['Nama unit sudah terdaftar.']
                    }
                }
            }), 422

        unit.name = name
        unit.slug = slug
        unit.symbol = symbol
        unit.description = description
        unit.owner_id = g.owner_id
        unit.isdeleted = isdeleted

        db.session.commit()

        schema = UnitSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(unit).data

        return jsonify({
            'success': True,
            'message': 'Unit berhasil diubah.',
            'data': {
                'unit': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/units/<int:unit_id>
    @permission jwt_required
    @return deleted unit object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('unit', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, unit_id):

        ''' Delete Unit '''

        unit_id = reverse_id(unit_id)
        unit = UnitModel.get_by(unit_id, g.owner_id)

        if not unit:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'unit': ['Unit tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = UnitSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(unit).data

        if unit.isdeleted:
            db.session.delete(unit)
        else:
            unit.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Unit berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'unit': result
            }
        }), 200


api.add_resource(Unit, '/units/<int:unit_id>')


class Regions(Resource):
    
    '''
    @method GET
    @endpoint /v1/regions
    @permission jwt_required
    @return region list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'parent': fields.Int(missing=None),
        'administrative': fields.Bool(missing=False),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('region', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Region '''

        q = args['q']
        parent = args['parent']
        administrative = args['administrative']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = RegionModel.count_all(q, parent, administrative, isdeleted)
        query = RegionModel.get_all(q, parent, administrative, isdeleted, page, perpage)

        schema = RegionSchema(only=args['only'], exclude=args['exclude'], many=True)
        regions = schema.dump(query).data
        pagination = Pagination('api.regions', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'regions': regions,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/regions
    @permission jwt_required
    @return new region object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama wilayah maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama wilayah maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'latitude': fields.Str(missing=None),
        'longitude': fields.Str(missing=None),
        'polygon': fields.Str(missing=None),
        'postal': fields.Str(missing=None),
        'parent_id': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('region', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create Region '''

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        latitude = bleach.clean(payload['latitude'], strip=True) if payload['latitude'] else None
        longitude = bleach.clean(payload['longitude'], strip=True) if payload['longitude'] else None
        polygon = bleach.clean(payload['polygon'], strip=True) if payload['polygon'] else None
        postal = bleach.clean(payload['postal'], strip=True) if payload['postal'] else None
        parent_id = reverse_id(payload['parent_id']) if payload['parent_id'] else None

        region_exist = RegionModel.query.filter(RegionModel.slug==slug, RegionModel.parent_id==parent_id).first()

        if region_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'region': ['Nama wilayah sudah terdaftar.']
                    }
                }
            }), 422

        region = RegionModel()
        region.name = name
        region.slug = slug
        region.latitude = latitude
        region.longitude = longitude
        region.polygon = polygon
        region.postal = postal
        region.parent_id = parent_id
        region.isdeleted = False

        if parent_id:
            parent = RegionModel.query.get(parent_id)
            region.path = parent.path + '/' + slug
        else:
            region.path = slug

        db.session.add(region)
        db.session.commit()

        schema = RegionSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(region).data

        return jsonify({
            'success': True,
            'message': 'Wilayah berhasil ditambahkan.',
            'data': {
                'region': result
            }
        }), 201


api.add_resource(Regions, '/regions')


class Region(Resource):
    
    '''
    @method GET
    @endpoint /v1/regions/<int:region_id>
    @permission jwt_required
    @return region object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('region', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, region_id):

        ''' View Region '''

        region_id = reverse_id(region_id)
        region = RegionModel.get_by(region_id)

        if not region:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'region': ['Wilayah tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = RegionSchema(only=args['only'], exclude=args['exclude'], many=True)
        result = schema.dump(region).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'region': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/regions/<int:region_id>
    @permission jwt_required
    @return updated region object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama wilayah maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama wilayah maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'latitude': fields.Str(missing=None),
        'longitude': fields.Str(missing=None),
        'polygon': fields.Str(missing=None),
        'postal': fields.Str(missing=None),
        'parent_id': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('region', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, region_id):

        ''' Edit Unit '''

        region_id = reverse_id(region_id)
        region = RegionModel.get_by(region_id)

        if not region:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'region': ['Wilayah tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        latitude = bleach.clean(payload['latitude'], strip=True) if payload['latitude'] else None
        longitude = bleach.clean(payload['longitude'], strip=True) if payload['longitude'] else None
        polygon = bleach.clean(payload['polygon'], strip=True) if payload['polygon'] else None
        postal = bleach.clean(payload['postal'], strip=True) if payload['postal'] else None
        parent_id = reverse_id(payload['parent_id']) if payload['parent_id'] else None
        isdeleted = payload['archived']

        region_exist = RegionModel.query.filter(RegionModel.slug==slug, RegionModel.parent_id==parent_id, RegionModel.id!=unit_id).first()

        if region_exist:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'region': ['Nama wilayah sudah terdaftar.']
                    }
                }
            }), 422

        region.name = name
        region.slug = slug
        region.latitude = latitude
        region.longitude = longitude
        region.polygon = polygon
        region.postal = postal
        region.parent_id = parent_id
        region.isdeleted = archived

        if parent_id:
            parent = RegionModel.query.get(parent_id)
            region.path = parent.path + '/' + slug
        else:
            region.path = slug

        db.session.commit()

        schema = RegionSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(region).data

        return jsonify({
            'success': True,
            'message': 'Wilayah berhasil diubah.',
            'data': {
                'region': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/regions/<int:region_id>
    @permission jwt_required
    @return deleted region object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @permission_required('region', 'deleteother')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, region_id):

        ''' Delete Unit '''

        region_id = reverse_id(region_id)
        region = RegionModel.get_by(region_id)

        if not region:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'region': ['Wilayah tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = RegionSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(region).data

        if region.isdeleted:
            db.session.delete(region)
        else:
            region.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Wilayah berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'region': result
            }
        }), 200


api.add_resource(Region, '/regions/<int:region_id>')

class PaymentMethods(Resource):
    
    '''
    @method GET
    @endpoint /v1/payment-methods
    @permission jwt_required
    @return banks list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('paymentmethod', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Payment Methods '''

        q = args['q']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = PaymentMethodModel.count_all(q, g.owner_id, isdeleted)
        query = PaymentMethodModel.get_all(q, g.owner_id, isdeleted, page, perpage)

        schema = PaymentMethodSchema(only=args['only'], exclude=args['exclude'], many=True)
        payment_methods = schema.dump(query).data
        pagination = Pagination('api.paymentmethods', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'payment_methods': payment_methods,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/payment-methods
    @permission jwt_required
    @return new payment method object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama metode pembayaran maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama metode pembayaran maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'charge': fields.Int(missing=0),
        'charge_percent': fields.Int(missing=0),
        'fee': fields.Int(missing=0),
        'fee_percent': fields.Int(missing=0),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi metode pembayaran maksimal 255 karakter.')
        ]),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('paymentmethod', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Payment Method '''    

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        charge = payload['charge']
        charge_percent = payload['charge_percent']
        fee = payload['fee']
        fee_percent = payload['fee_percent']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None

        payment_method_exist = PaymentMethodModel.query.filter(PaymentMethodModel.slug==slug)

        if g.owner_id:
            payment_method_exist = payment_method_exist.filter(PaymentMethodModel.owner_id==g.owner_id)

        if payment_method_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'payment_method': ['Nama metode pembayaran sudah terdaftar.']
                    }
                }
            }), 422

        payment_method = PaymentMethodModel()
        payment_method.name = name
        payment_method.slug = slug
        payment_method.charge = charge
        payment_method.charge_percent = charge_percent
        payment_method.fee = fee
        payment_method.fee_percent = fee_percent
        payment_method.description = description
        payment_method.owner_id = g.owner_id
        payment_method.author_id = g.current_user.id
        payment_method.isdeleted = False

        db.session.add(payment_method)
        db.session.commit()

        schema = PaymentMethodSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(payment_method).data

        return jsonify({
            'success': True,
            'message': 'Metode pembayaran berhasil ditambahkan.',
            'data': {
                'payment_method': result
            }
        }), 201


api.add_resource(PaymentMethods, '/payment-methods')


class PaymentMethod(Resource):
    
    '''
    @method GET
    @endpoint /v1/payment-methods/<int:payment_method_id>
    @permission jwt_required
    @return payment method object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('paymentmethod', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, payment_method_id):

        ''' View Payment Method '''

        payment_method_id = reverse_id(payment_method_id)
        payment_method = PaymentMethodModel.get_by(payment_method_id, g.owner_id)

        if not payment_method:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'payment_method': ['Metode pembayaran tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = PaymentMethodSchema(only=args['only'], exclude=args['exclude'], many=True)
        result = schema.dump(payment_method).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'payment_method': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/payment-methods/<int:payment_method_id>
    @permission jwt_required
    @return updated payment method object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama metode pembayaran maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama metode pembayaran maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'charge': fields.Int(missing=0),
        'charge_percent': fields.Int(missing=0),
        'fee': fields.Int(missing=0),
        'fee_percent': fields.Int(missing=0),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi metode pembayaran maksimal 255 karakter.')
        ]),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('paymentmethod', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, payment_method_id):

        ''' Edit Payment Method '''

        payment_method_id = reverse_id(payment_method_id)
        payment_method = PaymentMethodModel.get_by(payment_method_id, g.owner_id)

        if not payment_method:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'payment_method': ['Metode pembayaran tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        charge = payload['charge']
        charge_percent = payload['charge_percent']
        fee = payload['fee']
        fee_percent = payload['fee_percent']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        isdeleted = payload['archived']

        payment_method_exist = PaymentMethodModel.query.filter(PaymentMethodModel.slug==slug, PaymentMethodModel.id!=payment_method_id)

        if g.owner_id:
            payment_method_exist = payment_method_exist.filter(PaymentMethodModel.owner_id==g.owner_id)

        if payment_method_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'payment_method': ['Nama metode pembayaran sudah terdaftar.']
                    }
                }
            }), 422

        payment_method.name = name
        payment_method.slug = slug
        payment_method.charge = charge
        payment_method.charge_percent = charge_percent
        payment_method.fee = fee
        payment_method.fee_percent = fee_percent
        payment_method.description = description
        payment_method.isdeleted = isdeleted

        db.session.commit()

        schema = PaymentMethodSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(payment_method).data

        return jsonify({
            'success': True,
            'message': 'Metode pembayaran berhasil diubah.',
            'data': {
                'payment_method': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/payment-methods/<int:payment_method_id>
    @permission jwt_required
    @return deleted payment method object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('paymentmethod', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, payment_method_id):

        ''' Delete Payment Method '''  

        payment_method_id = reverse_id(payment_method_id)
        payment_method = PaymentMethodModel.get_by(payment_method_id, g.owner_id)

        if not payment_method:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'payment_method': ['Metode pembayaran tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = PaymentMethodSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(payment_method).data

        if payment_method.isdeleted:
            db.session.delete(payment_method)
        else:
            payment_method.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Metode pembayaran berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'payment_method': result
            }
        }), 200


api.add_resource(PaymentMethod, '/payment-methods/<int:payment_method_id>')


class Categories(Resource):
    
    '''
    @method GET
    @endpoint /v1/categories
    @permission jwt_required
    @return category list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'module': fields.Str(validate=[
            validate.OneOf(choices=['announcement', 'residentreport', 'financialreport', 'marketplace', 'inventory'], labels=['Pemberitahuan', 'Laporan Warga', 'Laporan Keuangan', 'Marketplace', 'Inventaris'], error='Pilihan modul tidak valid.')
        ], missing=None),
        'parent': fields.Int(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('category', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Categories '''

        q = args['q']
        module = args['module']
        if args['parent'] and args['parent'] != 0:
            parent_id = reverse_id(args['parent']) 
        elif args['parent'] is None:
            parent_id = 0
        else:
            parent_id = args['parent']

        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = CategoryModel.count_all(q, module, parent_id, g.owner_id, isdeleted)
        query = CategoryModel.get_all(q, module, parent_id, g.owner_id, isdeleted, page, perpage)

        schema = CategorySchema(only=args['only'], exclude=args['exclude'], many=True)
        categories = schema.dump(query).data
        pagination = Pagination('api.categories', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'categories': categories,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/categories
    @permission jwt_required
    @return new category object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'module': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['announcement', 'residentreport', 'financialreport', 'marketplace', 'inventory'], labels=['Pemberitahuan', 'Laporan Warga', 'Laporan Keuangan', 'Marketplace', 'Inventaris'], error='Pilihan modul tidak valid.')
        ]),
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama kategori maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama kategori maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'icon': fields.Str(missing=None),
        'parent': fields.Int(missing=None),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi kategori maksimal 255 karakter.')
        ]),
        'media': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('category', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Category '''

        module = payload['module']
        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        icon = bleach.clean(payload['icon'], strip=True) if payload['icon'] else None
        parent_id = reverse_id(payload['parent']) if payload['parent'] else None
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        media_id = reverse_id(payload['media']) if payload['media'] else None

        category_exist = CategoryModel.query.filter(CategoryModel.slug==slug)

        if g.owner_id:
            category_exist = category_exist.filter(CategoryModel.owner_id==g.owner_id)

        if category_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'category': ['Nama kategori sudah terdaftar.']
                    }
                }
            }), 422

        category = CategoryModel()
        category.module = module
        category.name = name
        category.slug = slug
        category.icon = icon
        category.path = slug
        category.description = description
        category.parent_id = parent_id
        category.owner_id = g.owner_id
        category.author_id = g.current_user.id
        category.isdeleted = False

        if parent_id:
            parent = CategoryModel.query.get(parent_id)
            if parent:
                category.slug = parent.path + '/' + slug
        else:
            category.path = slug

        db.session.add(category)
        db.session.commit()

        if media_id:
            media = MediaModel.query.get(media_id)

            if media and media.isorphan:
                media.category_id = category.id
                media.residential_id = user.residential_id

                media.owner_id = g.owner_id

                media.isorphan = False
                media.expired = None

                file_path = os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)
                new_path = os.path.join(current_app.config.get('CATEGORY_FOLDER'), media.filename)

                is_dir_exist(new_path)

                if is_file_exist(file_path):
                    os.rename(file_path, new_path)

                db.session.commit()     

        schema = CategorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(category).data

        return jsonify({
            'success': True,
            'message': 'Kategori berhasil ditambahkan.',
            'data': {
                'category': result
            }
        }), 201


api.add_resource(Categories, '/categories')


class Category(Resource):
    
    '''
    @method GET
    @endpoint /v1/categories/<int:category_id>
    @permission jwt_required
    @return category object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('category', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, category_id):

        ''' View Category '''

        category_id = reverse_id(category_id)
        category = CategoryModel.get_by(category_id)

        if not category or (category.module != 'marketplace' and category.owner_id != g.owner_id): 
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'category': ['Kategori tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404            

        schema = CategorySchema(only=args['only'], exclude=args['exclude'])
        result = schema.dump(category).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'category': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/categories/<int:category_id>
    @permission jwt_required
    @return updated category object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'module': fields.Str(required=True, validate=[
            validate_required,
            validate.OneOf(choices=['announcement', 'residentreport', 'financialreport', 'marketplace', 'inventory'], labels=['Pemberitahuan', 'Laporan Warga', 'Laporan Keuangan', 'Marketplace', 'Inventaris'], error='Pilihan modul tidak valid.')
        ]),
        'name': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama kategori maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama kategori maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'icon': fields.Str(missing=None),
        'parent': fields.Int(missing=None),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi kategori maksimal 255 karakter.')
        ]),
        'archived': fields.Bool(missing=False),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('category', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, category_id):

        ''' Edit Category '''

        category_id = reverse_id(category_id)
        category = CategoryModel.get_by(category_id, g.owner_id)

        if not category:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'category': ['Kategori tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        module = payload['module']
        name = bleach.clean(payload['name'], strip=True)
        slug = slugify(name) if not payload['slug'] else slugify(payload['slug'])
        icon = bleach.clean(payload['icon'], strip=True) if payload['icon'] else None
        parent_id = reverse_id(payload['parent']) if payload['parent'] else None
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        isdeleted = payload['archived']

        category_exist = CategoryModel.query.filter(CategoryModel.slug==slug, CategoryModel.id!=category.id)

        if g.owner_id:
            category_exist = category_exist.filter(CategoryModel.owner_id==g.owner_id)

        if category_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'category': ['Nama kategori sudah terdaftar.']
                    }
                }
            }), 422

        category.module = module
        category.name = name
        category.slug = slug
        category.icon = icon
        category.description = description
        category.parent_id = parent_id
        category.owner_id = g.owner_id
        category.isdeleted = isdeleted

        if parent_id:
            parent = CategoryModel.query.get(parent_id)
            if parent:
                category.slug = parent.path + '/' + slug
        else:
            category.path = slug

        db.session.commit()

        schema = CategorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(category).data

        return jsonify({
            'success': True,
            'message': 'Kategori berhasil diubah.',
            'data': {
                'category': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/categories/<int:category_id>
    @permission jwt_required
    @return deleted category object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('category', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, category_id):

        ''' Delete Category '''  

        category_id = reverse_id(category_id)
        category = CategoryModel.get_by(category_id, g.owner_id)

        if not category:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'category': ['Kategori tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = CategorySchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(category).data

        if category.isdeleted:
            if category.media:
                os.remove(os.path.join(current_app.config.get('CATEGORY_FOLDER'), category.media.filename))
            db.session.delete(category)
        else:
            category.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Kategori berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'category': result
            }
        }), 200


api.add_resource(Category, '/categories/<int:category_id>')

class Contacts(Resource):
    
    '''
    @method GET
    @endpoint /v1/contacts
    @permission jwt_required
    @return category list
    '''  

    get_args = {
        'q': fields.Str(missing=None),
        'archived': fields.Bool(missing=False),
        'page': fields.Int(missing=1),
        'perpage': fields.Int(validate=validate.OneOf(choices=[25, 50, 100], error='Pilihan jumlah data yang ditampilkan per halaman tidak valid.'), missing=25),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('contact', 'viewall')
    @use_args(get_args, locations=['query'])
    def get(self, args):

        ''' All Contacts '''

        q = args['q']
        isdeleted = args['archived']

        page = args['page']
        perpage = args['perpage']

        total = ContactModel.count_all(q, g.owner_id, isdeleted)
        query = ContactModel.get_all(q, g.owner_id, isdeleted, page, perpage)

        schema = ContactSchema(only=args['only'], exclude=args['exclude'], many=True)
        contacts = schema.dump(query).data
        pagination = Pagination('api.contacts', total, **args)

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'contacts': contacts,
                'pagination': pagination.paginate
            }
        }), 200


    '''
    @method POST
    @endpoint /v1/contacts
    @permission jwt_required
    @return new contact object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    post_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama kontak maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'fullname': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama lengkap kontak maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama kontak maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'address': fields.Str(required=True, validate=[
            validate.Length(max=255, error='Panjang alamat maksimal 255 karakter.')
        ]),
        'phone': fields.Str(required=True, validate=[
            validate_required,
            validate.Length(max=45, error='Panjang no. telepon maksimal 45 karakter.')
        ]),
        'alt_phone': fields.Str(missing=None, validate=[
            validate.Length(max=45, error='Panjang no. telepon alternatif maksimal 45 karakter.')
        ]),
        'email': fields.Str(missing=None),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi bank maksimal 255 karakter.')
        ]),
        'latitude': fields.Str(missing=None),
        'longitude': fields.Str(missing=None),
        'region': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('contact', 'create')
    @use_args(post_args, locations=['json'])
    def post(self, payload):

        ''' Create New Contact '''

        title = bleach.clean(payload['title'], strip=True)      
        slug = slugify(payload['slug']) if payload['slug'] else slugify(title)  
        fullname = bleach.clean(payload['fullname'], strip=True)        
        address = bleach.clean(payload['address'], strip=True)
        phone = normalize_phone_number(payload['phone'])
        alt_phone = normalize_phone_number(payload['alt_phone']) if payload['alt_phone'] else None
        email = payload['email']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        latitude = payload['latitude']
        longitude = payload['longitude']
        region_id = reverse_id(payload['region']) if payload['region'] else None

        if email:
            email_match = re.match(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', email)

            if email_match is None:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'email': ['Alamat email tidak valid']
                        }
                    }
                }), 422

        contact_exist = ContactModel.query.filter(ContactModel.slug==slug, ContactModel.phone==phone)

        if g.owner_id:
            contact_exist = contact_exist.filter(ContactModel.owner_id==g.owner_id)

        if contact_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'contact': ['Kontak sudah terdaftar.']
                    }
                }
            }), 422

        contact = ContactModel()
        contact.title = title
        contact.fullname = fullname
        contact.slug = slug
        contact.address = address
        contact.phone = phone
        contact.alt_phone = alt_phone
        contact.email = email
        contact.description = description
        contact.latitude = latitude
        contact.longitude = longitude
        contact.region_id = region_id
        contact.owner_id = g.owner_id
        contact.author_id = g.current_user.id
        contact.isdeleted = False

        db.session.add(contact)
        db.session.commit()   

        schema = ContactSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(contact).data

        return jsonify({
            'success': True,
            'message': 'Kontak berhasil ditambahkan.',
            'data': {
                'contact': result
            }
        }), 201


api.add_resource(Contacts, '/contacts')


class Contact(Resource):
    
    '''
    @method GET
    @endpoint /v1/contacts/<int:contact_id>
    @permission jwt_required
    @return contact object
    '''  

    get_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('contact', 'viewown')
    @use_args(get_args, locations=['query'])
    def get(self, args, contact_id):

        ''' View Contact '''

        contact_id = reverse_id(contact_id)
        contact = ContactModel.get_by(contact_id, g.owner_id)

        if not contact:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'contact': ['Kontak tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = ContactSchema(only=args['only'], exclude=args['exclude'], many=True)
        result = schema.dump(contact).data

        return jsonify({
            'success': True,
            'message': '',
            'data': {
                'contact': result
            }
        }), 200


    '''
    @method PUT
    @endpoint /v1/contacts/<int:contact_id>
    @permission jwt_required
    @return updated contact object
    '''  

    def validate_required(value):
        if len(value.strip()) == 0:
            raise ValidationError(message='Silakan isi semua kolom yang wajib diisi.')   

    put_args = {
        'title': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama kontak maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'fullname': fields.Str(required=True, validate=[
            validate_required,
            validate.Regexp(r'^([a-zA-Z0-9-()&\s/+.,]{1,255})$', error='Panjang nama lengkap kontak maksimal 255 karakter. Hanya huruf, angka, tanda minus, tanda kurung, ampersand, garis miring, tanda plus, tanda koma, tanda titik dan spasi yang diperbolehkan.')
        ]),
        'slug': fields.Str(missing=None, validate=[
            validate.Regexp(r'^([a-zA-Z0-9-]{1,255})$', error='Panjang nama kontak maksimal 255 karakter. Hanya huruf, angka dan tanda minus yang diperbolehkan.')
        ]),
        'address': fields.Str(required=True, validate=[
            validate.Length(max=255, error='Panjang alamat maksimal 255 karakter.')
        ]),
        'phone': fields.Str(required=True, validate=[
            validate_required,
            validate.Length(max=45, error='Panjang no. telepon maksimal 45 karakter.')
        ]),
        'alt_phone': fields.Str(missing=None, validate=[
            validate.Length(max=45, error='Panjang no. telepon alternatif maksimal 45 karakter.')
        ]),
        'email': fields.Str(missing=None),
        'description': fields.Str(missing=None, validate=[
            validate.Length(max=255, error='Panjang deskripsi bank maksimal 255 karakter.')
        ]),
        'latitude': fields.Str(missing=None),
        'longitude': fields.Str(missing=None),
        'region': fields.Int(missing=None),
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('contact', 'editown')
    @use_args(put_args, locations=['json'])
    def put(self, payload, contact_id):

        ''' Edit Contact '''

        contact_id = reverse_id(contact_id)
        contact = ContactModel.get_by(contact_id, g.owner_id)

        if not contact:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'contact': ['Kontak tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        title = bleach.clean(payload['title'], strip=True)      
        slug = slugify(payload['slug']) if payload['slug'] else slugify(title)  
        fullname = bleach.clean(payload['fullname'], strip=True)        
        address = bleach.clean(payload['address'], strip=True)
        phone = normalize_phone_number(payload['phone'])
        alt_phone = normalize_phone_number(payload['alt_phone']) if payload['alt_phone'] else None
        email = payload['email']
        description = bleach.clean(payload['description'], strip=True) if payload['description'] else None
        latitude = payload['latitude']
        longitude = payload['longitude']
        region_id = reverse_id(payload['region']) if payload['region'] else None

        if email:
            email_match = re.match(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', email)

            if email_match is None:
                return jsonify({
                    'success': False,
                    'errors': {
                        'code': 422,
                        'messages': {
                            'email': ['Alamat email tidak valid']
                        }
                    }
                }), 422

        contact_exist = ContactModel.query.filter(ContactModel.slug==slug, ContactModel.phone==phone, ContactModel.id!=contact.id)

        if g.owner_id:
            contact_exist = contact_exist.filter(ContactModel.owner_id==g.owner_id)

        if contact_exist.first():
            return jsonify({
                'success': False,
                'errors': {
                    'code': 422,
                    'messages': {
                        'contact': ['Kontak sudah terdaftar.']
                    }
                }
            }), 422

        contact.title = title
        contact.fullname = fullname
        contact.slug = slug
        contact.address = address
        contact.phone = phone
        contact.alt_phone = alt_phone
        contact.description = description
        contact.latitude = latitude
        contact.longitude = longitude
        contact.region_id = region_id
        contact.isdeleted = False

        db.session.commit()   

        schema = ContactSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(contact).data

        return jsonify({
            'success': True,
            'message': 'Kontak berhasil ditambahkan.',
            'data': {
                'contact': result
            }
        }), 200


    '''
    @method DELETE
    @endpoint /v1/contacts/<int:contact_id>
    @permission jwt_required
    @return deleted contact object
    '''  

    delete_args = {
        'only': fields.DelimitedList(fields.Str(), missing=None),
        'exclude': fields.DelimitedList(fields.Str(), missing=None)
    }

    @cross_origin()
    @jwt_required
    @current_user()
    @ownership()
    @permission_required('contact', 'deleteown')
    @use_args(delete_args, locations=['json'])
    def delete(self, payload, contact_id):

        ''' Delete Contact '''  

        contact_id = reverse_id(contact_id)
        contact = ContactModel.get_by(contact_id, g.owner_id)

        if not contact:
            return jsonify({
                'success': False,
                'errors': {
                    'code': 404,
                    'messages': {
                        'contact': ['Kontak tidak ditemukan atau sudah dihapus.']
                    }
                }
            }), 404

        schema = ContactSchema(only=payload['only'], exclude=payload['exclude'])
        result = schema.dump(contact).data

        if contact.isdeleted:
            db.session.delete(contact)
        else:
            contact.isdeleted = True

        db.session.commit()

        return jsonify({
            'success': True,
            'message': 'Kontak berhasil dihapus dan tidak bisa dikembalikan.',
            'data': {
                'contact': result
            }
        }), 200


api.add_resource(Contact, '/contacts/<int:contact_id>')