import nexmo
import random
import requests
import json

from flask import current_app, render_template
from flask_mail import Message

from api import mail, celery

from api.v1.utils import (
    normalize_phone_number
)

def exponential_backoff(task_self):
    minutes = task_self.default_retry_delay / 60
    rand = random.uniform(minutes, minutes * 1.3)
    return int(rand ** task_self.request.retries) * 60

'''
Send Email Task Handler
'''

@celery.task(name='RingSatu Email Task', bind=True, max_retries=5, acks_late=True, default_retry_delay=10)
def sendemail_queue(self, *args, **kwargs):

    message = args if args else kwargs

    if type(message) == tuple:
        message = message[0]

    email = Message(message["subject"], sender=("RingSatu", "robot@ringsatu.id"), recipients=[message["to"]])
    email.body = render_template(message["template"] + '.txt', message=message["variables"])
    email.html = render_template(message["template"] + '.html', message=message["variables"])

    if 'attachment' in message['variables']:
        if message['variables']['attachment']:
            email.attach(message['variables']['attachment'], 'application/octect-stream', open(message['variables']['attachment'], 'rb').read())

    try:
        mail.send(email)
    except Exception as e:
        self.retry(exc=e, countdown=exponential_backoff(self))



'''
Send SMS Task Handler
'''

@celery.task(name='RingSatu SMS Task', bind=True, max_retries=5, acks_late=True, default_retry_delay=10)
def sendsms_queue(self, message):
    client = nexmo.Client(
        key=current_app.config.get('NEXMO_API_KEY'),
        secret=current_app.config.get('NEXMO_SECRET_KEY')
    )

    phone = normalize_phone_number(message["phonenumber"]) if message["phonenumber"] else None

    if phone:
        try:
            resp = client.send_message({
                'from': 'RingSatu App',
                'to': phone.encode("UTF-8"),
                'text': message["content"].encode("UTF-8"),
                'ttl': 120000
            })

            resp = resp['messages'][0]
            if resp['status'] == '0':
                return dict(success=True, message=dict(message_id=resp['message-id'], balance=resp["remaining-balance"]))
            else:
                return dict(success=False, message=resp["error-text"])
        except Exception as e:
            self.retry(exc=e, countdown=exponential_backoff(self))


'''
Send Notif Task Handler
'''

@celery.task(name='RingSatu Notif Task', bind=True, max_retries=5, acks_late=True, default_retry_delay=10)
def sendnotif_queue(self, *args, **kwargs):

    message = args if args else kwargs

    if type(message) == tuple:
        message = message[0]
    
    header = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Basic " + current_app.config.get('ONESIGNAL_APIKEY')
    }

    payload = {
        "app_id": current_app.config.get('ONESIGNAL_APPID'),
        "include_player_ids": message['player_ids'],
        "headings": {
            "en": message['title']
        },
        "contents": {
            "en": message['body']
        },
        "android_channel_id": "2ab91fbe-3abd-4301-a967-e06c1e92a9ab",
        "small_icon": "icon",
        "large_icon": "icon",
        "android_sound": "definite"
    }

    try:
        req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

        return {
            'status': req.status_code,
            'reason': req.reason
        }
    except Exception as e:
        self.retry(exc=e, countdown=exponential_backoff(self))

    

