import os
import time

from datetime import timedelta, datetime
from flask import current_app
from api import (
    db,
    scheduler
)
from api.v1.models.histories import (
    HistoryModel
)
from api.v1.models.media import (
    MediaModel
)
from api.v1.models.users import (
    RoleModel,
    PermissionModel,
    UserModel,
    ProfileModel,
    BankAccountModel
)
from api.v1.models.settings import (
    CategoryModel,
    BankModel, 
    UnitModel,
    PaymentMethodModel,
    RegionModel,
    ContactModel
)
from api.v1.models.residentials import (
    ResidentialModel,
    PanicModel,
    PanicActionModel,
    PanicReportModel,
    CCTVModel,
    AnnouncementModel,
    ResidentReportModel,
    FinancialReportModel,
    InventoryModel,
    InventoryMovementModel
)
from api.v1.models.marketplace import (
    ProductModel,
    OrderModel,
    OrderDetailModel
)
from api.v1.utils import(
    is_file_exist,
    is_dir_exist
)


def delete_tmp_media():

    with scheduler.app.app_context():
        medias = MediaModel.query.filter(MediaModel.isorphan==True, MediaModel.expired <= int(time.time())).limit(100).all()

        if medias:
            has_deleted = False
            for media in medias:
                if is_file_exist(os.path.join(current_app.config.get('TMP_FOLDER'), media.filename)):
                    os.remove(os.path.join(current_app.config.get('TMP_FOLDER'), media.filename))
                    db.session.delete(media)
                    db.session.commit()