import datetime
import time

from functools import update_wrapper, wraps
from flask import request, jsonify, g
from flask_jwt_extended import get_jwt_identity
from flask_restful import abort

from api.v1.models.users import UserModel
from api.v1.utils import reverse_id

def anonymous_required():
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if 'Authorization' in request.headers:
                return jsonify({
                    'errors': {
                        'code': 403,
                        'messages': {
                            'anonymous_required': ['Anda tidak memiliki izin untuk mengakses sumber daya ini.']
                        }
                    }
                }), 403
            return f(*args, **kwargs)
        return decorated_function
    return decorator


def current_user():
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            id = reverse_id(get_jwt_identity())
            user = UserModel.query.get(id)
            if user is None:
                return jsonify({
                    'errors': {
                        'code': 401,
                        'messages': {
                            'authorization': [ 'Anda tidak memiliki otorisasi untuk melanjutkan permintaan ini.']
                        }
                    }
                }), 401
            g.current_user = user
            return f(* args, **kwargs)
        return decorated_function
    return decorator


def ownership():
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            id = reverse_id(get_jwt_identity())
            user = UserModel.query.get(id)
            if user is None:
                return jsonify({
                    'errors': {
                        'code': 401,
                        'messages': {
                            'authorization': [ 'Anda tidak memiliki otorisasi untuk melanjutkan permintaan ini.']
                        }
                    }
                }), 401

            owner_id = None

            if user.role.slug != 'root':
                owner_id = user.parent_id if user.parent_id else None

            g.owner_id = owner_id

            return f(* args, **kwargs)
        return decorated_function
    return decorator


def permission_required(permission, action):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            id = reverse_id(get_jwt_identity())
            user = UserModel.query.get(id)
            if user and not user.check_permission(permission, action):
                return jsonify({
                    'errors': {
                        'code': 403,
                        'messages': {
                            'permission_required': ['Anda tidak memiliki izin untuk mengakses sumber daya ini.']
                        }
                    }
                }), 403
            return f(*args, **kwargs)
        return decorated_function
    return decorator