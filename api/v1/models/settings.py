import datetime
import arrow

from collections import OrderedDict
from sqlalchemy.sql.expression import and_, func, or_
from flask_sqlalchemy_caching import FromCache, RelationshipCache

from api import db, cache
from api.v1.models.histories import History
from api.v1.utils import reverse_id


class CategoryModel(db.Model):

    MODULE = OrderedDict([('announcement', 'Announcement'), ('residentreport', 'Resident Report'), ('financialreport', 'Financial Report'), ('marketplace', 'Marketplace'), ('inventory', 'Inventory')])

    __tablename__ = 'ringsatu_categories'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    module = db.Column(db.Enum(*MODULE, name="category_module"), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    path = db.Column(db.String(255), nullable=False)
    icon = db.Column(db.String(255), nullable=True)
    description = db.Column(db.String(255), nullable=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('ringsatu_categories.id'), index=True, nullable=True)
    childs = db.relationship('CategoryModel', backref=db.backref('parent', remote_side=[id]), lazy="joined", join_depth=1, cascade="all, delete-orphan", order_by='CategoryModel.name')
    parents = db.relationship('CategoryModel', back_populates='childs', uselist=False, remote_side=[id])
    isdeleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    media = db.relationship('MediaModel', backref='media_category', uselist=False, cascade='all, delete-orphan')    
    announcements = db.relationship('AnnouncementModel', backref='announcement_category', lazy='dynamic', cascade='all, delete-orphan')
    resident_reports = db.relationship('ResidentReportModel', backref='resident_report_category', lazy='dynamic', cascade='all, delete-orphan')
    financial_reports = db.relationship('FinancialReportModel', backref='financial_report_category', lazy='dynamic', cascade='all, delete-orphan')
    products = db.relationship('ProductModel', backref='product_category', lazy='dynamic', cascade='all, delete-orphan')
    inventory = db.relationship('InventoryModel', backref='inventory_category', lazy='dynamic', cascade='all, delete-orphan')
    author = db.relationship('UserModel', backref='author_categories', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_categories', foreign_keys=[owner_id])

    def __init__(self, **kwargs):
        super(CategoryModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Category %r, %r, %r>' % (self.id, self.module, self.name)

    @classmethod
    def count_all(cls, q=None, module=None, parent_id=0, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if module:
            query = query.filter(cls.module==module)

        if parent_id and parent_id != 0:
            query = query.filter(cls.parent_id==parent_id)
        elif parent_id == 0:
            query = query.filter(cls.parent_id==None)

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        cached_query = query.options(FromCache(cache))
        return query.count()


    @classmethod
    def get_all(cls, q=None, module=None, parent_id=0, owner_id=False, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if module:
            query = query.filter(cls.module==module)

        if parent_id and parent_id != 0:
            query = query.filter(cls.parent_id==parent_id)
        elif parent_id == 0:
            query = query.filter(cls.parent_id==None)

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))
        
        query = query.order_by(cls.module.asc()).limit(perpage).offset(offset)
        
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class BankModel(db.Model):

    __tablename__ = 'ringsatu_banks'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    code = db.Column(db.String(6), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    isdeleted = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    bank_accounts = db.relationship('BankAccountModel', backref='bank_account_bank', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(BankModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Bank %r, %r, %r>' % (self.id, self.code, self.name)

    @classmethod
    def count_all(cls, q=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(or_(cls.code.like('%'+q+'%'), cls.name.like('%'+q+'%')))

        cached_query = query.options(FromCache(cache))
        return query.count()


    @classmethod
    def get_all(cls, q=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(or_(cls.code.like('%'+q+'%'), cls.name.like('%'+q+'%')))

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        
        query = cls.query

        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)


class RegionModel(db.Model):

    __tablename__ = 'ringsatu_regions'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    path = db.Column(db.String(255), nullable=False)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    polygon = db.Column(db.Text, nullable=True)
    postal = db.Column(db.String(10), nullable=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('ringsatu_regions.id'), index=True, nullable=True)
    isdeleted = db.Column(db.Boolean, default=False)
    childs = db.relationship('RegionModel', backref=db.backref('parent', remote_side=[id]), lazy="joined", join_depth=1, cascade="all, delete-orphan", order_by='RegionModel.name')
    parents = db.relationship('RegionModel', back_populates='childs', uselist=False, remote_side=[id])
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    contacts = db.relationship('ContactModel', backref='contact_region', cascade='all, delete-orphan')
    residentials = db.relationship('ResidentialModel', backref='residential_region', cascade='all, delete-orphan')
    inventory_movements = db.relationship('InventoryMovementModel', backref='inventory_movement_region', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(RegionModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Region %r, %r, %r>' % (self.id, self.name, self.path)

    @classmethod
    def count_all(cls, q=None, parent_id=None, administrative=False, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(or_(cls.name.like('%'+q+'%'), cls.postal.like('%'+q+'%')))

        if parent_id is None and administrative == True:
            query = query.filter(cls.parent_id==parent_id)
        elif parent_id and isinstance(parent_id, int):
            parent_id = reverse_id(parent_id)
            query = query.filter(cls.parent_id==parent_id)

        cached_query = query.options(FromCache(cache))
        return query.count()


    @classmethod
    def get_all(cls, q=None, parent_id=None, administrative=False, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(or_(cls.name.like('%'+q+'%'), cls.postal.like('%'+q+'%')))

        if parent_id is None and administrative == True:
            query = query.filter(cls.parent_id==parent_id).order_by(cls.name.asc())
        elif parent_id and isinstance(parent_id, int):
            parent_id = reverse_id(parent_id)
            query = query.filter(cls.parent_id==parent_id).order_by(cls.name.asc())
        else:
            query = query.order_by(cls.name.asc()).limit(perpage).offset(offset)
        
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        
        query = cls.query
        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)


class PaymentMethodModel(db.Model):

    __tablename__ = 'ringsatu_payment_methods'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(45), nullable=False)
    slug = db.Column(db.String(45), nullable=False)
    charge = db.Column(db.Integer, default=0)
    charge_percent = db.Column(db.Integer, default=0)
    fee = db.Column(db.Integer, default=0)
    fee_percent = db.Column(db.Integer, default=0)
    description = db.Column(db.String(255), nullable=True)
    isdeleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    author = db.relationship('UserModel', backref='author_payment_methods', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_payment_methods', foreign_keys=[owner_id])

    def __init__(self, **kwargs):
        super(PaymentMethodModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<PaymentMethod %r, %r>' % (self.id, self.name)

    @classmethod
    def count_all(cls, q=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        cached_query = query.options(FromCache(cache))
        return query.count()


    @classmethod
    def get_all(cls, q=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class UnitModel(db.Model):

    __tablename__ = 'ringsatu_units'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    symbol = db.Column(db.String(10), nullable=True)
    description = db.Column(db.String(255), nullable=True)
    isdeleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    products = db.relationship('ProductModel', backref='product_unit', lazy='dynamic', cascade='all, delete-orphan')
    author = db.relationship('UserModel', backref='author_units', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_units', foreign_keys=[owner_id])

    def __init__(self, **kwargs):
        super(UnitModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Unit %r, %r, %r>' % (self.id, self.name, self.slug)

    @classmethod
    def count_all(cls, q=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        cached_query = query.options(FromCache(cache))
        return query.count()


    @classmethod
    def get_all(cls, q=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)
        
        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class ContactModel(db.Model):

    __tablename__ = 'ringsatu_contacts'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    fullname = db.Column(db.String(255), nullable=False)    
    address = db.Column(db.String(255), nullable=False)
    phone = db.Column(db.String(45), nullable=False)
    alt_phone = db.Column(db.String(45), nullable=True)
    email = db.Column(db.String(45), nullable=True)
    description = db.Column(db.String(255), nullable=True)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    isdeleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), index=True, nullable=True)
    region_id = db.Column(db.Integer, db.ForeignKey('ringsatu_regions.id'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    author = db.relationship('UserModel', backref='author_informations', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_informations', foreign_keys=[owner_id])

    def __init__(self, **kwargs):
        super(ContactModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Contact %r, %r, %r>' % (self.id, self.name, self.phone)

    @classmethod
    def count_all(cls, q=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(or_(cls.title.like('%'+q+'%'), cls.fullname.like('%'+q+'%'), cls.phone.like('%'+q+'%'), cls.alt_phone.like('%'+q+'%')))

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        cached_query = query.options(FromCache(cache))
        return query.count()


    @classmethod
    def get_all(cls, q=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(or_(cls.title.like('%'+q+'%'), cls.fullname.like('%'+q+'%'), cls.phone.like('%'+q+'%'), cls.alt_phone.like('%'+q+'%')))

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        query = query.order_by(cls.title.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)
        
        cached_query = query.options(FromCache(cache))
        return cached_query.first()


History(CategoryModel)
History(BankModel)
History(RegionModel)
History(PaymentMethodModel)
History(UnitModel)
History(ContactModel)