import base64
import binascii
import datetime
import hashlib
import hmac
import json
import os
import re
import time
from collections import OrderedDict

import arrow
import bleach
from flask import current_app
from slugify import slugify
from sqlalchemy import asc, desc
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy_utils import JSONType
from sqlalchemy.sql.expression import and_, func, or_

from flask_sqlalchemy_caching import FromCache, RelationshipCache

from api import bcrypt, cache, db
from api.v1.models.histories import HistoryModel, History
from api.v1.models.settings import BankModel
from api.v1.models.residentials import ResidentialModel
from api.v1.tasks import sendemail_queue, sendsms_queue
from api.v1.utils import generate_sms_token, get_clients_ip, reverse_id


class PermissionModel(db.Model):
    
    __tablename__ = 'ringsatu_permissions'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    shared = db.Column(db.Boolean, default=False)
    isdeleted = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    def __init__(self, **kwargs):
        super(PermissionModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Permission %r, %r>' % (self.id, self.name)

    @classmethod
    def count_all(cls, q=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        cached_query = query.options(FromCache(cache))
        return query.count()


    @classmethod
    def get_all(cls, q=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        
        query = cls.query

        cached_query = query.options(FromCache(cache))
        return cached_query.get(id)


class RoleModel(db.Model):
    
    __tablename__ = "ringsatu_roles"

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    permissions = db.Column(MutableDict.as_mutable(JSONType), nullable=False)
    isdeleted = db.Column(db.Boolean, default=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    owner = db.relationship('UserModel', backref=db.backref('owner_role', uselist=False), foreign_keys=[owner_id])
    actions = db.relationship('PanicActionModel', backref='action_role', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(RoleModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Role %r, %r>' % (self.id, self.name)


    @classmethod
    def count_all(cls, q=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if owner_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class UserModel(db.Model):
    
    USER_STATUS = OrderedDict([('inactive', 'Inactive'), ('active', 'Active'), ('banned', 'Banned')])
    RELATIONSHIP_STATUS = OrderedDict([('husband', 'Husband'), ('wife', 'Wife'), ('children', 'Children'), ('parents', 'Parents'), ('sibling', 'Sibling'), ('cousin', 'cousin'), ('nephew', 'Nephew'), ('assistant', 'Assistant')])

    __tablename__ = "ringsatu_users"

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    nid = db.Column(db.String(128), nullable=True)
    phone = db.Column(db.String(25), index=True, unique=True, nullable=False)
    passkey = db.Column(db.String(6), nullable=True)
    passkey_expire = db.Column(db.Integer, nullable=True)
    verification_code = db.Column(db.String(128), nullable=True)
    status = db.Column(db.Enum(*USER_STATUS, name="user_status"), index=True, default='inactive')
    relationship_status = db.Column(db.Enum(*RELATIONSHIP_STATUS, name="relationship_status"), index=True, nullable=True)
    isonline = db.Column(db.Boolean, default=False)
    isdeleted = db.Column(db.Boolean, default=False)         
    parent_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), index=True, nullable=True)
    role_id = db.Column(db.Integer, db.ForeignKey('ringsatu_roles.id'), index=True, nullable=False)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', use_alter=True, name='fk_residential_id', ondelete='CASCADE'))    
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    childs = db.relationship('UserModel', backref=db.backref('parent', remote_side=[id]), lazy="joined", join_depth=1, cascade="all, delete-orphan", order_by='UserModel.id')
    parents = db.relationship('UserModel', back_populates='childs', uselist=False, remote_side=[id])
    profile = db.relationship('ProfileModel', backref='profile_user', uselist=False, cascade='all, delete-orphan')
    products = db.relationship('ProductModel', backref='product_user', lazy='dynamic', cascade='all, delete-orphan')
    role = db.relationship('RoleModel', backref='role_users', foreign_keys=[role_id])
    residential = db.relationship('ResidentialModel', backref="users", foreign_keys=residential_id, post_update=True, cascade='all, delete')
    infos = db.relationship('InfoModel', backref='info_user', lazy='dynamic', cascade='all, delete-orphan')
    comments = db.relationship('CommentModel', backref='comment_user', lazy='dynamic', cascade='all, delete-orphan')
    
    
    def __init__(self, **kwargs):
        super(UserModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<User %r, %r>' % (self.id, self.phone)

    def check_permission(self, key, value):
        permissions = self.role.permissions
        if key in permissions and permissions[key][value]:
            return True
        return False

    def verify_me(self, vtype='login', params={}):

        nextminutes = datetime.datetime.now() + datetime.timedelta(minutes=5)
        passkey_expire = time.mktime(nextminutes.timetuple())
        passkey = generate_sms_token(self.phone)

        self.passkey = passkey
        self.passkey_expire = passkey_expire

        message = str(time.time()*1000) + self.phone + get_clients_ip() + binascii.b2a_hex(os.urandom(100))
        signature = hmac.new(current_app.config.get('SECRET_KEY'), message, hashlib.sha256).hexdigest()

        self.verification_code = signature

        payload = {
            'module': 'user',
            'type': vtype,
            'signature': signature,
            'expire': passkey_expire,
            'params': params
        }

        verification_code = base64.urlsafe_b64encode(json.dumps(payload))
        db.session.commit()

       # sendsms_queue.delay({
       #   "phonenumber":self.profile.phone,
       #   "content":'Kode RINGSATU anda adalah ' + passkey + ', berlaku sampai dengan 5 menit.'
       # })

        return {
            'code': verification_code,
            'expired': passkey_expire
        }


    @classmethod
    def count_all(cls, owner, q=None, user_status=None, residential_id=None, role_id=None, parent_id=None, isonline=False, isdeleted=False):
        
        query = cls.query.join(ProfileModel).filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(or_(cls.phone.like('%'+q+'%'), ProfileModel.fullname.like('%'+q+'%'), ProfileModel.store_name.like('%'+q+'%')))

        if residential_id:
            query = query.filter(ResidentialModel.id==residential_id)

        if owner == 'root' and parent_id:
            query = query.filter(cls.parent_id==parent_id)
        elif owner != 'root':
            query = query.filter(or_(cls.id==owner, cls.parent_id==owner))

        if user_status:
            query = query.filter(cls.status==user_status)        

        if role_id:
            role = RoleModel.query.get(role_id)

            if role:
                query = query.filter(cls.role_id==role_id)

        if isonline:
            query = query.filter(cls.isonline==isonline) 

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, owner, q=None, user_status=None, residential_id=None, role_id=None, parent_id=None, isonline=False, isdeleted=False, page=1, perpage=25):

        offset = (page - 1) * perpage
        
        query = cls.query.join(ProfileModel).filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(or_(cls.phone.like('%'+q+'%'), ProfileModel.fullname.like('%'+q+'%'), ProfileModel.store_name.like('%'+q+'%')))

        if residential_id:
            query = query.filter(ResidentialModel.id==residential_id)

        if owner == 'root' and parent_id:
            query = query.filter(cls.parent_id==parent_id)
        elif owner != 'root':
            query = query.filter(or_(cls.id==owner, cls.parent_id==owner))

        if user_status:
            query = query.filter(cls.status==user_status)        

        if role_id:
            role = RoleModel.query.get(role_id)

            if role:
                query = query.filter(cls.role_id==role_id)

        if isonline:
            query = query.filter(cls.isonline==isonline) 

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_many_by_phone(cls, phone):

        query = cls.query.join(ProfileModel).filter(cls.phone==phone, ProfileModel.invitable==True).order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id=None, phone=None, parent_id=None, residential_id=None, invitable=None):

        query = cls.query.join(ProfileModel)

        if id:
            query = query.filter(cls.id==id)

        if parent_id:
            query = query.filter(cls.parent_id==parent_id)

        if residential_id:
            query = query.filter(ResidentialModel.id==residential_id)

        if phone:
            query = query.filter(cls.phone==phone)

        if invitable:
            query = query.filter(ProfileModel.invitable==invitable)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class ProfileModel(db.Model):

    GENDER = OrderedDict([('m', 'Male'), ('f', 'Female')])
    IDCARD = OrderedDict([('ktp', 'KTP'), ('sim', 'SIM'), ('passport', 'Passport'), ('badge', 'Badge')])
    RESIDENTIAL_STATUS = OrderedDict([('rent', 'Rent'), ('owner', 'Owner'), ('staffing', 'Staffing')])

    __tablename__ = 'ringsatu_user_profiles'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True) 
    fullname = db.Column(db.String(255), nullable=False)
    birthday = db.Column(db.Date, nullable=True)
    gender = db.Column(db.Enum(*GENDER, name="user_gender"), nullable=True) 
    phone = db.Column(db.String(25), nullable=False)
    address = db.Column(db.String(255), nullable=True)
    idcard = db.Column(db.Enum(*IDCARD, name="idcard_type"), nullable=True)
    idcard_number = db.Column(db.String(45), nullable=True)
    residential_status = db.Column(db.Enum(*RESIDENTIAL_STATUS, name="residential_status"), nullable=True)
    landlord_name = db.Column(db.String(45), nullable=True)
    landlord_phone = db.Column(db.String(25), nullable=True)
    landlord_address = db.Column(db.String(255), nullable=True) 
    store_name = db.Column(db.String(255), nullable=True) 
    store_about = db.Column(db.String(255), nullable=True) 
    store_open = db.Column(db.String(5), nullable=True) 
    store_close = db.Column(db.String(5), nullable=True) 
    invitable = db.Column(db.Boolean, default=False)
    balance = db.Column(db.Integer, default=0)
    point = db.Column(db.Integer, default=0)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    profile_region_id = db.Column(db.Integer, db.ForeignKey('ringsatu_regions.id', ondelete='CASCADE'), index=True, nullable=True)
    landlord_region_id = db.Column(db.Integer, db.ForeignKey('ringsatu_regions.id', ondelete='CASCADE'), index=True, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    media = db.relationship('MediaModel', backref='media_profile', uselist=False, cascade='all, delete-orphan')
    profile_region = db.relationship('RegionModel', backref=db.backref('profile_administrative_region', uselist=False), foreign_keys=[profile_region_id])
    landlord_region = db.relationship('RegionModel', backref=db.backref('landlord_administrative_region', uselist=False), foreign_keys=[landlord_region_id])

    def __init__(self, **kwargs):
        super(ProfileModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Profile %r, %r, %r>' % (self.id, self.fullname, self.phone)


class BankAccountModel(db.Model):

    __tablename__ = 'ringsatu_user_banks'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True) 
    account_name = db.Column(db.String(255), nullable=False)
    account_number = db.Column(db.String(255), nullable=False)
    branch = db.Column(db.String(255), nullable=True)
    description = db.Column(db.String(255), nullable=True)      
    isdeleted = db.Column(db.Boolean, default=False)
    bank_id = db.Column(db.Integer, db.ForeignKey('ringsatu_banks.id', ondelete='CASCADE'), index=True, nullable=False)
    holder_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='CASCADE'), index=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    holder = db.relationship('UserModel', backref='holder_banks', foreign_keys=[holder_id])
    owner = db.relationship('UserModel', backref='owner_banks', foreign_keys=[owner_id])


    def __init__(self, **kwargs):
        super(BankAccountModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<BankAccount %r, %r, %r>' % (self.id, self.account_name, self.account_number)

    @classmethod
    def count_all(cls, q=None, bank_id=None, owner_id=None, residential_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if bank_id:
            query = query.filter(cls.bank_id==bank_id)  

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)   

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)              

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, bank_id=None, residential_id=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if bank_id:
            query = query.filter(cls.bank_id==bank_id)  

        if residential_id:
            query = query.filter(cls.residential_id==residential_id) 

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)  

        query = query.order_by(cls.name.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, residential_id=None, owner_id=None):

        query = cls.query.filter(id=id)
        
        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class UserNotificationModel(db.Model):

    __tablename__ = 'ringsatu_user_notifications'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True) 
    nid = db.Column(db.String(255), nullable=True)
    title = db.Column(db.String(255), nullable=False)
    body = db.Column(db.String(255), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='CASCADE'), index=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    user = db.relationship('UserModel', backref='user_notitication', foreign_keys=[user_id])


    def __init__(self, **kwargs):
        super(UserNotificationModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<UserNotification %r, %r, %r>' % (self.id, self.title)

    @classmethod
    def count_all(cls, q=None, user_id=None, residential_id=None):
        
        query = cls.query

        if q:
            query = query.filter(cls.title.like('%'+q+'%'))

        if user_id:
            query = query.filter(cls.user_id==user_id)  

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)               

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, user_id=None, residential_id=None, page=1, perpage=25):
        
        query = cls.query

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.title.like('%'+q+'%'))

        if user_id:
            query = query.filter(cls.user_id==user_id)  

        if residential_id:
            query = query.filter(cls.residential_id==residential_id) 

        query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, residential_id=None):
        
        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


History(PermissionModel)
History(RoleModel)
History(UserModel)
History(BankAccountModel)
History(UserNotificationModel)
