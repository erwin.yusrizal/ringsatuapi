import datetime

from sqlalchemy.sql.expression import and_, func, or_
from flask_sqlalchemy_caching import FromCache, RelationshipCache

from api import db, cache
from api.v1.models.histories import History

class MediaModel(db.Model):
    
    __tablename__ = 'ringsatu_media'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    filename = db.Column(db.String(255), nullable=False)
    original_filename = db.Column(db.String(255), nullable=False)
    size = db.Column(db.String(45), nullable=False)
    mimetype = db.Column(db.String(45), nullable=False)
    ismain = db.Column(db.Boolean, default=False)
    isorphan = db.Column(db.Boolean, default=False)
    expired = db.Column(db.Integer)
    isdeleted = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=True)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='CASCADE'), index=True, nullable=True)
    profile_id = db.Column(db.Integer, db.ForeignKey('ringsatu_user_profiles.id', ondelete='CASCADE'), index=True, nullable=True)
    announcement_id = db.Column(db.Integer, db.ForeignKey('ringsatu_announcements.id', ondelete='CASCADE'), index=True, nullable=True)
    resident_report_id = db.Column(db.Integer, db.ForeignKey('ringsatu_resident_reports.id', ondelete='CASCADE'), index=True, nullable=True)
    financial_report_id = db.Column(db.Integer, db.ForeignKey('ringsatu_financial_reports.id', ondelete='CASCADE'), index=True, nullable=True)
    inventory_id = db.Column(db.Integer, db.ForeignKey('ringsatu_inventories.id', ondelete='CASCADE'), index=True, nullable=True)
    product_id = db.Column(db.Integer, db.ForeignKey('ringsatu_products.id', ondelete='CASCADE'), index=True, nullable=True)
    category_id = db.Column(db.Integer, db.ForeignKey('ringsatu_categories.id', ondelete='CASCADE'), index=True, nullable=True)
    profile = db.relationship('ProfileModel', backref=db.backref('profile_media', uselist=False), foreign_keys=[profile_id])
    info_id = db.Column(db.Integer, db.ForeignKey('ringsatu_info.id', ondelete='CASCADE'), index=True, nullable=True)
    comment_id = db.Column(db.Integer, db.ForeignKey('ringsatu_comments.id', ondelete='CASCADE'), index=True, nullable=True)
    message_id = db.Column(db.Integer, db.ForeignKey('ringsatu_chat_messages.id', ondelete='CASCADE'), index=True, nullable=True)
    panic_id = db.Column(db.Integer, db.ForeignKey('ringsatu_panics.id', ondelete='CASCADE'), index=True, nullable=True)
    freebies_id = db.Column(db.Integer, db.ForeignKey('ringsatu_freebies.id', ondelete='CASCADE'), index=True, nullable=True)
    owner = db.relationship('UserModel', backref=db.backref('owner_media', uselist=False), foreign_keys=[owner_id])

    def __init__(self, **kwargs):
        super(MediaModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Media %r, %r>' % (self.filename, self.mimetype)

    @classmethod
    def count_all(cls, residential_id=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return query.count()


    @classmethod
    def get_all(cls, residential_id=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        query = query.order_by(cls.name.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, residential_id=None, owner_id=None):

        query = cls.query.filter(cls.id==id)
        
        if residential_id:
            query = cls.query.filter(cls.residential_id==residential_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


History(MediaModel)