import datetime
import arrow
import math

from collections import OrderedDict
from sqlalchemy.sql.expression import and_, func, or_
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy_utils import JSONType
from sqlalchemy.ext.hybrid import hybrid_method
from flask_sqlalchemy_caching import FromCache, RelationshipCache

from api import db, cache
from api.v1.models.histories import History
from api.v1.utils import reverse_id

def gc_distance(lat1, lng1, lat2, lng2, math=math):
    ang = math.acos(math.cos(math.radians(lat1)) *
                    math.cos(math.radians(lat2)) *
                    math.cos(math.radians(lng2) -
                             math.radians(lng1)) +
                    math.sin(math.radians(lat1)) *
                    math.sin(math.radians(lat2)))

    return 6371 * ang


class FreebiesModel(db.Model):

    STATUS = OrderedDict([('draft', 'Draft'), ('publish', 'Publish')])

    __tablename__ = 'ringsatu_freebies'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=False)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    isdeleted = db.Column(db.Boolean, default=False)
    status = db.Column(db.Enum(*STATUS, name="freebies_status"), default='draft')
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    medias = db.relationship('MediaModel', backref='media_freebies', lazy='dynamic', cascade='all, delete-orphan')
    author = db.relationship('UserModel', backref='author_freebies', foreign_keys=[user_id])
    comments = db.relationship('CommentModel', backref='comment_freebies', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(FreebiesModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Freebies %r, %r, %r>' % (self.id, self.title, self.user_id)

    @hybrid_method
    def distance(cls, lat, lng):
        return gc_distance(lat, lng, cls.latitude, cls.longitude)

    @distance.expression
    def distance(cls, lat, lng):
        return gc_distance(lat, lng, cls.latitude, cls.longitude, math=func)

    @classmethod
    def count_all(cls, q=None, status=None, author_id=None, latitude=None, longitude=None, isdeleted=False):

        if latitude and longitude:
            subq = db.session.query(FreebiesModel.id, FreebiesModel.distance(latitude, longitude).label('distance')).subquery()
            query = db.session.query(FreebiesModel, subq)

            query = query.join(subq, FreebiesModel.id == subq.c.id)

            if q:
                query = query.filter(or_(FreebiesModel.title.like('%'+q+'%'), FreebiesModel.description.like('%'+q+'%')))        
            if status:
                query = query.filter(FreebiesModel.status==status)

            if author_id:
                query = query.filter(FreebiesModel.user_id==author_id)

            query = query.having(subq.c.distance < 50)
            

        else:
            query = cls.query.filter(cls.isdeleted==isdeleted)

            if q:
                query = query.filter(or_(cls.title.like('%'+q+'%'), cls.description.like('%'+q+'%')))        

            if status:
                query = query.filter(cls.status==status)

            if author_id:
                query = query.filter(cls.user_id==author_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, author_id=None, latitude=None, longitude=None, isdeleted=False, page=1, perpage=25):

        offset = (page - 1) * perpage

        if latitude and longitude:
            subq = db.session.query(FreebiesModel.id, FreebiesModel.distance(latitude, longitude).label('distance')).subquery()
            query = db.session.query(FreebiesModel, subq)

            query = query.join(subq, FreebiesModel.id == subq.c.id)

            if q:
                query = query.filter(or_(FreebiesModel.title.like('%'+q+'%'), FreebiesModel.description.like('%'+q+'%')))

            if status:
                query = query.filter(FreebiesModel.status==status)

            if author_id:
                query = query.filter(FreebiesModel.user_id==author_id)

            query = query.having(subq.c.distance < 50).order_by(subq.c.distance).limit(perpage).offset(offset)

        else:
        
            query = cls.query.filter(cls.isdeleted==isdeleted)            

            if q:
                query = query.filter(or_(cls.title.like('%'+q+'%'), cls.description.like('%'+q+'%')))

            if status:
                query = query.filter(cls.status==status)

            if author_id:
                query = query.filter(cls.user_id==author_id)

            query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, author_id=None):
        
        query = cls.query.filter(cls.id==id)

        if author_id:
            query = query.filter(cls.user_id==author_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()



class ProductModel(db.Model):

    STATUS = OrderedDict([('draft', 'Draft'), ('publish', 'Publish')])
    CONDITIONS = OrderedDict([('new', 'New'), ('used', 'Used')])

    __tablename__ = 'ringsatu_products'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    conditions = db.Column(db.Enum(*CONDITIONS, name="product_condition"), default='new')
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    minorder = db.Column(db.Integer, default=0)
    price = db.Column(db.Integer, default=0)
    discount = db.Column(db.Integer, default=0)
    isnegotiable = db.Column(db.Boolean, default=False)
    isdeliverable = db.Column(db.Boolean, default=False)
    delivery_cost = db.Column(db.Integer, default=0)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    isdeleted = db.Column(db.Boolean, default=False)
    status = db.Column(db.Enum(*STATUS, name="product_status"), default='draft')
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), nullable=True)
    category_id = db.Column(db.Integer, db.ForeignKey('ringsatu_categories.id'), nullable=True)
    unit_id = db.Column(db.Integer, db.ForeignKey('ringsatu_units.id'), nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    medias = db.relationship('MediaModel', backref='media_product', lazy='dynamic', cascade='all, delete-orphan')
    orders = db.relationship('OrderDetailModel', backref='order_product', lazy='dynamic', cascade='all, delete-orphan')
    author = db.relationship('UserModel', backref='author_product', foreign_keys=[user_id])
    comments = db.relationship('CommentModel', backref='comment_product', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(ProductModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Product %r, %r, %r>' % (self.id, self.title, self.price)

    @hybrid_method
    def distance(cls, lat, lng):
        return gc_distance(lat, lng, cls.latitude, cls.longitude)

    @distance.expression
    def distance(cls, lat, lng):
        return gc_distance(lat, lng, cls.latitude, cls.longitude, math=func)

    @classmethod
    def count_all(cls, q=None, status=None, category_id=None, author_id=None, latitude=None, longitude=None, isdeleted=False):

        if latitude and longitude:
            subq = db.session.query(ProductModel.id, ProductModel.distance(latitude, longitude).label('distance')).subquery()
            query = db.session.query(ProductModel, subq)

            query = query.join(subq, ProductModel.id == subq.c.id)

            if q:
                query = query.filter(or_(ProductModel.title.like('%'+q+'%'), ProductModel.description.like('%'+q+'%')))        
            if status:
                query = query.filter(ProductModel.status==status)

            if category_id:
                query = query.having(ProductModel.category_id==category_id)

            if author_id:
                query = query.filter(ProductModel.user_id==author_id)

            query = query.having(subq.c.distance < 50)
            

        else:
            query = cls.query.filter(cls.isdeleted==isdeleted)

            if q:
                query = query.filter(or_(cls.title.like('%'+q+'%'), cls.description.like('%'+q+'%')))        

            if status:
                query = query.filter(cls.status==status)

            if category_id:
                query = query.filter(cls.category_id==category_id)

            if author_id:
                query = query.filter(cls.user_id==author_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, category_id=None, author_id=None, latitude=None, longitude=None, isdeleted=False, page=1, perpage=25):

        offset = (page - 1) * perpage

        if latitude and longitude:
            subq = db.session.query(ProductModel.id, ProductModel.distance(latitude, longitude).label('distance')).subquery()
            query = db.session.query(ProductModel, subq)

            query = query.join(subq, ProductModel.id == subq.c.id)

            if q:
                query = query.filter(or_(ProductModel.title.like('%'+q+'%'), ProductModel.description.like('%'+q+'%')))

            if status:
                query = query.filter(ProductModel.status==status)

            if category_id:
                query = query.having(ProductModel.category_id==category_id)

            if author_id:
                query = query.filter(ProductModel.user_id==author_id)

            query = query.having(subq.c.distance < 50).order_by(subq.c.distance).limit(perpage).offset(offset)

        else:
        
            query = cls.query.filter(cls.isdeleted==isdeleted)            

            if q:
                query = query.filter(or_(cls.title.like('%'+q+'%'), cls.description.like('%'+q+'%')))

            if status:
                query = query.filter(cls.status==status)

            if category_id:
                query = query.filter(cls.category_id==category_id)

            if author_id:
                query = query.filter(cls.user_id==author_id)

            query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, author_id=None):
        
        query = cls.query.filter(cls.id==id)

        if author_id:
            query = query.filter(cls.user_id==author_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class OrderModel(db.Model):

    STATUS = OrderedDict([('expired', 'Expired'), ('waiting', 'Waiting'), ('approved', 'Approved'), ('rejected', 'Rejected'), ('ondelivery', 'On Delivery'), ('delivered', 'Delivered')])

    __tablename__ = 'ringsatu_orders'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    po = db.Column(db.String(16), nullable=False)
    expired = db.Column(db.Integer, nullable=False)
    reason = db.Column(db.String(255), nullable=False)
    isdeleted = db.Column(db.Boolean, default=False)
    status = db.Column(db.Enum(*STATUS, name="order_status"), default='waiting')
    buyer_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), nullable=False)
    seller_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    details = db.relationship('OrderDetailModel', backref='detail_order', lazy='dynamic', cascade='all, delete-orphan')
    buyer = db.relationship('UserModel', backref='buyer_orders', foreign_keys=[buyer_id])
    seller = db.relationship('UserModel', backref='seller_orders', foreign_keys=[seller_id])

    def __init__(self, **kwargs):
        super(OrderModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Order %r, %r, %r>' % (self.id, self.po, self.status)

    @classmethod
    def count_all(cls, q=None, status=None, buyer_id=None, seller_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.po.like('%'+q+'%'))

        if buyer_id:
            query = query.filter(cls.buyer_id==buyer_id)

        if seller_id:
            query = query.filter(cls.seller_id==seller_id)

        if status:
            query = query.filter(cls.status==status)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, buyer_id=None, seller_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.po.like('%'+q+'%'))

        if buyer_id:
            query = query.filter(cls.buyer_id==buyer_id)

        if seller_id:
            query = query.filter(cls.seller_id==seller_id)

        if status:
            query = query.filter(cls.status==status)

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, buyer_id=None, seller_id=None):
        
        query = cls.query.filter(cls.id==id)

        if buyer_id or seller_id:
            query = query.filter(or_(cls.buyer_id==buyer_id, cls.seller_id==seller_id))

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class OrderDetailModel(db.Model):

    __tablename__ = 'ringsatu_order_details'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    quantity = db.Column(db.Integer, default=0)
    price = db.Column(db.Integer, default=0)
    discount = db.Column(db.Integer, default=0)
    cost = db.Column(db.Integer, default=0)
    total = db.Column(db.Integer, default=0)
    note = db.Column(db.String(255), nullable=True)
    order_id = db.Column(db.Integer, db.ForeignKey('ringsatu_orders.id'), nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('ringsatu_products.id'), nullable=False)    
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    def __init__(self, **kwargs):
        super(OrderDetailModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<OrderDetail %r, %r, %r>' % (self.id, self.order_id, self.product_id)


History(ProductModel)
History(OrderModel)
History(FreebiesModel)