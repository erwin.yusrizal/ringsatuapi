import datetime
import arrow
import math

from collections import OrderedDict
from sqlalchemy import extract, case
from sqlalchemy.sql.expression import and_, func, or_
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.ext.hybrid import hybrid_method
from sqlalchemy_utils import JSONType
from flask_sqlalchemy_caching import FromCache, RelationshipCache

from api import db, cache
from api.v1.models.histories import History
from api.v1.models.settings import RegionModel
from api.v1.utils import reverse_id

def gc_distance(lat1, lng1, lat2, lng2, math=math):
    ang = math.acos(math.cos(math.radians(lat1)) *
                    math.cos(math.radians(lat2)) *
                    math.cos(math.radians(lng2) -
                             math.radians(lng1)) +
                    math.sin(math.radians(lat1)) *
                    math.sin(math.radians(lat2)))

    return 6371 * ang

residentials_announcements = db.Table('ringsatu_residentials_announcements',
    db.Column('residential_id', db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='cascade'), primary_key=True),
    db.Column('announcement_id', db.Integer, db.ForeignKey('ringsatu_announcements.id', ondelete='cascade'), primary_key=True)
)

residentials_panics = db.Table('ringsatu_residentials_panics',
    db.Column('residential_id', db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='cascade'), primary_key=True),
    db.Column('panic_id', db.Integer, db.ForeignKey('ringsatu_panics.id', ondelete='cascade'), primary_key=True)
)

class LiveLocationModel(db.Model):
    
    __tablename__ = 'ringsatu_live_locations'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    altitude = db.Column(db.Integer, nullable=True)
    accuracy = db.Column(db.Integer, nullable=True)
    speed = db.Column(db.Integer, nullable=True)
    time = db.Column(db.Integer, nullable=True)
    panic_id = db.Column(db.Integer, db.ForeignKey('ringsatu_panics.id', ondelete='CASCADE'), index=True, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    user = db.relationship('UserModel', foreign_keys=[user_id])

    def __init__(self, **kwargs):
        super(LiveLocationModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<LiveLocation %r, %r, %r>' % (self.id, self.latitude, self.longitude)

    @classmethod
    def get_all(cls, panic_id=None, user_id=None, page=1, perpage=25):
        
        query = cls.query.filter

        if panic_id:
            query = query.filter(cls.panic_id==panic_id)

        if user_id:
            query = query.filter(cls.user_id==user_id)

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


class ChatModel(db.Model):
    
    __tablename__ = 'ringsatu_chats'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    sender_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True)
    recipient_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True)      
    messages = db.relationship('ChatMessageModel', backref='chat', lazy='dynamic',  cascade='all, delete-orphan')
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    product_id = db.Column(db.Integer, db.ForeignKey('ringsatu_products.id', ondelete='CASCADE'), index=True)
    sender = db.relationship('UserModel', foreign_keys=[sender_id])
    recipient = db.relationship('UserModel', foreign_keys=[recipient_id]) 


    def __init__(self, **kwargs):
        super(ChatModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<ChatModel %r, %r>' % (self.sender.profile.fullname, self.recipient.profile.fullname)


    @classmethod
    def count_all(cls, sender_id=None, recipient_id=None):

        # query = cls.query.filter(case([(cls.sender_id == sender_id, cls.sender_id == sender_id),(cls.recipient_id == recipient_id, cls.recipient_id == recipient_id)]),((cls.sender_id == sender_id) | (cls.recipient_id == recipient_id)))

        query = cls.query

        if sender_id:
            query = query.filter(cls.sender_id == sender_id)
        else:
            query = query.filter(cls.recipient_id == recipient_id)
        
        cached_query = query.options(FromCache(cache))
        return cached_query.count()

    @classmethod
    def get_all(cls, sender_id=None, recipient_id=None, page=1, perpage=25):

        offset = (page - 1) * perpage

        # query = cls.query.filter(case([(cls.sender_id == sender_id, cls.sender_id == sender_id),(cls.recipient_id == recipient_id, cls.recipient_id == recipient_id)]),((cls.sender_id == sender_id) | (cls.recipient_id == recipient_id)))

        query = cls.query

        if sender_id:
            query = query.filter(cls.sender_id == sender_id)
        else:
            query = query.filter(cls.recipient_id == recipient_id)

        query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return cached_query.all()


class ChatMessageModel(db.Model):
    
    __tablename__ = 'ringsatu_chat_messages'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    message = db.Column(db.Text)   
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    mapurl = db.Column(db.Text, nullable=True) 
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True)
    chat_id = db.Column(db.Integer, db.ForeignKey('ringsatu_chats.id', ondelete='CASCADE'), index=True)   
    user = db.relationship('UserModel', foreign_keys=[user_id])
    medias = db.relationship('MediaModel', backref='media_message', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(ChatMessageModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<ChatMessageModel %r, %r>' % (self.chat.sender, self.chat.receiver)

    @classmethod
    def count_all(cls, chat_id):

        query = cls.query.filter(cls.chat_id == chat_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()

    @classmethod
    def get_all(cls, chat_id, page=1, perpage=25):

        offset = (page - 1) * perpage

        query = cls.query.filter(cls.chat_id == chat_id)
        query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return reversed(cached_query.all())


class ChatMessageStatusModel(db.Model):
    
    __tablename__ = 'ringsatu_chat_message_status'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True)
    message_id = db.Column(db.Integer, db.ForeignKey('ringsatu_chat_messages.id', ondelete='CASCADE'), index=True)
    read = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)      


    def __init__(self, **kwargs):
        super(ChatMessageStatusModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<ChatMessageStatusModel %r, %r>' % (self.read, self.chat.user_id)


class InfoModel(db.Model):

    __tablename__ = 'ringsatu_info'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    body = db.Column(db.Text, nullable=False)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    medias = db.relationship('MediaModel', backref='media_info', lazy='dynamic', cascade='all, delete-orphan')
    comments = db.relationship('CommentModel', backref='comment_info', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(InfoModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Info %r, %r, %r>' % (self.id, self.title, self.user_id)

    @hybrid_method
    def distance(cls, lat, lng):
        return gc_distance(lat, lng, cls.latitude, cls.longitude)

    @distance.expression
    def distance(cls, lat, lng):
        return gc_distance(lat, lng, cls.latitude, cls.longitude, math=func)

    @classmethod
    def count_all(cls, q=None, user_id=None, latitude=None, longitude=None):

        if latitude and longitude:
            subq = db.session.query(InfoModel.id, InfoModel.distance(latitude, longitude).label('distance')).subquery()
            query = db.session.query(InfoModel, subq)

            query = query.join(subq, InfoModel.id == subq.c.id)

            if q:
                query = query.filter(cls.title.like('%'+q+'%'))

            if user_id:
                query = query.filter(cls.user_id==user_id)

            query = query.having(subq.c.distance < 50)
        
        else:
            query = cls.query

            if q:
                query = query.filter(cls.title.like('%'+q+'%'))

            if user_id:
                query = query.filter(cls.user_id==user_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, user_id=None, latitude=None, longitude=None, page=1, perpage=25):
        
        offset = (page - 1) * perpage

        if latitude and longitude:
            subq = db.session.query(InfoModel.id, InfoModel.distance(latitude, longitude).label('distance')).subquery()
            query = db.session.query(InfoModel, subq)

            query = query.join(subq, InfoModel.id == subq.c.id)

            if q:
                query = query.filter(cls.title.like('%'+q+'%'))

            if user_id:
                query = query.filter(cls.user_id==user_id)

            query = query.having(subq.c.distance < 50).order_by(subq.c.distance).limit(perpage).offset(offset)

        else:
            query = cls.query

            if q:
                query = query.filter(cls.title.like('%'+q+'%'))

            if user_id:
                query = query.filter(cls.user_id==user_id)

            query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)

        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, user_id=None):
        
        query = cls.query.filter(cls.id==id)

        if user_id:
            query = query.filter(cls.user_id==user_id)
            
        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class CommentModel(db.Model):

    __tablename__ = 'ringsatu_comments'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    comment = db.Column(db.Text, nullable=False)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    mapurl = db.Column(db.Text, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    info_id = db.Column(db.Integer, db.ForeignKey('ringsatu_info.id', ondelete='CASCADE'), index=True, nullable=True)
    announcement_id = db.Column(db.Integer, db.ForeignKey('ringsatu_announcements.id', ondelete='CASCADE'), index=True, nullable=True)
    resident_report_id = db.Column(db.Integer, db.ForeignKey('ringsatu_resident_reports.id', ondelete='CASCADE'), index=True, nullable=True)
    product_id = db.Column(db.Integer, db.ForeignKey('ringsatu_products.id', ondelete='CASCADE'), index=True, nullable=True)
    panic_id = db.Column(db.Integer, db.ForeignKey('ringsatu_panics.id', ondelete='CASCADE'), index=True, nullable=True)
    freebies_id = db.Column(db.Integer, db.ForeignKey('ringsatu_freebies.id', ondelete='CASCADE'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    medias = db.relationship('MediaModel', backref='media_comment', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(CommentModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Comment %r, %r>' % (self.id, self.user_id)


    @classmethod
    def count_all(cls, q=None, user_id=None, info_id=None, announcement_id=None, resident_report_id=None, product_id=None, panic_id=None, freebies_id=None):
        
        query = cls.query

        if q:
            query = query.filter(cls.comment.like('%'+q+'%'))

        if user_id:
            query = query.filter(cls.user_id==user_id)

        if info_id:
            query = query.filter(cls.info_id==info_id)

        if announcement_id:
            query = query.filter(cls.announcement_id==announcement_id)

        if resident_report_id:
            query = query.filter(cls.resident_report_id==resident_report_id)

        if product_id:
            query = query.filter(cls.product_id==product_id)

        if panic_id:
            query = query.filter(cls.panic_id==panic_id)

        if freebies_id:
            query = query.filter(cls.freebies_id==freebies_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, user_id=None, info_id=None, announcement_id=None, resident_report_id=None, product_id=None, panic_id=None, freebies_id=None, page=1, perpage=25):
        
        query = cls.query

        offset = (page - 1) * perpage
        
        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if user_id:
            query = query.filter(cls.user_id==user_id)

        if info_id:
            query = query.filter(cls.info_id==info_id)

        if announcement_id:
            query = query.filter(cls.announcement_id==announcement_id)

        if resident_report_id:
            query = query.filter(cls.resident_report_id==resident_report_id)

        if product_id:
            query = query.filter(cls.product_id==product_id)

        if panic_id:
            query = query.filter(cls.panic_id==panic_id)

        if freebies_id:
            query = query.filter(cls.freebies_id==freebies_id)

        query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return reversed(cached_query.all())


    @classmethod
    def get_by(cls, id, user_id=None):
        
        query = cls.query.filter(cls.id==id)

        if user_id:
            query = query.filter(cls.user_id==user_id)
            
        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class SignedByModel(db.Model):

    __tablename__ = 'ringsatu_signedby'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    position = db.Column(db.String(255), nullable=False)
    announcement_id = db.Column(db.Integer, db.ForeignKey('ringsatu_announcements.id', ondelete='CASCADE'), index=True, nullable=True)
    financial_report_id = db.Column(db.Integer, db.ForeignKey('ringsatu_financial_reports.id', ondelete='CASCADE'), index=True, nullable=True)
    movement_id = db.Column(db.Integer, db.ForeignKey('ringsatu_inventory_movements.id', ondelete='CASCADE'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    def __init__(self, **kwargs):
        super(SignedByModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<SignedBy %r, %r, %r>' % (self.id, self.name, self.position)


class ResidentialModel(db.Model):

    __tablename__ = 'ringsatu_residentials'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    address = db.Column(db.String(255), nullable=False)    
    path = db.Column(db.String(255), nullable=False)
    polygon = db.Column(db.Text, nullable=True)
    note = db.Column(db.String(255), nullable=True)
    restricted = db.Column(db.Boolean, default=False)
    isdeleted = db.Column(db.Boolean, default=False)
    region_id = db.Column(db.Integer, db.ForeignKey('ringsatu_regions.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    medias = db.relationship('MediaModel', backref='media_residential', lazy='dynamic', cascade='all, delete-orphan')    
    bank_accounts = db.relationship('BankAccountModel', backref='bank_account_residential', lazy='dynamic', cascade='all, delete-orphan')
    cctv = db.relationship('CCTVModel', backref='cctv_residential', lazy='dynamic', cascade='all, delete-orphan')
    panics = db.relationship('PanicModel', secondary=residentials_panics, lazy='dynamic', backref=db.backref('panics_residentials', lazy='dynamic'))
    panic_reports = db.relationship('PanicReportModel', backref='panic_report_residential', lazy='dynamic', cascade='all, delete-orphan')
    announcements = db.relationship('AnnouncementModel', secondary=residentials_announcements, backref=db.backref('announcements_residentials', lazy='dynamic'))
    resident_reports = db.relationship('ResidentReportModel', backref='resident_report_residential', lazy='dynamic', cascade='all, delete-orphan')
    financial_reports = db.relationship('FinancialReportModel', backref='financial_report_residential', lazy='dynamic', cascade='all, delete-orphan')
    inventories = db.relationship('InventoryModel', backref='inventory_residential', lazy='dynamic', cascade='all, delete-orphan')
    movements = db.relationship('InventoryMovementModel', backref='inventory_movement_residential', lazy='dynamic', cascade='all, delete-orphan')
    owner = db.relationship('UserModel', foreign_keys=owner_id, backref='residentials')

    def __init__(self, **kwargs):
        super(ResidentialModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Residential %r, %r, %r>' % (self.id, self.name, self.region_id)

    @classmethod
    def count_all(cls, q=None, region_id=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        if region_id:
            region = RegionModel.query.get(region_id)
            if region:
                query = query.filter(cls.path.like(region.path+'%'))

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, region_id=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage
        
        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        if region_id:
            region = RegionModel.query.get(region_id)
            if region:
                query = query.filter(cls.path.like(region.path+'%'))

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)
            
        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class PanicModel(db.Model):

    PANIC_GROUP = OrderedDict([('community', 'Community'), ('family', 'Family'), ('friendship', 'Friendship'), ('work', 'Work'), ('residential', 'Residential')])

    __tablename__ = 'ringsatu_panics'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    panic_group = db.Column(db.Enum(*PANIC_GROUP, name="panic_group"), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    icon = db.Column(db.String(255), nullable=True)
    description = db.Column(db.String(255), nullable=True)
    restricted = db.Column(db.Boolean, default=False)
    isdeleted = db.Column(db.Boolean, default=False)
    live_location = db.Column(db.Boolean, default=False)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id'), index=True, nullable=True)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    reports = db.relationship('PanicReportModel', backref='panic_report_panic', lazy='dynamic', cascade='all, delete-orphan')
    author = db.relationship('UserModel', backref='author_panics', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_panics', foreign_keys=[owner_id])
    participants = db.relationship('PanicParticipantModel', backref='participant_panic', lazy='dynamic', cascade='all, delete-orphan')
    actions = db.relationship('PanicActionModel', backref='action_panic', lazy='dynamic', cascade='all, delete-orphan')
    reports = db.relationship('PanicReportModel', backref='report_panic', lazy='dynamic', cascade='all, delete-orphan')
    comments = db.relationship('CommentModel', backref='comment_panic', lazy='dynamic', cascade='all, delete-orphan')
    live_locations = db.relationship('LiveLocationModel', backref='live_location_panic', lazy='dynamic', cascade='all, delete-orphan')
    media = db.relationship('MediaModel', backref='media_panic', uselist=False, cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(PanicModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Panic %r, %r, %r>' % (self.id, self.name, self.isdeleted)

    @classmethod
    def count_all(cls, q=None, restricted=None, residential_id=None, owner_id=None, author_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if restricted:
            query = query.filter(cls.restricted==restricted)

        if residential_id:
            query = query.join(ResidentialModel.panics).filter(ResidentialModel.id==residential_id)

        if owner_id and not author_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        if author_id:
            query = query.filter(or_(cls.author_id==author_id, cls.participants.any(PanicParticipantModel.user_id == author_id)))

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, restricted=None, residential_id=None, owner_id=None, author_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if restricted:
            query = query.filter(cls.restricted==restricted)

        if residential_id:
            query = query.join(ResidentialModel.panics).filter(ResidentialModel.id==residential_id)

        if owner_id and not author_id:
            query = query.filter(or_(cls.owner_id==None, cls.owner_id==owner_id))

        if author_id:     
            query = query.filter(or_(cls.author_id==author_id, cls.participants.any(PanicParticipantModel.user_id == author_id)))

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class PanicParticipantModel(db.Model):

    STATUS = OrderedDict([('waiting', 'Waiting'), ('approved', 'Approved'), ('rejected', 'Rejected')])

    __tablename__ = 'ringsatu_panic_participants'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    panic_id = db.Column(db.Integer, db.ForeignKey('ringsatu_panics.id', ondelete='CASCADE'), index=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    status = db.Column(db.Enum(*STATUS, name="panic_participant_status"), default='waiting')
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    user = db.relationship('UserModel', backref='user_participant', foreign_keys=[user_id])

    def __init__(self, **kwargs):
        super(PanicParticipantModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<PanicParticipant %r, %r, %r>' % (self.id, self.panic_id, self.user_id)

    @classmethod
    def count_all(cls, panic_id=None, user_id=None):
        
        query = cls.query.filter

        if panic_id:
            query = query.filter(cls.panic_id==panic_id)

        if user_id:
            query = query.filter(cls.user_id==user_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, panic_id=None, user_id=None, page=1, perpage=25):
        
        query = cls.query.filter

        if panic_id:
            query = query.filter(cls.panic_id==panic_id)

        if user_id:
            query = query.filter(cls.user_id==user_id)

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id):
        
        query = cls.query.filter(cls.id==id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class PanicActionModel(db.Model):

    __tablename__ = 'ringsatu_panic_actions'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    action = db.Column(db.String(255), nullable=False)
    phone = db.Column(db.String(25), nullable=True)
    alt_phone = db.Column(db.String(25), nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    panic_id = db.Column(db.Integer, db.ForeignKey('ringsatu_panics.id', ondelete='CASCADE'), index=True, nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('ringsatu_roles.id', ondelete='CASCADE'), index=True, nullable=True)
    reports = db.relationship('PanicReportModel', backref='report_action', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(PanicActionModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<PanicAction %r, %r, %r>' % (self.id, self.action, self.phone)


class PanicReportModel(db.Model):

    STATUS = OrderedDict([('waiting', 'Waiting'), ('onprogress', 'On Progress'), ('done', 'Done')])

    __tablename__ = 'ringsatu_panic_reports'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    number = db.Column(db.String(45), nullable=False)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    mapurl = db.Column(db.Text, nullable=True)
    isdeleted = db.Column(db.Boolean, default=False)    
    performance = db.Column(db.Integer, default=0)
    note = db.Column(db.String(255), nullable=True)
    live_expired = db.Column(db.Integer, nullable=True)
    status = db.Column(db.Enum(*STATUS, name="panic_report_status"), default='waiting')
    reporter_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), index=True, nullable=True)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id'), index=True, nullable=True)
    panic_id = db.Column(db.Integer, db.ForeignKey('ringsatu_panics.id'), index=True, nullable=False)
    action_id = db.Column(db.Integer, db.ForeignKey('ringsatu_panic_actions.id'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    reporter = db.relationship('UserModel', backref='reporter_panic_reports', foreign_keys=[reporter_id])
    owner = db.relationship('UserModel', backref='owner_panic_reports', foreign_keys=[owner_id])

    def __init__(self, **kwargs):
        super(PanicReportModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<PanicReport %r, %r, %r>' % (self.id, self.latitude, self.longitude)

    @classmethod
    def count_all(cls, q=None, status=None, residential_id=None, panic_id=None, action_id=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.number.like('%'+q+'%'))

        if status:
            query = query.filter(cls.status==status)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if panic_id:
            query = query.filter(cls.panic_id==panic_id)

        if action_id:
            query = query.filter(cls.action_id==action_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, residential_id=None, panic_id=None, action_id=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.number.like('%'+q+'%'))

        if status:
            query = query.filter(cls.status==status)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if panic_id:
            query = query.filter(cls.panic_id==panic_id)

        if action_id:
            query = query.filter(cls.action_id==action_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class CCTVModel(db.Model):

    STATUS = OrderedDict([('online', 'Online'), ('offline', 'Offline')])

    __tablename__ = 'ringsatu_cctv'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    url = db.Column(db.String(255), nullable=False)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    isdeleted = db.Column(db.Boolean, default=False)
    status = db.Column(db.Enum(*STATUS, name="cctv_status"), default='online')
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=True)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='CASCADE'), index=True, nullable=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    author = db.relationship('UserModel', backref='author_cctv', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_cctv', foreign_keys=[owner_id])

    def __init__(self, **kwargs):
        super(CCTVModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<CCTV %r, %r, %r>' % (self.id, self.name, self.isdeleted)

    @classmethod
    def count_all(cls, q=None, status=None, residential_id=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if status:
            query = query.filter(cls.status==status)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, residential_id=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(cls.name.like('%'+q+'%'))

        if status:
            query = query.filter(cls.status==status)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        query = query.order_by(cls.id.asc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class AnnouncementModel(db.Model):

    STATUS = OrderedDict([('draft', 'Draft'), ('publish', 'Publish')])

    __tablename__ = 'ringsatu_announcements'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    number = db.Column(db.String(45), nullable=False)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    body = db.Column(db.Text, nullable=False)
    status = db.Column(db.Enum(*STATUS, name="announcement_status"), nullable=False)
    isdeleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='CASCADE'), index=True, nullable=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=True)
    category_id = db.Column(db.Integer, db.ForeignKey('ringsatu_categories.id'), index=True, nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    medias = db.relationship('MediaModel', backref='media_announcement', lazy='dynamic', cascade='all, delete-orphan')
    author = db.relationship('UserModel', backref='author_announcements', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_announcements', foreign_keys=[owner_id])
    signs = db.relationship('SignedByModel', backref='signed_announcement', lazy='dynamic', cascade='all, delete-orphan')
    comments = db.relationship('CommentModel', backref='comment_announcement', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(AnnouncementModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Announcement %r, %r, %r>' % (self.id, self.number, self.title)

    @classmethod
    def count_all(cls, q=None, status=None, author_id=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(or_(cls.number.like('%'+q+'%'), cls.title.like('%'+q+'%')))

        if status:
            query = query.filter(cls.status==status)

        if author_id:
            query = query.filter(cls.author_id==author_id)

        if residential_id:
            query = query.join(ResidentialModel.announcements).filter(ResidentialModel.id==residential_id)

        if category_id:
            query = query.filter(cls.category_id==category_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, author_id=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(or_(cls.number.like('%'+q+'%'), cls.title.like('%'+q+'%')))

        if status:
            query = query.filter(cls.status==status)

        if author_id:
            query = query.filter(cls.author_id==author_id)

        if residential_id:
            query = query.join(ResidentialModel.announcements).filter(ResidentialModel.id==residential_id)

        if category_id:
            query = query.filter(cls.category_id==category_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class ResidentReportModel(db.Model):

    STATUS = OrderedDict([('draft', 'Draft'), ('publish', 'Publish')])

    __tablename__ = 'ringsatu_resident_reports'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    number = db.Column(db.String(45), nullable=False)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    body = db.Column(db.Text, nullable=False)
    latitude = db.Column(db.Numeric(10,8), nullable=True)
    longitude = db.Column(db.Numeric(11,8), nullable=True)
    status = db.Column(db.Enum(*STATUS, name="residentreport_status"), nullable=False)
    isdeleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='CASCADE'), index=True, nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('ringsatu_categories.id'), index=True, nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    medias = db.relationship('MediaModel', backref='media_resident_report', lazy='dynamic', cascade='all, delete-orphan')
    author = db.relationship('UserModel', backref='author_resident_reports', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_resident_reports', foreign_keys=[owner_id])
    comments = db.relationship('CommentModel', backref='comment_resident_report', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(ResidentReportModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<ResidentReport %r, %r, %r>' % (self.id, self.number, self.title)

    @classmethod
    def count_all(cls, q=None, status=None, author_id=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(or_(cls.number.like('%'+q+'%'), cls.title.like('%'+q+'%')))

        if status:
            query = query.filter(cls.status==status)

        if author_id:
            query = query.filter(cls.author_id==author_id)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if category_id:
            query = query.filter(cls.category_id==category_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, author_id=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(or_(cls.number.like('%'+q+'%'), cls.title.like('%'+q+'%')))

        if status:
            query = query.filter(cls.status==status)

        if author_id:
            query = query.filter(cls.author_id==author_id)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if category_id:
            query = query.filter(cls.category_id==category_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class FinancialReportModel(db.Model):

    TRX_TYPE = OrderedDict([('debit', 'Debit'), ('credit', 'Credit')])
    TRX_STATUS = OrderedDict([('waiting', 'Waiting'), ('unpaid', 'Unpaid'), ('paid', 'Paid')])

    __tablename__ = 'ringsatu_financial_reports'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    trx_type = db.Column(db.Enum(*TRX_TYPE, name="transaction_type"), nullable=False)
    number = db.Column(db.String(45), nullable=False)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    debit = db.Column(db.Integer, default=0)
    credit = db.Column(db.Integer, default=0)
    recepient = db.Column(db.String(255), nullable=True)
    period = db.Column(db.Date, nullable=True)
    note = db.Column(db.String(255), nullable=True)
    status = db.Column(db.Enum(*TRX_STATUS, name="transaction_status"), default='waiting')
    isdeleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='CASCADE'), index=True, nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('ringsatu_categories.id'), nullable=True,)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    medias = db.relationship('MediaModel', backref='media_financial_report', lazy='dynamic', cascade='all, delete-orphan')
    author = db.relationship('UserModel', backref='author_financial_reports', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_financial_reports', foreign_keys=[owner_id])
    signs = db.relationship('SignedByModel', backref='signed_financial_report', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(FinancialReportModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<FinancialReport %r, %r, %r>' % (self.id, self.number, self.title)

    @classmethod
    def count_all(cls, q=None, trx_type=None, period=None, status=None, author_id=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False, cached=False):

        x = db.session.query(FinancialReportModel, (FinancialReportModel.credit-FinancialReportModel.debit).label('bal')).subquery()
        
        query = db.session.query(FinancialReportModel, func.sum(x.c.bal).label('balance')).filter(FinancialReportModel.isdeleted==isdeleted)

        query = query.join(x, x.c.id <= FinancialReportModel.id)

        if q:
            query = query.filter(or_(FinancialReportModel.number.like('%'+q+'%'), FinancialReportModel.title.like('%'+q+'%'), FinancialReportModel.recepient.like('%'+q+'%')))

        if trx_type:
            query = query.filter(FinancialReportModel.trx_type==trx_type)

        if status:
            query = query.filter(FinancialReportModel.status==status)

        if period:
            year = arrow.get(period).format('YYYY')
            month = arrow.get(period).format('MM')

            query = query.filter(extract('year', FinancialReportModel.period) == year, extract('month', FinancialReportModel.period) == month)

        if author_id:
            query = query.filter(FinancialReportModel.author_id==author_id)

        if residential_id:
            query = query.filter(FinancialReportModel.residential_id==residential_id)

        if category_id:
            query = query.filter(FinancialReportModel.category_id==category_id)

        if owner_id:
            query = query.filter(FinancialReportModel.owner_id==owner_id)

        query = query.order_by(FinancialReportModel.id.desc()).group_by(FinancialReportModel.id)

        if cached == True:
            cached_query = query.options(FromCache(cache))
            return cached_query.count()
        else:
            return query.count()


    @classmethod
    def get_all(cls, q=None, trx_type=None, period=None, status=None, author_id=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False, cached=False, page=1, perpage=25):

        x = db.session.query(FinancialReportModel, (FinancialReportModel.credit-FinancialReportModel.debit).label('bal')).subquery()
        
        query = db.session.query(FinancialReportModel, func.sum(x.c.bal).label('balance')).filter(FinancialReportModel.isdeleted==isdeleted)

        query = query.join(x, x.c.id <= FinancialReportModel.id)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(or_(FinancialReportModel.number.like('%'+q+'%'), FinancialReportModel.title.like('%'+q+'%'), FinancialReportModel.recepient.like('%'+q+'%')))

        if trx_type:
            query = query.filter(FinancialReportModel.trx_type==trx_type)

        if status:
            query = query.filter(FinancialReportModel.status==status)
            
        if period:
            year = arrow.get(period).format('YYYY')
            month = arrow.get(period).format('MM')

            query = query.filter(extract('year', FinancialReportModel.period) == year, extract('month', FinancialReportModel.period) == month)

        if author_id:
            query = query.filter(FinancialReportModel.author_id==author_id)

        if residential_id:
            query = query.filter(FinancialReportModel.residential_id==residential_id)

        if category_id:
            query = query.filter(FinancialReportModel.category_id==category_id)

        if owner_id:
            query = query.filter(FinancialReportModel.owner_id==owner_id)

        # rn = func.row_number().over(partition_by=cls.id, order_by=cls.id.asc()).label('rn')
        # query = query.with_entities(cls, (func.sum(cls.credit).over(order_by=cls.id.asc()) - func.sum(cls.debit).over(order_by=cls.id.asc())).label('balance'), rn).from_self().filter(rn==1).order_by(cls.id.desc()).group_by(cls.id).limit(perpage).offset(0)
        
        query = query.order_by(FinancialReportModel.id.desc()).group_by(FinancialReportModel.id).limit(perpage).offset(offset)
        
        if cached == True:
            cached_query = query.options(FromCache(cache))
            return reversed(cached_query.all())
        return query.all()


    @classmethod
    def get_total_debit(cls, q=None, trx_type=None, period=None, status=None, author_id=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False):

        query = cls.query.with_entities(func.sum(cls.debit).label('total_debit')).filter(cls.trx_type=='debit', cls.isdeleted==isdeleted)

        if period:
            year = arrow.get(period).format('YYYY')
            month = arrow.get(period).format('MM')

            query = query.filter(extract('year', cls.period) == year, extract('month', cls.period) == month)

        if author_id:
            query = query.filter(cls.author_id==author_id)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if category_id:
            query = query.filter(cls.category_id==category_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        query = query.first()

        return query.total_debit or 0


    @classmethod
    def get_total_credit(cls, q=None, trx_type=None, period=None, status=None, author_id=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False):

        query = cls.query.with_entities(func.sum(cls.credit).label('total_credit')).filter(cls.trx_type=='credit', cls.isdeleted==isdeleted)

        if period:
            year = arrow.get(period).format('YYYY')
            month = arrow.get(period).format('MM')

            query = query.filter(extract('year', cls.period) == year, extract('month', cls.period) == month)

        if author_id:
            query = query.filter(cls.author_id==author_id)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if category_id:
            query = query.filter(cls.category_id==category_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        query = query.first()

        return query.total_credit or 0


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class InventoryModel(db.Model):

    __tablename__ = 'ringsatu_inventories'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    number = db.Column(db.String(45), nullable=False)
    title = db.Column(db.String(255), nullable=False)
    slug = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=True)
    received_date = db.Column(db.Date, nullable=False)
    quantity = db.Column(db.Integer, default=1)
    isdeleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='CASCADE'), index=True, nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('ringsatu_categories.id'), nullable=True,)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    medias = db.relationship('MediaModel', backref='media_inventory', lazy='dynamic', cascade='all, delete-orphan')
    movements = db.relationship('InventoryMovementModel', backref='movement_inventory', lazy='dynamic', cascade='all, delete-orphan')
    author = db.relationship('UserModel', backref='author_inventories', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_inventories', foreign_keys=[owner_id])

    def __init__(self, **kwargs):
        super(InventoryModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<Inventory %r, %r, %r>' % (self.id, self.number, self.title)

    @classmethod
    def count_all(cls, q=None, status=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(or_(cls.number.like('%'+q+'%'), cls.title.like('%'+q+'%')))

        if status:
            query = query.filter(cls.movements.any(InventoryMovementModel.status == status))

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if category_id:
            query = query.filter(cls.category_id==category_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, residential_id=None, category_id=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(or_(cls.number.like('%'+q+'%'), cls.title.like('%'+q+'%')))

        if status:
            query = query.filter(cls.movements.any(InventoryMovementModel.status == status))

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if category_id:
            query = query.filter(cls.category_id==category_id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()


class InventoryMovementModel(db.Model):

    STATUS = OrderedDict([('waiting', 'Waiting For Approval'), ('onloan', 'On Loan'), ('rentedout', 'Rented Out'), ('beingrepaired', 'Being Repaired'), ('broken', 'Broken'), ('returned', 'Returned'), ('missing', 'Missing'), ('fixed', 'Fixed')])

    __tablename__ = 'ringsatu_inventory_movements'

    id = db.Column(db.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    number = db.Column(db.String(45), nullable=False)
    name = db.Column(db.String(45), nullable=False)
    address = db.Column(db.String(255), nullable=False)
    phone = db.Column(db.String(45), nullable=False)
    start_date = db.Column(db.Date, nullable=True)
    end_date = db.Column(db.Date, nullable=True)
    quantity = db.Column(db.Integer, default=1)
    cost = db.Column(db.Integer, default=0)
    description = db.Column(db.String(255), nullable=True)
    status = db.Column(db.Enum(*STATUS, name="movement_status"), nullable=False)
    isdeleted = db.Column(db.Boolean, default=False)
    author_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id', ondelete='CASCADE'), index=True, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('ringsatu_users.id'), index=True, nullable=False)
    region_id = db.Column(db.Integer, db.ForeignKey('ringsatu_regions.id'), index=True, nullable=True)
    residential_id = db.Column(db.Integer, db.ForeignKey('ringsatu_residentials.id', ondelete='CASCADE'), index=True, nullable=False)
    inventory_id = db.Column(db.Integer, db.ForeignKey('ringsatu_inventories.id'), index=True, nullable=True)  
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    updated = db.Column(db.DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    author = db.relationship('UserModel', backref='author_inventory_movements', foreign_keys=[author_id])
    owner = db.relationship('UserModel', backref='owner_inventory_movements', foreign_keys=[owner_id])
    signs = db.relationship('SignedByModel', backref='signed_inventory_movement', lazy='dynamic', cascade='all, delete-orphan')

    def __init__(self, **kwargs):
        super(InventoryMovementModel, self).__init__(**kwargs)

    def __repr__(self):
        return '<InventoryMovement %r, %r, %r>' % (self.id, self.number, self.name)

    @classmethod
    def count_all(cls, q=None, status=None, start_date=None, end_date=None, residential_id=None, owner_id=None, isdeleted=False):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        if q:
            query = query.filter(or_(cls.number.like('%'+q+'%'), cls.name.like('%'+q+'%'), cls.phone.like('%'+q+'%')))

        if status:
            query = query.filter(cls.status==status)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if start_date and end_date:
            query = query.filter(cls.start_date>=start_date, cls.end_date <= end_date)
        elif start_date:
            query = query.filter(cls.start_date>=start_date)
        elif end_date:
            query = query.filter(cls.end_date>=end_date)
        
        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.count()


    @classmethod
    def get_all(cls, q=None, status=None, start_date=None, end_date=None, residential_id=None, owner_id=None, isdeleted=False, page=1, perpage=25):
        
        query = cls.query.filter(cls.isdeleted==isdeleted)

        offset = (page - 1) * perpage

        if q:
            query = query.filter(or_(cls.number.like('%'+q+'%'), cls.name.like('%'+q+'%'), cls.phone.like('%'+q+'%')))

        if status:
            query = query.filter(cls.status==status)

        if residential_id:
            query = query.filter(cls.residential_id==residential_id)

        if start_date and end_date:
            query = query.filter(cls.start_date>=start_date, cls.end_date <= end_date)
        elif start_date:
            query = query.filter(cls.start_date>=start_date)
        elif end_date:
            query = query.filter(cls.end_date>=end_date)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        query = query.order_by(cls.updated.desc()).limit(perpage).offset(offset)
        cached_query = query.options(FromCache(cache))
        return cached_query.all()


    @classmethod
    def get_by(cls, id, owner_id=None):
        
        query = cls.query.filter(cls.id==id)

        if owner_id:
            query = query.filter(cls.owner_id==owner_id)

        cached_query = query.options(FromCache(cache))
        return cached_query.first()



History(ResidentialModel)
History(PanicModel)
History(PanicParticipantModel)
History(PanicReportModel)
History(CCTVModel)
History(AnnouncementModel)
History(ResidentReportModel)
History(FinancialReportModel)
History(InventoryModel)
History(InventoryMovementModel)
History(InfoModel)
History(CommentModel)
History(ChatModel)
History(ChatMessageModel)