from webargs import missing
from math import ceil
from flask import url_for

'''
Simple Pagination 
'''

class Pagination(object):

    def __init__(self, endpoint, total, **kwargs):

        self.page = kwargs['page']
        self.perpage = kwargs['perpage']
        self.total = total
        self.endpoint = endpoint
        self.kwargs = kwargs

        self._normalize()

    def _normalize(self):
        
        if 'exclude' in self.kwargs and self.kwargs['exclude'] is not None:
            self.kwargs['exclude'] = ','.join(self.kwargs['exclude']) 

        if 'only' in self.kwargs and self.kwargs['only'] is not None:
            self.kwargs['only'] = ','.join(self.kwargs['only'])

        if 'page' in self.kwargs:
            del self.kwargs['page']
    
    @property
    def paginate(self):
        return {
            'pages': self.pages,
            'page': self.page,
            'perpage': self.perpage,
            'total': self.total,
            'has_prev': self.has_prev,
            'has_next': self.has_next,
            'prev_url': self.prev_url,
            'next_url': self.next_url,
            'curr_url': self.curr_url
        }

    @property
    def pages(self):
        return int(ceil(self.total / float(self.perpage)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    @property
    def prev_url(self):
        if self.has_prev:
            return url_for(self.endpoint, page=self.page - 1, **self.kwargs)
        return ''

    @property
    def next_url(self):
        if self.has_next:
            return url_for(self.endpoint, page=self.page + 1, **self.kwargs)
        return ''

    @property
    def curr_url(self):
        return url_for(self.endpoint, page=self.page, **self.kwargs)

    def iter_pages(self, left_edge=2, left_current=2, right_current=5, right_edge=2):
        last = 0
        for num in xrange(1, self.pages + 1):
            if num <= left_edge or (num > self.page - left_current - 1 and num < self.page + right_current) or num > self.pages - right_edge:
                if last + 1 != num:
                    yield None
                yield num
                last = num