import os
import re
import random
import string
import arrow
import json
import base64
import requests
import urlparse
import decimal

from PIL import Image
from StringIO import StringIO
from os.path import basename

from datetime import date
from datetime import datetime, timedelta

from flask import current_app, url_for, request, flash

from math import ceil
from wheezy.core.feistel import make_feistel_number
from wheezy.core.feistel import sample_f

import logging

# Comment out this line to hide the log lines
logging.basicConfig(level=logging.DEBUG)

feistel_number = make_feistel_number(sample_f)

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.101 Safari/537.36'

def allowed_domain(url):
    url = urlparse.urlparse(url)
    if url.netloc != '':
        return url.netloc.lower().replace('www.', '') in current_app.config.get('ALLOWED_DOMAINS')
    return False

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in current_app.config.get('ALLOWED_EXTENSIONS')


def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

def initial(fullname):
    return ''.join([x[0].upper() for idx, x in enumerate(fullname.split(' ')) if idx < 2])

def get_clients_ip():
    headers_list = request.headers.getlist("X-Forwarded-For")
    user_ip = headers_list[0] if headers_list else request.remote_addr
    return user_ip

def generate_sms_token(phone='', n=6):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits + phone) for _ in range(n))

def generate_unique_id(n=16):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits + str(arrow.utcnow().timestamp)) for _ in range(n))

def generate_filename(path):
    path = os.path.expanduser(path)

    root, ext = os.path.splitext(os.path.expanduser(path))
    dir = os.path.dirname(root)
    fname = os.path.basename(root)
    candidate = fname+ext

    if not os.path.exists(path):
        return candidate

    index = 0
    ls = set(os.listdir(dir))
    while candidate in ls:
        candidate = "{}_{}{}".format(fname,index,ext)
        index += 1
    return candidate


def generate_password(length=8):

    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()-_+=\/.,?[]|'
    password = ''
    for c in range(length):
        password += random.choice(chars)

    return password


def normalize_phone_number(phone_number):

    phone = phone_number

    if phone_number.startswith('08'):
        phone = phone_number.replace(phone_number[:2], '628', 1)
    elif phone_number.startswith('+628'):
        phone = phone_number.replace(phone_number[:4], '628', 1)
    
    return phone


def denormalize_phone_number(phone_number):

    phone = phone_number
    
    if phone_number.startswith('628'):
        phone = phone_number.replace(phone_number[:3], '08', 1)
    elif phone_number.startswith('+628'):
        phone = phone_number.replace(phone_number[:4], '08', 1)       
    
    return phone

def parse_verification_token(token):
    try:
        payload = json.loads(base64.urlsafe_b64decode(str(token)))
        return payload
    except ValueError:
        return None

def is_file_exist(path):
    return os.path.exists(path)


def is_dir_exist(file):
    if not os.path.exists(os.path.dirname(file)):
        try:
            os.makedirs(os.path.dirname(file))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise

def reverse_id(id):
    return feistel_number(id)

def split_by_n(seq, n):
    while seq:
        yield seq[:n]
        seq = seq[n:]

def valid_url(url):

    regex = re.compile(
        r'^(?:http|hxxp|ftp|fxp|rtsp)s?://' # http:// or https://
        r"(?:(\w{1,255}):(.{1,255})@|^|)"
        r'(?:(?:(?=\S{0,253}(?:$|:))' #domain...
        r'((?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+' #subdomain
        r'(?:[a-z0-9]{1,63})))' #TLD
        r'|localhost)' # localhost
        r'(:\d{1,5})?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

    return re.match(regex, url)

def get_administrative_region(region, level=0):

    regions = []

    if level == 0 and region.postal:
        regions.append(region.postal)

    if region.parents:        
        regions.append(region.name)
        level = level + 1
        regions.extend(get_administrative_region(region.parents, level))

    else:
        regions.append(region.name)

    if len(regions) == 5:
        regions.append(regions.pop(0))

    return regions


def restricted_area(lat, lng, polygon):
    x = lat
    y = lng
    
    inside = False
    intersections = 0
    ss = ''
    polyPoints = polygon
    
    j = len(polyPoints) - 1

    for i in range(len(polyPoints)):
        
        xi = decimal.Decimal(polyPoints[i]['lat']) 
        yi = decimal.Decimal(polyPoints[i]['lng'])
        xj = decimal.Decimal(polyPoints[j]['lat'])
        yj = decimal.Decimal(polyPoints[j]['lng'])

        if (yj == yi and yj == y and x > min(xj, xi) and x < max(xj, xi)):
            return True

        if (y > min(yj, yi) and y <= max(yj, yi) and x <= max(xj, xi) and yj != yi):

            ss = (y - yj) * (xi - xj) / (yi - yj) + xj

            if ss == x:
                return True


            if (xj == xi or x <= ss):
                intersections += 1

        if i > 0:
            j = i

    if intersections % 2 != 0:
        return True
    else:
        return False


def generate_stream(stream, error_image, boundary):
    # Add headers to the error
    with open(error_image, 'rb') as f:
        error_with_headers = b'--' + boundary + '\r\nContent-Type: image/jpeg\r\n\r\n' + f.read() + b'\r\n'

    while True:
        content_length_found = False

        # Start with blank data var
        data = ''

        # Read first several lines, looking for content length line
        for line in range(10):
            data += stream.readline()

            if 'Content-Length' in data:
                content_length_found = True
                break

        if content_length_found:
            # Figure out the content length
            try:
                content_length = int(data.splitlines()[-1].rstrip().split()[-1])
            except (KeyboardInterrupt, SystemExit):
                raise
            except Exception as ex:
                logging.debug("Failed to parse the content length line")
                yield error_with_headers
                raise StopIteration

            logging.debug("Content_Length: %d" % content_length)
        else:
            # Unable to figure out content length
            logging.debug("Content length field not found")
            yield error_with_headers
            raise StopIteration

        # Read the line before the jpeg
        data += stream.readline()

        # Read the jpeg
        data += stream.read(content_length)

        # Return the jpeg
        yield data