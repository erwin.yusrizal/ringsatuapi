from flask import current_app
from pusher import Pusher

class PusherSocket(object):

    def __init__(self):

        self.pusher = None
        self.initialize()


    def initialize(self):

        pusher = Pusher(
            app_id=current_app.config.get('PUSHER_APPID'),
            key=current_app.config.get('PUSHER_KEY'),
            secret=current_app.config.get('PUSHER_SECRET'),
            cluster=current_app.config.get('PUSHER_CLUSTER')
        )

        self.pusher = pusher


    def trigger(self, channel, event, payload):

        self.pusher.trigger(channel, event, payload)