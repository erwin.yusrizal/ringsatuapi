from flask import Flask
from flask_apscheduler import APScheduler
from flask_babel import Babel
from flask_bcrypt import Bcrypt
from flask_caching import Cache
from flask_cors import CORS
from flask_images import Images
from flask_jwt_extended import JWTManager
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_mail import Mail
from flask_marshmallow import Marshmallow
from flask_secure_headers.core import Secure_Headers
from flask_sqlalchemy import SQLAlchemy, Model
from flask_sqlalchemy_caching import CachingQuery

from celery import Celery
from config import config, Config

Model.query_class = CachingQuery

babel = Babel()
bcrypt = Bcrypt()
cache = Cache()
cors = CORS()
db = SQLAlchemy(session_options={'query_cls': CachingQuery})
images = Images()
jwt = JWTManager()
limiter = Limiter(key_func=get_remote_address)
mail = Mail()
marshmallow = Marshmallow()
scheduler = APScheduler()
secureheaders = Secure_Headers()

secureheaders.rewrite({'CSP':{'report-uri':['/v1/csp_report']}})
celery = Celery(__name__, broker=Config.CELERY_BROKER_URL, include=['api.v1.tasks'])

def create_app(cfg):

    app = Flask(__name__)
    app.config.from_object(config[cfg])
    config[cfg].init_app(app)

    babel.init_app(app)
    bcrypt.init_app(app)
    cache.init_app(app)
    cors.init_app(app)
    db.init_app(app)
    images.init_app(app)
    jwt.init_app(app)
    limiter.init_app(app)
    mail.init_app(app)
    marshmallow.init_app(app)
    secureheaders.init_app(app)
    celery.conf.update(app.config)
    scheduler.init_app(app)
    scheduler.start()

    from api.v1 import blueprint as api_module

    app.register_blueprint(api_module)

    return app