import os
import redis
from datetime import timedelta

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    
    SERVER_NAME = 'api.ringsatu.id'

    ROOT = os.path.join(basedir, 'api')    
    UPLOAD_FOLDER = os.path.join(ROOT, 'static/uploads')
    TMP_FOLDER = os.path.join(UPLOAD_FOLDER, 'tmp')
    PROFILE_FOLDER = os.path.join(UPLOAD_FOLDER, 'profiles')
    ANNOUNCEMENT_FOLDER = os.path.join(UPLOAD_FOLDER, 'announcements')
    RESIDENT_REPORT_FOLDER = os.path.join(UPLOAD_FOLDER, 'residentreports')
    FINANCIAL_REPORT_FOLDER = os.path.join(UPLOAD_FOLDER, 'financials')
    INVENTORY_FOLDER = os.path.join(UPLOAD_FOLDER, 'inventories')
    PRODUCT_FOLDER = os.path.join(UPLOAD_FOLDER, 'products')
    CATEGORY_FOLDER = os.path.join(UPLOAD_FOLDER, 'categories')
    INFO_FOLDER = os.path.join(UPLOAD_FOLDER, 'infos')
    COMMENT_FOLDER = os.path.join(UPLOAD_FOLDER, 'comments')
    MESSAGE_FOLDER = os.path.join(UPLOAD_FOLDER, 'messages')
    PANIC_FOLDER = os.path.join(UPLOAD_FOLDER, 'panics')
    FREEBIES_FOLDER = os.path.join(UPLOAD_FOLDER, 'freebies')

    # FLASK
    DEBUG = False
    TESTING = False
    SQLALCHEMY_ECHO = False

    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx'])
    MAX_CONTENT_LENGTH = 20 * 1024 * 1024

    ERROR_404_HELP = False

    # BLEACH
    BLEACH_ALLOWED_TAGS = set(['a', 'b', 'em', 'i', 'li', 'ol',
                               'strong', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'p'])
    BLEACH_ALLOWED_ATTRIBUTES = ['href', 'title', 'style', 'src']
    BLEACH_ALLOWED_STYLES = ['font-family', 'font-weight']
    BLEACH_STRIP_TAGS = True

    SECRET_KEY = b'\xa1\xbeD\x86\x8a\xa0\xb0J\x1b\x03\x12\xdcD\x88o\x87\x99@\xc6\x8fM\x80\xf1\r\x00\xbf\x0f2tLA\x1a#(\xa34\x04\xa3\xdb\x95r\xb8\xa2\x10(Z-\x1d\xf6\xab\x8d\xee\xf2G\n\x12\x8eC\xdbu5\xa6\x9a\xee\x9ca\x8aj\xf68\xba\xb1]\xae\x80\xd6W\xd6\xf8$\xeceG\x9d\xb3\x11\xa1\xdc\xc2\xaev\x9d\x19\xd4!%\xd7\xd3\x9b\xb8'

    # BABEL
    BABEL_DEFAULT_LOCALE = 'id'
    BABEL_DEFAULT_TIMEZONE = 'Asia/Jakarta'


    # CACHE
    CACHE_TYPE = 'redis'
    CACHE_DEFAULT_TIMEOUT = 60
    CACHE_REDIS_URL = 'redis://:Pr0m0th3u5GAMON@127.0.0.1:18567/0'
    CACHE_KEY_PREFIX = 'ringsatu_cache_'

    # RATE LIMIT
    RATELIMIT_HEADERS_ENABLED = True
    RATELIMIT_STORAGE_URL = 'redis://:Pr0m0th3u5GAMON@127.0.0.1:18567/0'
    RATELIMIT_KEY_PREFIX = 'ringsatu_ratelimit_'

    # CELERY
    CELERY_BROKER_URL = 'redis://:Pr0m0th3u5GAMON@127.0.0.1:18567/1'
    CELERY_RESULT_BACKEND = 'redis://:Pr0m0th3u5GAMON@127.0.0.1:18567/1'
    CELERY_ACCEPT_CONTENT = ['json', 'pickle']

    # RESIZE MEDIA
    IMAGES_PATH = ['static']
    IMAGES_CACHE = os.path.join(ROOT, 'static/cache')

    # MAIL
    ADMIN_EMAIL = 'robot@ringsatu.id'
    MAIL_SERVER = 'smtp.webfaction.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'rs_robot'
    MAIL_PASSWORD = 'Pr0m0th3u52922'
    MAIL_DEFAULT_SENDER = 'robot@ringsatu.id'
    MAIL_ASCII_ATTACHMENTS = True
    MAIL_DEBUG = True

    # NEXMO
    NEXMO_API_KEY = 'aa1df3e7'
    NEXMO_SECRET_KEY = '5a821e5921be07f4'

    # JWT
    JWT_TOKEN_LOCATION = ['headers']
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=365)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=365)

    CORS_ENABLED = True
    CORS_ALLOW_HEADERS = ['Origin', 'Accept', 'Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With']
    # CORS_RESOURCES = {r"/v1/*": {"origins": "*"}}
    CORS_METHODS = ['GET', 'POST', 'DELETE', 'PUT', 'OPTIONS']
    CORS_SUPPORTS_CREDENTIALS = True

    # ONE SIGNAL
    ONESIGNAL_APPID = '6bc67df0-7c2c-4b78-8719-c17eb0f94766'
    ONESIGNAL_APIKEY = 'OWQyMDE1OTAtYWM1OS00MGRjLWI2YjYtMzczMmQ5MTY2Njdl'

    # PUSHER
    PUSHER_APPID = '808245'
    PUSHER_KEY = '84d7991410678a0998f6'
    PUSHER_SECRET = '9878244a3d8dba9f30b8'
    PUSHER_CLUSTER = 'ap1'


    # DOMAINS FOR URL VALIDATIONS
    # TODO: Move this to DB Table
    ALLOWED_DOMAINS = set(['api.ringsatu:3000'])

    #APSCHEDULER
    JOBS = [
        {
            'id': 'delete_tmp_media',
            'func': 'api.v1.tasks.schedules:delete_tmp_media',
            'trigger': 'cron',
            'minute': '0'
        }
    ]

    SCHEDULER_EXECUTORS = {
        'default': {'type': 'threadpool', 'max_workers': 20}
    }

    SCHEDULER_JOB_DEFAULTS = {
        'coalesce': False,
        'max_instances': 3
    }

    @staticmethod
    def init_app(app):
        pass


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'mysql://ringsatu:Pr0m0th3u5RINGSATU@127.0.0.1:3306/db_ringsatu'


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'mysql://ringsatu:Pr0m0th3u5RINGSATU@127.0.0.1:3306/db_ringsatu'


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = 'mysql://ringsatu:Pr0m0th3u5RINGSATU@127.0.0.1:3306/db_ringsatu'


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': ProductionConfig
}
